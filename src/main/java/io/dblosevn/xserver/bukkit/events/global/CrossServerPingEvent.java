package io.dblosevn.xserver.bukkit.events.global;

import com.google.gson.JsonObject;
import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.objects.CrossServerEvent;
import io.dblosevn.xserver.bukkit.sockets.packets.PingPacket;
import org.bukkit.event.HandlerList;

public class CrossServerPingEvent
extends CrossServerEvent {
    private static final HandlerList handlers = new HandlerList();
    private String server = "";

    public CrossServerPingEvent() {
    }

    public CrossServerPingEvent(JsonObject o) {
        this.deSerialize(o);
    }

    @Override
    public JsonObject serialize() {
        JsonObject o = new JsonObject();
        o.addProperty("server", CrossServerAPI.getServer());
        return o;
    }

    @Override
    public void deSerialize(JsonObject o) {
        this.server = o.get("server").getAsString();
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public PingPacket getPacket() {
        return new PingPacket(this);
    }

    public String getServer() {
        return this.server;
    }
}

