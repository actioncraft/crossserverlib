package io.dblosevn.xserver.bukkit.events.global;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.dblosevn.xserver.bukkit.objects.CrossServerEvent;
import io.dblosevn.xserver.bukkit.objects.ProxyPlayer;
import io.dblosevn.xserver.bukkit.sockets.packets.ProxyStatusPacket;
import io.netty.util.internal.ConcurrentSet;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import org.bukkit.event.HandlerList;

public class CrossServerProxyStatusEvent
extends CrossServerEvent {
    private static final HandlerList handlers = new HandlerList();
    public final int STATUS_REQUEST = 1;
    public final int STATUS_RESPONSE = 2;
    private int status = 0;
    private ConcurrentHashMap<String, ConcurrentSet<ProxyPlayer>> serversToPlayers = new ConcurrentHashMap<String, ConcurrentSet<ProxyPlayer>>();
    private ConcurrentHashMap<UUID, ProxyPlayer> uuidsToPlayer = new ConcurrentHashMap<UUID, ProxyPlayer>();
    private ConcurrentHashMap<String, ProxyPlayer> playernamesToPlayer = new ConcurrentHashMap<String, ProxyPlayer>();
    private ConcurrentHashMap<String, String> serverNames = new ConcurrentHashMap<String, String>();

    public CrossServerProxyStatusEvent() {
    }

    public CrossServerProxyStatusEvent(int status) {
        this.status = status;
    }

    public CrossServerProxyStatusEvent(JsonObject o) {
        this.deSerialize(o);
    }

    @Override
    public JsonObject serialize() {
        JsonObject o = new JsonObject();
        o.addProperty("status", (Number)this.status);
        switch (this.status) {
            case 1: {
                break;
            }
            case 2: {
                JsonObject servers = new JsonObject();
                for (Map.Entry<String, ConcurrentSet<ProxyPlayer>> entry : this.serversToPlayers.entrySet()) {
                    JsonArray players = new JsonArray();
                    for (ProxyPlayer p : entry.getValue()) {
                        JsonObject temp = new JsonObject();
                        temp.addProperty("uuid", p.getUuid().toString());
                        temp.addProperty("name", p.getName());
                        players.add((JsonElement)temp);
                    }
                    servers.add(entry.getKey(), (JsonElement)players);
                }
                o.add("servers", (JsonElement)servers);
            }
        }
        return o;
    }

    @Override
    public void deSerialize(JsonObject o) {
        this.status = o.get("status").getAsInt();
        switch (this.status) {
            case 1: {
                break;
            }
            case 2: {
                for (Map.Entry<String, JsonElement> entry : o.get("servers").getAsJsonObject().entrySet()) {
                    this.serversToPlayers.put(((String)entry.getKey()).toLowerCase(), new ConcurrentSet<ProxyPlayer>());
                    for (JsonElement ePlayer : (entry.getValue()).getAsJsonArray()) {
                        JsonObject player = ePlayer.getAsJsonObject();
                        ProxyPlayer pPlayer = new ProxyPlayer(player.get("name").getAsString(), player.get("uuid").getAsString(), (String)entry.getKey());
                        this.serversToPlayers.get(((String)entry.getKey()).toLowerCase()).add(pPlayer);
                        this.uuidsToPlayer.put(pPlayer.getUuid(), pPlayer);
                        this.playernamesToPlayer.put(pPlayer.getName().toLowerCase(), pPlayer);
                        this.serverNames.put(((String)entry.getKey()).toLowerCase(), (String)entry.getKey());
                    }
                }
            }
        }
    }

    public ConcurrentHashMap<String, String> getServerNames() {
        return this.serverNames;
    }

    public ConcurrentHashMap<String, ConcurrentSet<ProxyPlayer>> getServersToPlayers() {
        return this.serversToPlayers;
    }

    public ConcurrentHashMap<UUID, ProxyPlayer> getUuidsToPlayer() {
        return this.uuidsToPlayer;
    }

    public ConcurrentHashMap<String, ProxyPlayer> getPlayernamesToPlayer() {
        return this.playernamesToPlayer;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public ProxyStatusPacket getPacket() {
        return new ProxyStatusPacket(this);
    }

    public boolean isResponse() {
        if (this.status != 1) {
            return true;
        }
        return false;
    }

    public int getStatus() {
        return this.status;
    }
}

