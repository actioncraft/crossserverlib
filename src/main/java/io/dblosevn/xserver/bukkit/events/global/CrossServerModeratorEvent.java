package io.dblosevn.xserver.bukkit.events.global;

import com.google.gson.JsonObject;

import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.objects.CrossServerEvent;
import io.dblosevn.xserver.bukkit.objects.ModAction;
import io.dblosevn.xserver.bukkit.sockets.packets.ModeratorPacket;
import io.dblosevn.xserver.bukkit.util.Util;
import static io.dblosevn.xserver.bukkit.CrossServerLib.primaryColor;
import static io.dblosevn.xserver.bukkit.CrossServerLib.secondaryColor;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

public class CrossServerModeratorEvent extends CrossServerEvent {
	private static final HandlerList handlers = new HandlerList();
	private String source = "";
	private String target = "";
	private String message = "";
	private long expires = 0;
	private ModAction action = ModAction.UNKNOWN;

	public CrossServerModeratorEvent() {
	}

	public CrossServerModeratorEvent(JsonObject o) {
		this.deSerialize(o);
	}

	public CrossServerModeratorEvent(String source, String target, String message, ModAction action, long expires) {
		this(source, target, message, action);
		this.expires = expires;
	}
	public CrossServerModeratorEvent(String source, String target, String message, ModAction action) {
		this.source = source;
		this.target = target;
		this.message = message;
		this.action = action;
	}

	public CrossServerModeratorEvent(String source, String target, ModAction action) {
		this.source = source;
		this.target = target;
		this.action = action;
	}

	@Override
	public JsonObject serialize() {
		JsonObject o = new JsonObject();
		o.addProperty("action", (Number) this.action.getId());
		o.addProperty("source", this.source);
		o.addProperty("target", this.target);
		o.addProperty("expires", this.expires);
		o.addProperty("message", this.message);
		return o;
	}

	@Override
	public void deSerialize(JsonObject o) {
		this.action = ModAction.getById(o.get("action").getAsInt());
		this.source = o.get("source").getAsString();
		this.target = o.get("target").getAsString();
		this.message = o.get("message").getAsString();
		this.expires = o.get("expires").getAsLong();
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public ModeratorPacket getPacket() {
		return new ModeratorPacket(this);
	}

	public void handle() {
		switch (this.action) {
			case KICK:
				if (CrossServerAPI.localOnline(this.target)) {
					Player target = Bukkit.getPlayer(this.target);
					target.kickPlayer(ChatColor.translateAlternateColorCodes((char) '&',
							ChatColor.RED + " You have been kicked from the server by "
									+  primaryColor + this.source +  ChatColor.RED + " for "
									+ this.message));
				}
				Util.modMessage(secondaryColor + this.source +  primaryColor + " kicked " +  secondaryColor
						+ this.target +  primaryColor + " for " + this.message);
			break;
			case TEMP:
				if (CrossServerAPI.localOnline(this.target)) {
					Player target = Bukkit.getPlayer(this.target);
					target.kickPlayer(ChatColor.translateAlternateColorCodes((char) '&',
							ChatColor.RED + " You have been temporarily banned from the server by "
									+  secondaryColor + this.source + ChatColor.RED + " until " + secondaryColor + Util.timeToString(expires * 1000) + ChatColor.RED + " for " + secondaryColor + this.message));
				}
				
				Util.modMessage(secondaryColor + this.source +  ChatColor.RED + " has temporarily banned " +  secondaryColor
						+ this.target + ChatColor.RED + " until " + secondaryColor + Util.timeToString(expires * 1000) + ChatColor.RED + " for " + secondaryColor + this.message);
			break;
			case PERM:
				if (CrossServerAPI.localOnline(this.target)) {
					Player target = Bukkit.getPlayer(this.target);
					target.kickPlayer(ChatColor.translateAlternateColorCodes((char) '&',
							ChatColor.RED + " You have been banned from the server by "
									+  secondaryColor + this.source +  ChatColor.RED + " for "
									+ secondaryColor + this.message));
				}
				Util.modMessage(secondaryColor + this.source +  primaryColor + " banned " +  secondaryColor
						+ this.target +  primaryColor + " for " + secondaryColor + this.message);
			break;
			case UNBAN:
				Util.modMessage(secondaryColor + this.source +  primaryColor + " unbanned " +  secondaryColor
						+ this.target);
			break;
			default:
			break;
		}
	}

	public ModAction getAction() {
		return this.action;
	}
	public long getExpires() {
		return expires;
	}
	public String getMessage() {
		return message;
	}

}
