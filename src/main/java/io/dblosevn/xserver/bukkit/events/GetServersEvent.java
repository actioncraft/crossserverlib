package io.dblosevn.xserver.bukkit.events;

import java.util.Arrays;
import java.util.List;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GetServersEvent
extends Event {
    private static final HandlerList handlers = new HandlerList();
    private List<String> serverList;

    public GetServersEvent(String serverList) {
        this.serverList = Arrays.asList(serverList.split(", "));
    }

    public List<String> getServers() {
        return this.serverList;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}

