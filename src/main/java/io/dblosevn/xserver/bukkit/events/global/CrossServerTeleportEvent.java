package io.dblosevn.xserver.bukkit.events.global;

import com.google.gson.JsonObject;
import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.objects.CrossServerEvent;
import io.dblosevn.xserver.bukkit.objects.ProxyPlayer;
import io.dblosevn.xserver.bukkit.objects.TeleportLocation;
import io.dblosevn.xserver.bukkit.objects.TeleportType;
import io.dblosevn.xserver.bukkit.sockets.lib.Server;
import io.dblosevn.xserver.bukkit.sockets.packets.TeleportPacket;
import io.dblosevn.xserver.bukkit.util.Util;
import java.util.HashMap;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

public class CrossServerTeleportEvent extends CrossServerEvent implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private static HashMap<UUID, CrossServerTeleportEvent> cache = new HashMap<UUID, CrossServerTeleportEvent>();

	private boolean cancelled;

	private long created = System.currentTimeMillis();
	private TeleportType type;
	private String orginServer = "";
	private String fromPlayer = "";
	private String toPlayer = "";
	private String source = "";
	private String target = "";

	private TeleportLocation fromLocation = new TeleportLocation();
	private TeleportLocation toLocation = new TeleportLocation();

	public CrossServerTeleportEvent() {
	}

	public CrossServerTeleportEvent(Player fromPlayer, String toPlayer, TeleportType type) {
		this.type = type;
		orginServer = CrossServerAPI.getServer();
		switch (type) {
			case PLAYER_TP:
			case TELEPORT_REQUEST:
				this.fromPlayer = fromPlayer.getName();
				this.toPlayer = toPlayer;
				source = this.fromPlayer;
				target = toPlayer;
				fromLocation = new TeleportLocation(fromPlayer.getLocation());
			break;
			case TELEPORT_HERE_REQUEST:
			case PLAYER_TPHERE:
				this.fromPlayer = fromPlayer.getName();
				this.toPlayer = toPlayer;
				source = toPlayer;
				target = this.fromPlayer;
			default:
				break;
		}

	}

	public CrossServerTeleportEvent(Player player, TeleportType type) {
		this.type = type;
		orginServer = CrossServerAPI.getServer();
		switch (type) {
		case TELEPORT_HERE_ACCEPT:
		case TELEPORT_ACCEPT:
		case TELEPORT_DENY:
			if (player.hasMetadata("tppending")) {
				for (MetadataValue mdv : player.getMetadata("tppending")) {
					if (mdv.getOwningPlugin().equals(CrossServerLib.get())) {
						CrossServerTeleportEvent event = (CrossServerTeleportEvent) mdv.value();
						if (System.currentTimeMillis() - created > 120000L) {
							player.sendMessage(String.format("%sError: %sTeleport request has timed out.",
									new Object[] { ChatColor.RED, ChatColor.DARK_RED, ChatColor.GOLD }));
						}
						created = event.created;
						fromPlayer = event.fromPlayer;
						toPlayer = event.toPlayer;
						source = event.source;
						target = event.target;
						fromLocation = event.fromLocation;
						toLocation = event.toLocation;
						orginServer = event.orginServer;
						if (type != TeleportType.TELEPORT_DENY) {
							this.type = (type == TeleportType.TELEPORT_HERE_ACCEPT) ? TeleportType.PLAYER_TPHERE : TeleportType.PLAYER_TP;
						}
						return;
					}
				}
			}
			player.sendMessage(String.format("%sError: %sYou do not have a pending request.",
					new Object[] { ChatColor.RED, ChatColor.DARK_RED, ChatColor.GOLD }));

			break;
		case TELEPORT_CANCEL:
			if (player.hasMetadata("tprequest")) {
				for (MetadataValue mdv : player.getMetadata("tprequest")) {
					if (mdv.getOwningPlugin().equals(CrossServerLib.get())) {
						CrossServerTeleportEvent event = (CrossServerTeleportEvent) mdv.value();
						created = event.created;
						fromPlayer = event.fromPlayer;
						toPlayer = event.toPlayer;
						source = event.source;
						target = event.target;
						fromLocation = event.fromLocation;
						toLocation = event.toLocation;
						orginServer = event.orginServer;
						break;
					}
				}
				return;
			}
			player.sendMessage(String.format("%sError: %sYou do not have a pending request.",
					new Object[] { ChatColor.RED, ChatColor.DARK_RED, ChatColor.GOLD }));
			break;
		default:
			break;
		}

	}

	public CrossServerTeleportEvent(Player from, TeleportLocation to) {
		type = TeleportType.TO_LOCATION;
		orginServer = CrossServerAPI.getServer();
		source = from.getName().toLowerCase();
		fromPlayer = source;
		fromLocation = new TeleportLocation(from.getLocation());
		toLocation = to;
	}

	public CrossServerTeleportEvent(JsonObject o) {
		deSerialize(o);
	}

	public JsonObject serialize() {
		JsonObject o = new JsonObject();
		o.addProperty("created", Long.valueOf(created));
		o.addProperty("type", type.toString());
		o.addProperty("fromPlayer", fromPlayer);
		o.addProperty("toPlayer", toPlayer);
		o.addProperty("orginServer", orginServer);
		o.addProperty("source", source);
		o.addProperty("target", target);
		o.add("fromLocation", fromLocation.getObject());
		o.add("toLocation", toLocation.getObject());
		return o;
	}

	public void deSerialize(JsonObject o) {
		created = o.get("created").getAsLong();
		type = TeleportType.lookup(o.get("type").getAsString());
		fromPlayer = o.get("fromPlayer").getAsString();
		toPlayer = o.get("toPlayer").getAsString();
		orginServer = o.get("orginServer").getAsString();
		source = o.get("source").getAsString();
		target = o.get("target").getAsString();

		fromLocation = new TeleportLocation(o.get("fromLocation").getAsJsonObject());
		toLocation = new TeleportLocation(o.get("toLocation").getAsJsonObject());
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public TeleportPacket getPacket() {
		return new TeleportPacket(this);
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancel) {
		cancelled = cancel;
	}

	public boolean teleport() {
		final Player source;
		switch (type) {
			case HOME:
			case SPAWN:
			case PWARP:
			case TELEPORT_ACCEPT:
			case PLAYER_TP:
			case PLAYER_TPHERE:
				if (isLocal()) {
					Player from = Bukkit.getPlayer(fromPlayer);
					Player to = Bukkit.getPlayer(toPlayer);
					source = Bukkit.getPlayer(this.source);
					Player target = Bukkit.getPlayer(this.target);
					if ((from != null) && (to != null)) {
						if (from.hasMetadata("tprequest")) {
							from.removeMetadata("tprequest", CrossServerLib.get());
						}
						if (to.hasMetadata("tppending")) {
							to.removeMetadata("tppending", CrossServerLib.get());
						}
						if (this.source != fromPlayer) {
							fromLocation = new TeleportLocation(source.getLocation());
						}
						source.setMetadata("tpbacklocation", new FixedMetadataValue(CrossServerLib.get(), fromLocation));
						source.sendMessage(String.format("%sTeleporting...", new Object[] { ChatColor.GOLD }));
						source.setMetadata("tpactive", new FixedMetadataValue(CrossServerLib.get(), Boolean.valueOf(true)));
						boolean ret = source.teleport(target);
						Bukkit.getScheduler().runTaskLater(CrossServerLib.get(), new Runnable() {
							public void run() {
								source.removeMetadata("tpactive", CrossServerLib.get());
							}
						}, 20L);
						return ret;
					}
					return false;
				}
	
				if (CrossServerAPI.localOnline(this.target)) {
					ProxyPlayer from = CrossServerAPI.findPlayer(this.source);
					cache.put(from.getUuid(), this);
				} else if (CrossServerAPI.localOnline(this.source)) {
					Player from = Bukkit.getPlayer(this.source);
					Util.connect(from, CrossServerAPI.findPlayer(this.target).getServer());
				}
		break;
			case TELEPORT_HERE_ACCEPT:
			case TO_LOCATION:
			case UNKNOWN:
				if (isLocal()) {
					if (CrossServerAPI.localOnline(fromPlayer)) {
						final Player from = Bukkit.getPlayer(this.source);
						from.sendMessage(String.format("%sTeleporting...", new Object[] { ChatColor.GOLD }));
						from.setMetadata("tpbacklocation", new FixedMetadataValue(CrossServerLib.get(), fromLocation));
						from.setMetadata("tpactive", new FixedMetadataValue(CrossServerLib.get(), Boolean.valueOf(true)));
						boolean ret = from.teleport(toLocation.getLocation());
						Bukkit.getScheduler().runTaskLater(CrossServerLib.get(), new Runnable() {
							public void run() {
								from.removeMetadata("tpactive", CrossServerLib.get());
							}
						}, 20L);
						return ret;
					}
					ProxyPlayer from = CrossServerAPI.findPlayer(fromPlayer);
					cache.put(from.getUuid(), this);
				} else if (CrossServerAPI.localOnline(fromPlayer)) {
					Player from = Bukkit.getPlayer(fromPlayer);
					Server.client.send(new CrossServerBackLocationEvent(from, toLocation.getServer()).getPacket());
					Util.connect(from, toLocation.getServer());
				}
		break;
			case TELEPORT_REQUEST:
				if (CrossServerAPI.localOnline(fromPlayer)) {
					Player from = Bukkit.getPlayer(fromPlayer);
					ProxyPlayer to = CrossServerAPI.findPlayer(toPlayer);
					from.sendMessage(String.format("%sRequest sent to %s%s%s.",
							new Object[] { ChatColor.GOLD, to.getColor(), to.getName(), ChatColor.GOLD }));
					from.sendMessage(String.format("%sTo cancel this request, type %s/tpacancel%s.",
							new Object[] { ChatColor.GOLD, ChatColor.RED, ChatColor.GOLD }));
					from.setMetadata("tprequest", new FixedMetadataValue(CrossServerLib.get(), this));
				}
				if (CrossServerAPI.localOnline(toPlayer)) {
					ProxyPlayer from = CrossServerAPI.findPlayer(fromPlayer);
					Player to = Bukkit.getPlayer(toPlayer);
					to.sendMessage(String.format("%s%s%s has requested to teleport to you.", from.getColor(), from.getName(), ChatColor.GOLD ));
					to.sendMessage(String.format("%sTo Teleport, type %s/tpaccept%s.", ChatColor.GOLD, ChatColor.RED, ChatColor.GOLD ));
					to.sendMessage(String.format("%sTo To deny this request, type %s/tpdeny%s.", ChatColor.GOLD, ChatColor.RED, ChatColor.GOLD ));
					to.sendMessage(String.format("%sThis request will timeout after %s120 seconds%s.", ChatColor.GOLD, ChatColor.RED, ChatColor.GOLD ));
					to.setMetadata("tppending", new FixedMetadataValue(CrossServerLib.get(), this));
					return true;
				}
			break;
			case TELEPORT_HERE_REQUEST:
				if (CrossServerAPI.localOnline(fromPlayer)) {
					Player from = Bukkit.getPlayer(fromPlayer);
					ProxyPlayer to = CrossServerAPI.findPlayer(toPlayer);
					from.sendMessage(String.format("%sRequest sent to %s%s%s.", ChatColor.GOLD, to.getColor(), to.getName(), ChatColor.GOLD ));
					from.sendMessage(String.format("%sTo cancel this request, type %s/tpacancel%s.", ChatColor.GOLD, ChatColor.RED, ChatColor.GOLD));
					from.setMetadata("tprequest", new FixedMetadataValue(CrossServerLib.get(), this));
				}
				if (CrossServerAPI.localOnline(toPlayer)) {
					ProxyPlayer from = CrossServerAPI.findPlayer(fromPlayer);
					Player to = Bukkit.getPlayer(toPlayer);
					to.sendMessage(String.format("%s%s%s has requested that you teleport to them.", from.getColor(), from.getName(), ChatColor.GOLD));
					to.sendMessage(String.format("%sTo Teleport, type %s/tpaccept%s.", ChatColor.GOLD, ChatColor.RED, ChatColor.GOLD));
					to.sendMessage(String.format("%sTo To deny this request, type %s/tpdeny%s.", ChatColor.GOLD, ChatColor.RED, ChatColor.GOLD));
					to.sendMessage(String.format("%sThis request will timeout after %s120 seconds%s.", ChatColor.GOLD, ChatColor.RED, ChatColor.GOLD ));
					to.setMetadata("tppending", new FixedMetadataValue(CrossServerLib.get(), this));
					return true;
				}
			break;
			case TELEPORT_DENY:
				CrossServerTeleportEvent event;
				if (CrossServerAPI.localOnline(toPlayer)) {
					
					Player to = Bukkit.getPlayer(toPlayer);
					to.sendMessage(String.format("%sTeleport request denied.", ChatColor.GOLD ));
					if (to.hasMetadata("tppending")) {
						for (MetadataValue mdv : to.getMetadata("tppending")) {
							if (mdv.getOwningPlugin().equals(CrossServerLib.get())) {
								event = (CrossServerTeleportEvent) mdv.value();
								if (event.target.toLowerCase().equals(target.toLowerCase())) {
									to.removeMetadata("tppending", CrossServerLib.get());
								}
							}
						}
					}
				}
				if (CrossServerAPI.localOnline(fromPlayer)) {
					Player from = Bukkit.getPlayer(fromPlayer);
					ProxyPlayer to = CrossServerAPI.findPlayer(toPlayer);
					from.sendMessage(String.format("%s%s%s denied your teleport request.",
							new Object[] { to.getColor(), to.getName(), ChatColor.GOLD }));
					if (from.hasMetadata("tprequest")) {
						for (MetadataValue mdv : from.getMetadata("tprequest")) {
							if (mdv.getOwningPlugin().equals(CrossServerLib.get())) {
								event = (CrossServerTeleportEvent) mdv.value();
								if (event.fromPlayer.toLowerCase().equals(fromPlayer.toLowerCase())) {
									from.removeMetadata("tprequest", CrossServerLib.get());
								}
							}
						}
					}
					return true;
				}
			break;
			case TELEPORT_CANCEL:
				if (CrossServerAPI.localOnline(fromPlayer)) {
					Player from = Bukkit.getPlayer(fromPlayer);
					from.sendMessage(String.format("%sAll outstanding teleport requests canceled.",
							new Object[] { ChatColor.GOLD }));
					if (from.hasMetadata("tprequest")) {
						from.removeMetadata("tprequest", CrossServerLib.get());
					}
				}
	
				if (CrossServerAPI.localOnline(toPlayer)) {
					ProxyPlayer from = CrossServerAPI.findPlayer(fromPlayer);
					Player to = Bukkit.getPlayer(toPlayer);
					if (to.hasMetadata("tppending")) {
						to.sendMessage(String.format("%s%s%s has canceled their teleport request.",
								new Object[] { from.getColor(), from.getName(), ChatColor.GOLD }));
						to.removeMetadata("tppending", CrossServerLib.get());
					}
				}
		break;
			default:
		break;
		}

		return false;
	}

	public boolean isLocal() {
		switch (type) {
			case HOME:
			case PLAYER_TP:
			case PLAYER_TPHERE:
			case PWARP:
			case SPAWN:
			case TELEPORT_CANCEL:
				if ((CrossServerAPI.localOnline(fromPlayer)) && (CrossServerAPI.localOnline(toPlayer))) {
					return true;
				}
			break;
			case TELEPORT_HERE_ACCEPT:
			case TELEPORT_HERE_REQUEST:
			case TELEPORT_REQUEST:
			case TO_LOCATION:
				if (CrossServerAPI.isServer(toLocation.getServer())) {
					return true;
				}
			break;
			case UNKNOWN:
				return true;
			default:
				return false;
				
		}
		return false;
	}

	public static CrossServerTeleportEvent getCache(UUID uuid) {
		return (CrossServerTeleportEvent) cache.get(uuid);
	}

	public static void removeCache(UUID uuid) {
		cache.remove(uuid);
	}
}
