package io.dblosevn.xserver.bukkit.events.global;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.dblosevn.xserver.bukkit.chat.ChatSourceObject;
import io.dblosevn.xserver.bukkit.objects.CrossServerEvent;
import io.dblosevn.xserver.bukkit.sockets.packets.ChatPacket;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class CrossServerChatEvent
extends CrossServerEvent
implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    protected ChatSourceObject source;
    private String message;
    private boolean cancelled;

    public CrossServerChatEvent() {
    }

    public CrossServerChatEvent(Player p, String channel, String message) {
        this.source = new ChatSourceObject(p, channel);
        if (!this.source.isColorMessages()) {
            message = ChatColor.stripColor((String)ChatColor.translateAlternateColorCodes((char)'&', (String)message));
        }
        this.message = message;
    }

    public CrossServerChatEvent(CommandSender p, String channel, String message) {
        this.source = new ChatSourceObject(p, channel);
        if (!this.source.isColorMessages()) {
            message = ChatColor.stripColor((String)ChatColor.translateAlternateColorCodes((char)'&', (String)message));
        }
        this.message = message;
    }

    public CrossServerChatEvent(JsonObject o) {
        this.deSerialize(o);
    }

    @Override
    public JsonObject serialize() {
        JsonObject o = new JsonObject();
        o.addProperty("message", this.message);
        o.add("source", (JsonElement)this.source.serialize());
        return o;
    }

    @Override
    public void deSerialize(JsonObject o) {
        this.message = o.get("message").getAsString();
        this.source = new ChatSourceObject(o.get("source").getAsJsonObject());
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public ChatPacket getPacket() {
        return new ChatPacket(this);
    }

    public ChatSourceObject getSender() {
        return this.source;
    }

    public String getMessage() {
        return this.message;
    }

    public boolean isCancelled() {
        return this.cancelled;
    }

    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }
}

