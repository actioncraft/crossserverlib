package io.dblosevn.xserver.bukkit.events.global;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.dblosevn.xserver.bukkit.chat.ChatSourceObject;
import io.dblosevn.xserver.bukkit.events.global.CrossServerChatEvent;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

public class CrossServerPMEvent
extends CrossServerChatEvent {
    private static final HandlerList handlers = new HandlerList();
    private ChatSourceObject recipient;
    private String message;

    public CrossServerPMEvent() {
    }

    public CrossServerPMEvent(Player sender, String recipient, String channel, String message) {
        this.source = new ChatSourceObject(sender, channel);
        this.recipient = new ChatSourceObject(recipient, channel);
        this.message = message = ChatColor.stripColor((String)ChatColor.translateAlternateColorCodes((char)'&', (String)message));
    }

    public CrossServerPMEvent(JsonObject o) {
        this.deSerialize(o);
    }

    @Override
    public JsonObject serialize() {
        JsonObject o = new JsonObject();
        o.addProperty("message", this.message);
        o.add("source", (JsonElement)this.source.serialize());
        o.add("recipient", (JsonElement)this.recipient.serialize());
        return o;
    }

    @Override
    public void deSerialize(JsonObject o) {
        this.message = o.get("message").getAsString();
        this.source = new ChatSourceObject(o.get("source").getAsJsonObject());
        this.recipient = new ChatSourceObject(o.get("recipient").getAsJsonObject());
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public ChatSourceObject getRecipient() {
        return this.recipient;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}

