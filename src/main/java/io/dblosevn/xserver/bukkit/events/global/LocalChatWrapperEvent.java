package io.dblosevn.xserver.bukkit.events.global;

import java.util.ArrayList;
import java.util.HashSet;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class LocalChatWrapperEvent
extends AsyncPlayerChatEvent {
    public LocalChatWrapperEvent(boolean async, Player who, String message, ArrayList<Player> recipients) {
        super(async, who, message, new HashSet<Player>(recipients));
    }
}

