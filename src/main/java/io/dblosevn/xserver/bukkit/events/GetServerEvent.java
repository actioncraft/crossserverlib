package io.dblosevn.xserver.bukkit.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GetServerEvent
extends Event {
    private static final HandlerList handlers = new HandlerList();
    private String serverName;

    public GetServerEvent(String serverName) {
        this.serverName = serverName;
    }

    public String getServer() {
        return this.serverName;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}

