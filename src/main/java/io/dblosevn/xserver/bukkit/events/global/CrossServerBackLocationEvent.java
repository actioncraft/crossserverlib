package io.dblosevn.xserver.bukkit.events.global;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.dblosevn.xserver.bukkit.objects.CrossServerEvent;
import io.dblosevn.xserver.bukkit.objects.TeleportLocation;
import io.dblosevn.xserver.bukkit.sockets.packets.BackPacket;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

public class CrossServerBackLocationEvent
extends CrossServerEvent {
    private static ConcurrentHashMap<UUID, TeleportLocation> cache = new ConcurrentHashMap<UUID, TeleportLocation>();
    private static final HandlerList handlers = new HandlerList();
    private String server = "";
    private String source = "";
    private UUID sourceUUID = null;
    private TeleportLocation location;

    public CrossServerBackLocationEvent() {
    }

    public CrossServerBackLocationEvent(Player source, String targetServer) {
        this.source = source.getName();
        this.sourceUUID = source.getUniqueId();
        this.server = targetServer;
        this.location = new TeleportLocation(source.getLocation());
    }

    public CrossServerBackLocationEvent(JsonObject o) {
        this.deSerialize(o);
    }

    @Override
    public JsonObject serialize() {
        JsonObject o = new JsonObject();
        o.addProperty("server", this.server);
        o.addProperty("source", this.source);
        o.addProperty("sourceUUID", this.sourceUUID.toString());
        o.add("location", (JsonElement)this.location.getObject());
        return o;
    }

    @Override
    public void deSerialize(JsonObject o) {
        this.server = o.get("server").getAsString();
        this.source = o.get("source").getAsString();
        this.sourceUUID = UUID.fromString(o.get("sourceUUID").getAsString());
        this.location = new TeleportLocation(o.get("location").getAsJsonObject());
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public BackPacket getPacket() {
        return new BackPacket(this);
    }

    public String getServer() {
        return this.server;
    }

    public String getSource() {
        return this.source;
    }

    public UUID getSourceUUID() {
        return this.sourceUUID;
    }

    public TeleportLocation getLocation() {
        return this.location;
    }

    public static TeleportLocation getCache(UUID uuid) {
        return cache.get(uuid);
    }

    public static void removeCache(UUID uuid) {
        cache.remove(uuid);
    }

    public static void setCache(UUID uuid, TeleportLocation loc) {
        cache.put(uuid, loc);
    }
}

