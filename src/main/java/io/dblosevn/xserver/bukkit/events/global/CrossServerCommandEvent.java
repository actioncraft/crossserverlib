package io.dblosevn.xserver.bukkit.events.global;

import com.google.gson.JsonObject;
import io.dblosevn.xserver.bukkit.objects.CrossServerEvent;
import io.dblosevn.xserver.bukkit.sockets.packets.CommandPacket;
import org.bukkit.command.CommandSender;
import org.bukkit.event.HandlerList;

public class CrossServerCommandEvent
extends CrossServerEvent {
    private static final HandlerList handlers = new HandlerList();
    private String command;
    private String sender;

    public CrossServerCommandEvent() {
    }

    public CrossServerCommandEvent(CommandSender sender, String command) {
        this.command = command;
        this.sender = sender.getName();
    }

    public CrossServerCommandEvent(JsonObject o) {
        this.deSerialize(o);
    }

    @Override
    public JsonObject serialize() {
        JsonObject o = new JsonObject();
        o.addProperty("command", this.command);
        o.addProperty("sender", this.sender);
        return o;
    }

    @Override
    public void deSerialize(JsonObject o) {
        this.command = o.get("command").getAsString();
        this.sender = o.get("sender").getAsString();
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public CommandPacket getPacket() {
        return new CommandPacket(this);
    }

    public String getSender() {
        return this.sender;
    }

    public String getCommand() {
        return this.command;
    }
}

