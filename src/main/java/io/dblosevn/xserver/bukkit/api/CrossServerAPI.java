package io.dblosevn.xserver.bukkit.api;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.objects.ProxyPlayer;
import io.netty.util.internal.ConcurrentSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class CrossServerAPI {
	private static ConcurrentHashMap<String, ConcurrentSet<ProxyPlayer>> serversToPlayers = new ConcurrentHashMap<String, ConcurrentSet<ProxyPlayer>>();
	private static ConcurrentHashMap<UUID, ProxyPlayer> uuidsToPlayer = new ConcurrentHashMap<UUID, ProxyPlayer>();
	private static ConcurrentHashMap<String, ProxyPlayer> playernamesToPlayer = new ConcurrentHashMap<String, ProxyPlayer>();
	private static ConcurrentHashMap<String, ProxyPlayer> nicksToPlayer = new ConcurrentHashMap<String, ProxyPlayer>();
	private static String server = CrossServerLib.get().getConfig().getString("serverName");

	public static void refreshStatus(ConcurrentHashMap<String, ConcurrentSet<ProxyPlayer>> serversToPlayers,
			ConcurrentHashMap<UUID, ProxyPlayer> uuidsToPlayer,
			ConcurrentHashMap<String, ProxyPlayer> playernamesToPlayer) {
		CrossServerAPI.serversToPlayers = serversToPlayers;
		CrossServerAPI.uuidsToPlayer = uuidsToPlayer;
		CrossServerAPI.playernamesToPlayer = playernamesToPlayer;
		nicksToPlayer.clear();
		for (ProxyPlayer p: playernamesToPlayer.values()) {
			if (!p.getNick().isEmpty()) {
				nicksToPlayer.put(p.getNick(), p);
			}
		}
	}

	public static ProxyPlayer findPlayer(UUID uuid) {
		return uuidsToPlayer.get(uuid);
	}

	public static ProxyPlayer findPlayer(String name) {
		if (nicksToPlayer.containsKey(name.toLowerCase())) {
			return nicksToPlayer.get(name.toLowerCase());
		}
		if (playernamesToPlayer.containsKey(name.toLowerCase())) {
			return playernamesToPlayer.get(name.toLowerCase());
		}
		return null;
	}

	public static Set<ProxyPlayer> getServerPlayers(String server) {
		return Collections.unmodifiableSet(serversToPlayers.get(server.toLowerCase()));
	}

	public static Set<String> getAllPlayerNames() {
		
		return Collections.unmodifiableSet(playernamesToPlayer.keySet());
	}

	public static ArrayList<String> getServers() {
		return new ArrayList<String>(serversToPlayers.keySet());
	}

	public static boolean online(String playername) {
		if (nicksToPlayer.get(playername.toLowerCase()) != null) {
			return true;
		}
		if (playernamesToPlayer.get(playername.toLowerCase()) != null) {
			return true;
		}
		return false;
	}

	public static boolean online(UUID uuid) {
		if (uuidsToPlayer.get(uuid) != null) {
			return true;
		}
		return false;
	}

	public static boolean localOnline(String playername) {
		ProxyPlayer pPlayer = findPlayer(playername);
		if (null != pPlayer) {
			for (Player p : Bukkit.getOnlinePlayers()) {
				if (!p.getName().toLowerCase().equals(pPlayer.getName().toLowerCase()))
					continue;
				return true;
			}
		}
		return false;
	}

	public static boolean localOnline(UUID uuid) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.getUniqueId().compareTo(uuid) != 0)
				continue;
			return true;
		}
		return false;
	}

	public static String getServer() {
		return server;
	}

	public static boolean isServer(String server) {
		return CrossServerAPI.server.toLowerCase().equals(server.toLowerCase());
	}
}
