package io.dblosevn.xserver.bukkit.api;

import io.dblosevn.xserver.bukkit.api.DB;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public class DBSchema {
	private static ArrayList<String> tables = new ArrayList<String>();
	private static String prefix = DB.getPrefix();

	static {
		tables.add(
				"CREATE TABLE If Not Exists `%prefix%bans` ( `ban_player_id` int(11) DEFAULT NULL, `ban_staff_id` int(11) DEFAULT NULL, `ban_time` bigint(20) DEFAULT NULL, `ban_type` enum('TEMP','PERM') DEFAULT 'PERM', `ban_length` bigint(20) DEFAULT '0', `ban_reason` varchar(255) DEFAULT NULL, KEY `NewIndex1` (`ban_player_id`), KEY `NewIndex2` (`ban_staff_id`) )");
		tables.add(
				"CREATE TABLE If Not Exists `%prefix%homes` ( `home_id` int(11) NOT NULL AUTO_INCREMENT, `home_player_id` int(11) DEFAULT NULL, `home_name` varchar(50) DEFAULT NULL, `home_server` varchar(50) DEFAULT NULL, `home_world` varchar(50) DEFAULT NULL, `home_x` float DEFAULT NULL, `home_y` float DEFAULT NULL, `home_z` float DEFAULT NULL, `home_yaw` float DEFAULT NULL, `home_pitch` float DEFAULT NULL, PRIMARY KEY (`home_id`), UNIQUE KEY `NewIndex1` (`home_player_id`,`home_name`) )");
		tables.add(
				"CREATE TABLE If Not Exists `%prefix%logins` ( `player_id` int(11) DEFAULT NULL, `player_join_time` bigint(20) DEFAULT NULL, `player_ip` varchar(15) DEFAULT NULL, `player_name` varchar(16) DEFAULT NULL, KEY `NewIndex1` (`player_id`) )");
		tables.add(
				"CREATE TABLE If Not Exists `%prefix%players` ( `player_id` int(11) NOT NULL AUTO_INCREMENT, `player_uuid_lower` bigint(20) NOT NULL, `player_uuid_upper` bigint(20) NOT NULL, `player_uuid` varchar(36) NOT NULL, `player_name` varchar(16) DEFAULT '', `player_join_date` bigint(20) DEFAULT NULL, `player_last_login` bigint(20) DEFAULT '0', `player_last_logout` bigint(20) DEFAULT '0', `player_rank` varchar(20) DEFAULT '', `player_time` bigint(20) DEFAULT '0', `player_last_server` varchar(20) DEFAULT '', `player_last_ip` varchar(15) DEFAULT '', `player_nick` varchar(20) DEFAULT '', PRIMARY KEY (`player_id`), UNIQUE KEY `NewIndex1` (`player_uuid_lower`,`player_uuid_upper`) )");
		tables.add(
				"CREATE TABLE If Not Exists `%prefix%pwarps` ( `warp_id` int(11) NOT NULL AUTO_INCREMENT, `warp_player_id` int(11) DEFAULT NULL, `warp_cost` int(11) DEFAULT NULL, `warp_last_payment` int(11) DEFAULT NULL, `warp_status` enum('ACTIVE','PENDING_DELETE') DEFAULT 'ACTIVE', `warp_name` varchar(50) DEFAULT NULL, `warp_server` varchar(50) DEFAULT NULL, `warp_world` varchar(50) DEFAULT NULL, `warp_x` float DEFAULT NULL, `warp_y` float DEFAULT NULL, `warp_z` float DEFAULT NULL, `warp_yaw` float DEFAULT NULL, `warp_pitch` float DEFAULT NULL, PRIMARY KEY (`warp_id`), UNIQUE KEY `NewIndex1` (`warp_name`))");
		tables.add(
				"CREATE TABLE If Not Exists `%prefix%request` ( `request_id` int(11) NOT NULL AUTO_INCREMENT, `request_player_id` int(11) DEFAULT NULL, `request_staff_id` int(11) DEFAULT NULL, `request_time` bigint(20) DEFAULT NULL, `request_type` enum('MODREQ','REPORT') DEFAULT 'MODREQ', `request_status` enum('OPEN','CLAIMED','CLOSED') DEFAULT 'OPEN', `request_comment` text, PRIMARY KEY (`request_id`), KEY `NewIndex1` (`request_player_id`), KEY `NewIndex2` (`request_staff_id`) )");
		tables.add(
				"CREATE TABLE If Not Exists `%prefix%spawns` ( `spawn_id` int(11) NOT NULL AUTO_INCREMENT, `spawn_group` varchar(50) DEFAULT 'default', `spawn_server` varchar(50) DEFAULT NULL, `spawn_world` varchar(50) DEFAULT NULL, `spawn_x` float DEFAULT NULL, `spawn_y` float DEFAULT NULL, `spawn_z` float DEFAULT NULL, `spawn_yaw` float DEFAULT NULL, `spawn_pitch` float DEFAULT NULL, PRIMARY KEY (`spawn_id`), UNIQUE KEY `NewIndex1` (`spawn_group`,`spawn_server`) )");
		tables.add(
				"CREATE TABLE If Not Exists `%prefix%warps` ( `warp_id` int(11) NOT NULL AUTO_INCREMENT, `warp_name` varchar(50) DEFAULT NULL, `warp_enabled` tinyint(1) DEFAULT '1', `warp_server` varchar(50) DEFAULT NULL, `warp_world` varchar(50) DEFAULT NULL, `warp_x` float DEFAULT NULL, `warp_y` float DEFAULT NULL, `warp_z` float DEFAULT NULL, `warp_yaw` float DEFAULT NULL, `warp_pitch` float DEFAULT NULL, PRIMARY KEY (`warp_id`), UNIQUE KEY `NewIndex1` (`warp_name`))");
	}

	public static void createTables() {
		Connection connection = DB.connect();
		for (String table : tables) {
			try {
				connection.prepareStatement(table.replace("%prefix%", prefix)).execute();
			} catch (SQLException sQLException) {
				// empty catch block
			}
		}
		DB.close(connection);
	}

	protected static String getPrefix() {
		return prefix;
	}

	public static String p(String sql) {
		return sql.replace("%prefix%", prefix);
	}

	public static enum Tables {
		PLAYERS("%prefix%players"), LOGINS("%prefix%logins"), WARPS("%prefix%warps"), SPAWNS("%prefix%spawns"),
		PWARPS("%prefix%pwarps"), HOMES("%prefix%homes"), REQUEST("%prefix%request"), BANS("%prefix%bans");

		private final String tableName;

		private Tables(String name) {
			tableName = name.replace("%prefix%", DBSchema.prefix);
		}

		public String toString() {
			return this.tableName;
		}
	}

}
