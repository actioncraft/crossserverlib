package io.dblosevn.xserver.bukkit.api;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.dblosevn.xserver.bukkit.CrossServerLib;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DB {
	private static HikariDataSource db = null;
	private static String prefix;

	public static boolean initialize() throws Exception {
		if (db == null) {
			db = new HikariDataSource(DB.getDBConfig());
			if (db == null) {
				throw new Exception("Could not connect to database.");
			}
			try {
				db.getConnection().close();
				return true;
			} catch (Exception e) {
				db = null;
				throw new Exception("Invalid database connection.  Disabling plugin...");
			}
		}
		return true;
	}

	public static boolean initialize(String host, int port, String database, String username, String password,
			String prefix) throws Exception {
		if (db == null) {
			DB.prefix = prefix;
			db = new HikariDataSource(DB.getProps(host, port, database, username, password));
			if (db == null) {
				throw new Exception("Could not connect to database.");
			}
			try {
				db.getConnection().close();
				return true;
			} catch (Exception e) {
				db = null;
				throw new Exception("Invalid database connection.  Disabling plugin...");
			}
		}
		return true;
	}

	private static HikariConfig getDBConfig() {
		String host = CrossServerLib.get().getConfig().getString("db.host", "localhost");
		int port = CrossServerLib.get().getConfig().getInt("db.port", 3306);
		String database = CrossServerLib.get().getConfig().getString("db.database", "game");
		String username = CrossServerLib.get().getConfig().getString("db.user", "root");
		String password = CrossServerLib.get().getConfig().getString("db.pass", "");
		prefix = CrossServerLib.get().getConfig().getString("db.prefix", "game_");
		return DB.getProps(host, port, database, username, password);
	}

	private static HikariConfig getProps(String host, int port, String database, String username, String password) {
		Properties props = new Properties();
		props.setProperty("jdbcUrl",
				String.format("jdbc:mysql://%s:%d/%s?autoReconnect=true&useSSL=false", host, port, database));
		props.setProperty("username", username);
		props.setProperty("password", password);
		props.setProperty("dataSource.cachePrepStmts", "true");
		props.setProperty("dataSource.prepStmtCacheSize", "250");
		props.setProperty("dataSource.prepStmtCacheSqlLimit", "2048");
		props.setProperty("dataSource.useServerPrepStmts", "true");
		props.setProperty("dataSource.useLocalSessionState", "true");
		props.setProperty("dataSource.useLocalTransactionState", "true");
		props.setProperty("dataSource.rewriteBatchedStatements", "true");
		props.setProperty("dataSource.cacheResultSetMetadata", "true");
		props.setProperty("dataSource.cacheServerConfiguration", "true");
		props.setProperty("dataSource.elideSetAutoCommits", "true");
		props.setProperty("dataSource.maintainTimeStats", "false");
		HikariConfig config = new HikariConfig(props);
		return config;
	}

	public static void close() {
		if (!db.isClosed()) {
			db.close();
		}
	}

	public static Connection connect() {
		if (db != null) {
			try {
				return db.getConnection();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	public static void close(Connection connection) {
		DB.close(null, null, connection);
	}

	public static void close(Statement statement, Connection connection) {
		DB.close(null, statement, connection);
	}

	public static void close(ResultSet resultSet, Connection connection) {
		DB.close(resultSet, null, connection);
	}

	public static void close(ResultSet resultSet, Statement statement, Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (Exception exception) {
				// empty catch block
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (Exception exception) {
				// empty catch block
			}
		}
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (Exception exception) {
				// empty catch block
			}
		}
	}

	public static String getPrefix() {
		return prefix;
	}
}
