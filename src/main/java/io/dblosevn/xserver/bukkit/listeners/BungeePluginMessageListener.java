package io.dblosevn.xserver.bukkit.listeners;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.events.GetServerEvent;
import io.dblosevn.xserver.bukkit.events.GetServersEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class BungeePluginMessageListener implements PluginMessageListener {
	CrossServerLib plugin;

	public BungeePluginMessageListener(CrossServerLib plugin) {
		this.plugin = plugin;
		plugin.getServer().getMessenger().registerOutgoingPluginChannel(plugin, "BungeeCord");
		plugin.getServer().getMessenger().registerIncomingPluginChannel(plugin, "BungeeCord", this);
	}

	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if (!channel.equals("BungeeCord")) {
			return;
		}
		ByteArrayDataInput in = ByteStreams.newDataInput(message);
		String subchannel = in.readUTF();
		System.out.println("Bungee: " + subchannel);
		if (subchannel.equals("GetServer")) {
			String serverName = in.readUTF();
			this.plugin.getServer().getPluginManager().callEvent((Event) new GetServerEvent(serverName));
		}
		if (subchannel.equals("GetServers")) {
			String serverList = in.readUTF();
			this.plugin.getServer().getPluginManager().callEvent((Event) new GetServersEvent(serverList));
		}
	}

	public void getServer() {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("GetServer");
		this.plugin.getServer().sendPluginMessage((Plugin) this.plugin, "BungeeCord", out.toByteArray());
	}

	public void getServers() {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("GetServers");
		this.plugin.getServer().sendPluginMessage((Plugin) this.plugin, "BungeeCord", out.toByteArray());
	}
}
