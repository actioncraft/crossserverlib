package io.dblosevn.xserver.bukkit.listeners;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.objects.ProxyPlayer;
import io.dblosevn.xserver.bukkit.objects.SQL;
import io.dblosevn.xserver.bukkit.util.PermUtil;
import me.lucko.luckperms.api.Group;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.event.EventBus;
import me.lucko.luckperms.api.event.user.UserDataRecalculateEvent;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.bringholm.nametagchanger.NameTagChanger;

public class ColorListener implements Listener {
	LuckPermsApi api;
	private static me.lucko.luckperms.api.event.EventHandler<UserDataRecalculateEvent> recalculateEvent = null;
	
	public ColorListener() {
		api = CrossServerLib.getPerms();
		EventBus eventBus = api.getEventBus();
		recalculateEvent = eventBus.subscribe(UserDataRecalculateEvent.class, e -> {
	    	String server = CrossServerLib.get().getConfig().getString("serverName", "Lobby");
	    	
	        SQL.playerRank(e.getUser().getUuid(), PermUtil.getGroupForServer(e.getUser(), server));
			ProxyPlayer p = new ProxyPlayer("", e.getUser().getUuid().toString(), "");
			if (CrossServerAPI.localOnline(e.getUser().getUuid())) {
				Player player = Bukkit.getPlayer(e.getUser().getUuid());
				if ( p.getNick().isEmpty()) {
					NameTagChanger.INSTANCE.resetPlayerName(player);
				} else {
					NameTagChanger.INSTANCE.changePlayerName(player, p.getColor() + p.getDisplayName());
				}
			}
			setTabColors();
		});
	}
	public static void onDisable() {
		if (null != recalculateEvent) {
			if (recalculateEvent.isActive()) {
				recalculateEvent.unregister();
			}
		}
	}

	@EventHandler
	public void onSetColor(PlayerJoinEvent e) {
		ColorListener.setTabColors();
	}

	public static void setTabColors() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			ColorListener.setTabColorPlayer(p);
		}
	}

	@SuppressWarnings("deprecation")
	private static void setTabColorPlayer(Player player) {
		String server = CrossServerLib.get().getConfig().getString("serverName", "Lobby");

		if (player.getScoreboard() == null) {
			player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		}

		Scoreboard scoreboard = player.getScoreboard();

		for (Group g : CrossServerLib.getPerms().getGroups()) {
			Team t = scoreboard.getTeam(g.getName()) == null ? scoreboard.registerNewTeam(g.getName())
					: scoreboard.getTeam(g.getName());
			String color = "&" + PermUtil.getColorForGroup(g.getName());
			t.setPrefix(ChatColor.translateAlternateColorCodes((char) '&', (String) color));
		}
		for (Player p : Bukkit.getOnlinePlayers()) {
			String group = PermUtil.getGroupForServer(PermUtil.getUser(p), server);
			ProxyPlayer pPlayer = new ProxyPlayer(p.getName());
			p.setPlayerListName(pPlayer.getColor() + pPlayer.getDisplayName());
			scoreboard.getTeam(group).addPlayer(p);
		}
	}
}
