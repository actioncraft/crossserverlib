package io.dblosevn.xserver.bukkit.listeners;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.events.global.CrossServerCommandEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class CommandListener
implements Listener {
    @EventHandler
    public void onSyncCommand(CrossServerCommandEvent e) {
    	Bukkit.getScheduler().runTask(CrossServerLib.get(), new Runnable() {
			@Override
			public void run() {
		        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), e.getCommand());
			}
		});
    }
}

