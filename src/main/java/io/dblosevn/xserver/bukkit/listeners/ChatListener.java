package io.dblosevn.xserver.bukkit.listeners;

import io.dblosevn.xserver.bukkit.chat.ChatSourceObject;
import io.dblosevn.xserver.bukkit.commands.PMChannelCommand;
import io.dblosevn.xserver.bukkit.events.global.CrossServerChatEvent;
import io.dblosevn.xserver.bukkit.events.global.CrossServerPMEvent;
import io.dblosevn.xserver.bukkit.events.global.LocalChatWrapperEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener
implements Listener {
    @EventHandler(priority=EventPriority.HIGHEST, ignoreCancelled=true)
    public void onGlobalChatEvent(CrossServerChatEvent e) {
        if (e.isCancelled()) {
            return;
        }
        ChatSourceObject source = e.getSender();
        source.getChannel().sendMessage(e);
    }

    @EventHandler(priority=EventPriority.HIGHEST, ignoreCancelled=true)
    public void onGlobalPMEvent(CrossServerPMEvent e) {
        if (e.isCancelled()) {
            return;
        }
        ChatSourceObject sender = e.getSender();
        ChatSourceObject recipient = e.getRecipient();
        PMChannelCommand.replyTo.put(recipient.getPlayerName(), sender.getPlayerName());
        sender.getChannel().sendMessage(e);
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onPlayerChatLocal(AsyncPlayerChatEvent e) {
        if (e instanceof LocalChatWrapperEvent) {
            return;
        }
        
        CrossServerChatEvent event = new CrossServerChatEvent(e.getPlayer(), "local", e.getMessage());
        Bukkit.getServer().getPluginManager().callEvent((Event)event);
        e.setCancelled(true);
    }
}

