package io.dblosevn.xserver.bukkit.listeners;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.commands.SeenCommand;
import io.dblosevn.xserver.bukkit.events.global.CrossServerBackLocationEvent;
import io.dblosevn.xserver.bukkit.events.global.CrossServerModeratorEvent;
import io.dblosevn.xserver.bukkit.events.global.CrossServerProxyStatusEvent;
import io.dblosevn.xserver.bukkit.events.global.CrossServerTeleportEvent;
import io.dblosevn.xserver.bukkit.objects.PacketType;
import io.dblosevn.xserver.bukkit.objects.SQL;
import io.dblosevn.xserver.bukkit.objects.Spawn;
import io.dblosevn.xserver.bukkit.objects.TeleportLocation;
import io.dblosevn.xserver.bukkit.sockets.packets.ProxyStatusPacket;
import io.dblosevn.xserver.bukkit.util.PermUtil;
import io.dblosevn.xserver.bukkit.util.Util;
import io.netty.util.internal.ConcurrentSet;

import java.util.ArrayList;
import java.util.LinkedList;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

public class PlayerManagementListener
implements Listener {
    @EventHandler
    public void onProxyUpdate(CrossServerProxyStatusEvent e) {
        ProxyStatusPacket packet = e.getPacket();
        if (packet.getType().equals(PacketType.PROXY_STATUS)) {
            e.getClass();
            if (e.getStatus() == 2) {
                CrossServerAPI.refreshStatus(e.getServersToPlayers(), e.getUuidsToPlayer(), e.getPlayernamesToPlayer());
            }
        }
    }
    @EventHandler
    public void onModeratorEvent(final CrossServerModeratorEvent e) {
        Bukkit.getScheduler().runTask((Plugin)CrossServerLib.get(), new Runnable(){

            @Override
            public void run() {
                e.handle();
            }
        });
    }

    @EventHandler
    public void onCrossServerTeleport(CrossServerTeleportEvent e) {
        e.teleport();
    }

    @EventHandler
    public void onCrossServerBackPacket(CrossServerBackLocationEvent e) {
        if (CrossServerAPI.isServer(e.getServer())) {
            if (CrossServerAPI.localOnline(e.getSource())) {
                Player source = Bukkit.getPlayer((String)e.getSource());
                source.setMetadata("tpbacklocation", (MetadataValue)new FixedMetadataValue((Plugin)CrossServerLib.get(), e.getLocation()));
            } else {
                CrossServerBackLocationEvent.setCache(e.getSourceUUID(), e.getLocation());
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        e.getEntity().setMetadata("tpbacklocation", (MetadataValue)new FixedMetadataValue((Plugin)CrossServerLib.get(), new TeleportLocation(e.getEntity().getLocation())));
    }

    @EventHandler(priority=EventPriority.HIGHEST)
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        LinkedList<Spawn> spawns = SQL.getSpawn(new TeleportLocation(e.getPlayer().getLocation()));
        for (Spawn s : spawns) {
            if (!s.getGroup().equalsIgnoreCase("default")) {
            	if (!PermUtil.hasOwnPerm(e.getPlayer(), "spawn." + s.getGroup())) {
            		continue;
            	}
            }
            e.setRespawnLocation(s.getLoc().getLocation());
            break;
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent e) {
        if (e.getFrom().equals(e.getTo())) {
            e.setCancelled(true);
            return;
        }
        if (!e.getPlayer().hasMetadata("tpactive")) {
            e.getPlayer().setMetadata("tpbacklocation", (MetadataValue)new FixedMetadataValue((Plugin)CrossServerLib.get(), new TeleportLocation(e.getFrom())));
            return;
        }
        if (e.getPlayer().hasMetadata("tpactive")) {
            return;
        }
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        TeleportLocation backLoc;
    	String server = CrossServerLib.get().getConfig().getString("serverName", "Lobby");
        
        SQL.playerRank(e.getPlayer().getUniqueId(), PermUtil.getGroupForServer(PermUtil.getUser(e.getPlayer()), server));
        CrossServerTeleportEvent event = CrossServerTeleportEvent.getCache(e.getPlayer().getUniqueId());
        if (event != null) {
            CrossServerTeleportEvent.removeCache(e.getPlayer().getUniqueId());
            event.teleport();
        }
        if ((backLoc = CrossServerBackLocationEvent.getCache(e.getPlayer().getUniqueId())) != null) {
            CrossServerBackLocationEvent.removeCache(e.getPlayer().getUniqueId());
            e.getPlayer().setMetadata("tpbacklocation", (MetadataValue)new FixedMetadataValue((Plugin)CrossServerLib.get(), backLoc));
        }
    }
    
    ConcurrentSet<Player> seenCache = new ConcurrentSet<Player>();
    @EventHandler(priority=EventPriority.HIGHEST, ignoreCancelled=true)
    public void onPlayerRightClickPlayer(PlayerInteractAtEntityEvent e) {
        if (e.getRightClicked() instanceof Player) {
        	if (!seenCache.contains(e.getPlayer())) {
        		final Player player = e.getPlayer();
        		seenCache.add(player);
        		Bukkit.getScheduler().runTaskLater(CrossServerLib.get(), new Runnable() {
					@Override
					public void run() {
						seenCache.remove(player);
					}
				}, 60L);
        		SeenCommand.seen(player, e.getRightClicked().getName());
        	}
        }
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onTab(PlayerChatTabCompleteEvent e) {
        String token = e.getLastToken().toLowerCase();
        e.getTabCompletions().clear();
        for (String p : CrossServerAPI.getAllPlayerNames()) {
            if (!p.toLowerCase().startsWith(token) && !token.isEmpty()) continue;
            e.getTabCompletions().add(CrossServerAPI.findPlayer(p).getName());
        }
    }

    @EventHandler(priority=EventPriority.MONITOR)
    public void onWhitelist(final PlayerJoinEvent e) {
        boolean enabled = CrossServerLib.get().getConfig().getBoolean("whitelist.enabled", false);
        if (enabled) {
            boolean shouldKick = CrossServerLib.get().getConfig().getBoolean("whitelist.action.kick", true);
            final String server = CrossServerLib.get().getConfig().getString("whitelist.action.server", "hub");
            ArrayList<String> groups = (ArrayList<String>)CrossServerLib.get().getConfig().getStringList("whitelist.groups");
            ArrayList<String> players = (ArrayList<String>)CrossServerLib.get().getConfig().getStringList("whitelist.players");

            for (String g : groups) {
                if (!e.getPlayer().hasPermission("group." + g)) continue;
                return;
            }
            if (players.contains(e.getPlayer().getUniqueId().toString())) {
                return;
            }
            if (shouldKick) {
                e.getPlayer().kickPlayer(ChatColor.RED + "You are not whitelisted on this server.");
                return;
            }
            ArrayList<String> servers = CrossServerAPI.getServers();
            for (String s : servers) {
                if (!s.toLowerCase().equals(server.toLowerCase())) continue;
                Util.pluginMessage(e.getPlayer(), ChatColor.RED + "You are not whitelisted on this server teleporting to: " + CrossServerLib.secondaryColor + server);
                e.getPlayer().teleport(new Location(e.getPlayer().getWorld(), 0.0, 0.0, 0.0));
                Bukkit.getScheduler().runTaskLater((Plugin)CrossServerLib.get(), new Runnable(){

                    @Override
                    public void run() {
                        Util.connect(e.getPlayer(), server);
                    }
                }, 40L);
                return;
            }
            e.getPlayer().kickPlayer(ChatColor.RED + "You are not whitelisted on this server.");
        }
    }

}

