package io.dblosevn.xserver.bukkit.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.bringholm.nametagchanger.NameTagChanger;
import com.earth2me.essentials.IEssentials;
import com.earth2me.essentials.User;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.objects.ProxyPlayer;
import net.ess3.api.events.AfkStatusChangeEvent;
import net.ess3.api.events.NickChangeEvent;

public class EssListener implements Listener {
	IEssentials ess;
	public EssListener() {
		ess = CrossServerLib.getEss();
	}
	@EventHandler
	public void isAFK(AfkStatusChangeEvent e) {
		//TODO: implement afk
	}
	@EventHandler
	public void isNickChange(NickChangeEvent e) {
		Player player = e.getAffected().getBase();
		ProxyPlayer p = CrossServerAPI.findPlayer(player.getUniqueId());
		String eNick = (null == e.getValue()) ? "" : e.getValue();
		if (null != p) {
			p.setNick(eNick);
			NameTagChanger.INSTANCE.changePlayerName(player, p.getColor() + p.getDisplayName());
		} else {
			NameTagChanger.INSTANCE.resetPlayerName(player);
		}
		ColorListener.setTabColors();
	}
	@EventHandler
	public void setNick(PlayerJoinEvent e) {
		ProxyPlayer p = new ProxyPlayer(e.getPlayer().getName());
		User eUser = ess.getUser(e.getPlayer());
		String eNick = (null == eUser.getNickname()) ? "" : eUser.getNickname();
		String pNick = (p.getNick().isEmpty()) ? null : p.getNick();
		if (!p.getNick().equalsIgnoreCase(eNick)) {
			eUser.setNickname(pNick);
		}
		if (!p.getNick().isEmpty()) {
			NameTagChanger.INSTANCE.changePlayerName(e.getPlayer(), p.getColor() + p.getDisplayName());
		}
	}
}
