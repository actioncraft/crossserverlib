package io.dblosevn.xserver.bukkit.objects;

import io.dblosevn.xserver.bukkit.objects.TeleportLocation;

public class Spawn {
    String group = "";
    TeleportLocation loc = null;

    public Spawn(String group, TeleportLocation loc) {
        this.group = group;
        this.loc = loc;
    }

    public String getGroup() {
        return this.group;
    }

    public TeleportLocation getLoc() {
        return this.loc;
    }
}

