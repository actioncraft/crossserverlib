package io.dblosevn.xserver.bukkit.objects;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.objects.PlayerInfo;
import io.dblosevn.xserver.bukkit.objects.SQL;
import io.dblosevn.xserver.bukkit.util.PermUtil;
import me.lucko.luckperms.api.caching.MetaData;

import java.util.UUID;
import org.bukkit.ChatColor;

public class ProxyPlayer {
    private String name = null;
    private UUID uuid = null;
    private String server = null;
    private String nick = null;
    private String rank;
    private ChatColor color;
    private int player_id = 0;
    private long joindate;
    private long lastLogin;
    private long lastLogout;
    private long playTime;
    private boolean initialized = false;

    public ProxyPlayer(String name, String uuid, String server) {
        this.name = name;
        this.uuid = UUID.fromString(uuid);
        this.server = server;
    }

    public ProxyPlayer(String name) {
        this.name = name;
    }

    public String getName() {
        this.init();
        return this.name;
    }

    public UUID getUuid() {
        if (this.uuid == null) {
            this.init();
        }
        return this.uuid;
    }

    public String getServer() {
        if (this.server == null) {
            this.init();
        }
        return this.server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public ProxyPlayer init() {
        if (this.initialized) {
            return this;
        }
        try {
            PlayerInfo rs = this.uuid != null ? SQL.getPlayerInfo(this.uuid) : SQL.getPlayerInfo(this.name);
            this.player_id = rs.player_id;
            this.name = rs.player_name;
            this.lastLogin = rs.player_last_login;
            this.lastLogout = rs.player_last_logout;
            this.joindate = rs.player_join_date;
            this.playTime = rs.player_time;
            this.server = rs.player_last_server;
            this.rank = rs.player_rank;
            this.uuid = UUID.fromString(rs.player_uuid);
            this.nick = rs.player_nick;
            
        	String server = CrossServerLib.get().getConfig().getString("serverName", "Lobby");
    		MetaData meta = PermUtil.getMetaDataForServer(PermUtil.getUser(this.name), server);

    		this.color = ChatColor.getByChar(meta.getMeta().getOrDefault("tabcolor", "8").replace("&", ""));
            this.initialized = true;
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }
        return this;
    }

    public int getId() {
        this.init();
        return this.player_id;
    }

    public String getRank() {
        this.init();
        return this.rank;
    }

    public ChatColor getColor() {
        this.init();
        return this.color;
    }

    public long getJoinDate() {
        this.init();
        return this.joindate;
    }

    public long getLastLogin() {
        this.init();
        return this.lastLogin;
    }

    public long getLastLogout() {
        this.init();
        return this.lastLogout;
    }

    public long getPlayTime() {
        this.init();
        return this.playTime;
    }

    public boolean isValid() {
        this.init();
        return this.initialized;
    }

	public String getNick() {
        this.init();
		return nick;
	}

	public boolean setNick(String nick) {
        this.init();
		if (SQL.setNick(this.uuid, nick)) {
			this.nick = nick;
			return true;
		}
		return false;
	}
	
	public String getDisplayName() {
		return getDisplayName(false);
	}
	public String getDisplayName(boolean noPrefix) {
		if (noPrefix) {
			return (getNick().equals("")) ? getName() : getNick();
		} else {
			return (getNick().equals("")) ? getName() : ("~" + getNick());
		}
	}
}
