package io.dblosevn.xserver.bukkit.objects;

import io.dblosevn.xserver.bukkit.objects.TeleportLocation;

public class Home {
    String name = "";
    TeleportLocation loc = null;

    public Home(String name, TeleportLocation loc) {
        this.name = name;
        this.loc = loc;
    }

    public Home(int player_id, double cost, long lastPayment, String name, TeleportLocation loc) {
        this.name = name;
        this.loc = loc;
    }

    public String getName() {
        return this.name;
    }

    public TeleportLocation getLoc() {
        return this.loc;
    }
}

