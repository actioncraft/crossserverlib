package io.dblosevn.xserver.bukkit.objects;

public class PlayerInfo {
    public int player_id;
    public long player_uuid_lower;
    public long player_uuid_upper;
    public String player_uuid;
    public String player_name;
    public long player_join_date;
    public long player_last_login;
    public long player_last_logout;
    public String player_rank;
    public long player_time;
    public String player_last_server;
    public String player_last_ip;
    public String player_nick;
}

