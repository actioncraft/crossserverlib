package io.dblosevn.xserver.bukkit.objects;

import io.dblosevn.xserver.bukkit.objects.TeleportLocation;

public class Warp {
    String name = "";
    int player_id = 0;
    boolean isPwarp = false;
    double cost = 0.0;
    long lastPayment = 0L;
    TeleportLocation loc = null;

    public Warp(String name, TeleportLocation loc) {
        this.name = name;
        this.loc = loc;
    }

    public Warp(int player_id, double cost, long lastPayment, String name, TeleportLocation loc) {
        this.name = name;
        this.loc = loc;
        this.isPwarp = true;
        this.player_id = player_id;
        this.cost = cost;
        this.lastPayment = lastPayment;
    }

    public String getName() {
        return this.name;
    }

    public int getPlayer_id() {
        return this.player_id;
    }

    public boolean isPwarp() {
        return this.isPwarp;
    }

    public double getCost() {
        return this.cost;
    }

    public long getLastPayment() {
        return this.lastPayment;
    }

    public TeleportLocation getLoc() {
        return this.loc;
    }
}

