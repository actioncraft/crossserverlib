package io.dblosevn.xserver.bukkit.objects;

import com.google.gson.JsonObject;
import io.dblosevn.xserver.bukkit.objects.Packet;
import org.bukkit.event.Event;

public abstract class CrossServerEvent
extends Event {
    public abstract JsonObject serialize();

    public abstract void deSerialize(JsonObject var1);

    public abstract Packet getPacket();
}

