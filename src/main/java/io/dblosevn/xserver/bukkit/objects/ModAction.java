package io.dblosevn.xserver.bukkit.objects;


public enum ModAction {
	UNBAN(4), PERM(3), TEMP(2), KICK(1), UNKNOWN(0);

	private final int action;

	private ModAction(int action) {
		this.action = action;
	}

	public int getId() {
		return this.action;
	}

	public static ModAction getById(int id) {
        for (ModAction t : values()) {
          if (t.action == id) {
            return t;
          }
        }
        return UNKNOWN;
      }
}
