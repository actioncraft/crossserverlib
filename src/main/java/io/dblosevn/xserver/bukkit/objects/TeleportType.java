package io.dblosevn.xserver.bukkit.objects;

public enum TeleportType {
    PLAYER_TP,
    PLAYER_TPHERE,
    TELEPORT_REQUEST,
    TELEPORT_HERE_REQUEST,
    TELEPORT_ACCEPT,
    TELEPORT_HERE_ACCEPT,
    TELEPORT_DENY,
    TELEPORT_CANCEL,
    TO_LOCATION,
    HOME,
    WARP,
    PWARP,
    SPAWN,
    UNKNOWN;
    


    public static TeleportType lookup(String o) {
        if (TeleportType.valueOf(o) == null) {
            return UNKNOWN;
        }
        return TeleportType.valueOf(o);
    }
}

