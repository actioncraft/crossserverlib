package io.dblosevn.xserver.bukkit.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BanInfo {
	public String staff = "";
	public String player = "";
	public long time = 0;
	public ModAction type = ModAction.UNKNOWN;
	public BanStatus status = BanStatus.UNKNOWN;
	public long length = 0;
	public String reason = "";
	public long remaining = 0;
	
	public BanInfo() { }
	public BanInfo(ResultSet rs) throws SQLException { 
		staff = rs.getString("staff_name");
		player = rs.getString("player_name");
		time = rs.getLong("ban_time");
		type = ModAction.valueOf(rs.getString("ban_type"));
		status = BanStatus.valueOf(rs.getString("ban_status"));
		length = rs.getLong("ban_length");
		reason = rs.getString("ban_reason");
		remaining = rs.getLong("remaining");
		
		if (remaining < 0) {
			remaining = 0;
		}
	}
	
}
