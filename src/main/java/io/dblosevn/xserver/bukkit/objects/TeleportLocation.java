package io.dblosevn.xserver.bukkit.objects;

import com.google.gson.JsonObject;
import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class TeleportLocation {
    private String server = "";
    private String world = "";
    private double x = 0.0;
    private double y = 0.0;
    private double z = 0.0;
    private float pitch = 0.0f;
    private float yaw = 0.0f;

    public TeleportLocation() {
    }

    public TeleportLocation(String server, String world, double x, double y, double z, float pitch, float yaw) {
        this.server = server;
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
    }

    public TeleportLocation(String server, Location location) {
        this.server = server;
        this.world = location.getWorld().getName();
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        this.pitch = location.getPitch();
        this.yaw = location.getYaw();
    }

    public TeleportLocation(Location location) {
        this(CrossServerAPI.getServer(), location);
    }

    public TeleportLocation(JsonObject o) {
        this.server = o.get("server").getAsString();
        this.world = o.get("world").getAsString();
        this.x = o.get("x").getAsDouble();
        this.y = o.get("y").getAsDouble();
        this.z = o.get("z").getAsDouble();
        this.pitch = o.get("pitch").getAsFloat();
        this.yaw = o.get("yaw").getAsFloat();
    }

    public String getServer() {
        return this.server;
    }

    public String getWorld() {
        return this.world;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public float getPitch() {
        return this.pitch;
    }

    public float getYaw() {
        return this.yaw;
    }

    public Location getLocation() {
        return new Location(Bukkit.getWorld((String)this.world), this.x, this.y, this.z, this.yaw, this.pitch);
    }

    public JsonObject getObject() {
        JsonObject t = new JsonObject();
        t.addProperty("server", this.server);
        t.addProperty("world", this.world);
        t.addProperty("x", (Number)this.x);
        t.addProperty("y", (Number)this.y);
        t.addProperty("z", (Number)this.z);
        t.addProperty("pitch", (Number)Float.valueOf(this.pitch));
        t.addProperty("yaw", (Number)Float.valueOf(this.yaw));
        return t;
    }
}

