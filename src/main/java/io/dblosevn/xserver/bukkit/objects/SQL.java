package io.dblosevn.xserver.bukkit.objects;

import io.dblosevn.xserver.bukkit.api.DB;
import io.dblosevn.xserver.bukkit.api.DBSchema;
import io.dblosevn.xserver.bukkit.objects.Home;
import io.dblosevn.xserver.bukkit.objects.PlayerInfo;
import io.dblosevn.xserver.bukkit.objects.Spawn;
import io.dblosevn.xserver.bukkit.objects.TeleportLocation;
import io.dblosevn.xserver.bukkit.objects.Warp;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.UUID;

public class SQL {
    public static void playerRank(UUID uuid, String rank) {
        String sql = "Update " + (DBSchema.Tables.PLAYERS) + " Set player_rank=? Where player_uuid_upper=? And player_uuid_lower=?";
        Connection connection = DB.connect();
        PreparedStatement ps = null;
        try {
            try {
                ps = connection.prepareStatement(sql);
                ps.setString(1, rank);
                ps.setLong(2, uuid.getMostSignificantBits());
                ps.setLong(3, uuid.getLeastSignificantBits());
                ps.execute();
            }
            catch (SQLException e) {
                e.printStackTrace();
                DB.close(null, ps, connection);
            }
        }
        finally {
            DB.close(null, ps, connection);
        }
    }
    
    public static boolean banPlayer(ProxyPlayer staff, ProxyPlayer player, ModAction type, long expires, String message) {
    	String sql = "Update " + (DBSchema.Tables.BANS) + " Set ban_status='REBAN' Where ban_player_id=? And ban_status='ACTIVE'";
    	
        Connection connection = DB.connect();
        PreparedStatement ps = null;
        try {
            try {
                ps = connection.prepareStatement(sql);
                ps.setInt(1, player.getId());
                int cnt = ps.executeUpdate(); //Only one active ban allowed expire active bans with status reban
                ps.close();

                sql = "Insert Into " + (DBSchema.Tables.BANS) + 
                		" Set ban_staff_id=?, ban_player_id=?, ban_time=UNIX_tIMESTAMP(NOW()), ban_type=?, ban_length=?, ban_reason=?";
                ps = connection.prepareStatement(sql);
                ps.setInt(1, staff.getId());
                ps.setInt(2, player.getId());
                ps.setString(3, type.name());
                ps.setLong(4, expires);
                ps.setString(5, message);
                ps.execute();
                return (cnt != 0); //true for reban false for ban
            }
            catch (SQLException e) {
                e.printStackTrace();
                DB.close(null, ps, connection);
            }
        }
        finally {
            DB.close(null, ps, connection);
        }
        return false;
    }
    
    public static boolean isBanned(ProxyPlayer player) {
        Connection connection;
        ResultSet rs;
        PreparedStatement ps;
        String sql = "Select 1 From " + (DBSchema.Tables.BANS) + " Where ban_player_id=? and ban_status='ACTIVE'";
        connection = DB.connect();
        ps = null;
        rs = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, player.getId());
            rs = ps.executeQuery();
            if (rs.next()) {
            	return true;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            DB.close(rs, ps, connection);
        }
        DB.close(rs, ps, connection);
        return false;
    }
	public static boolean isBanned(UUID player) {
		Connection connection;
		ResultSet rs;
		PreparedStatement ps;
		String sql = "Select 1 From " + (DBSchema.Tables.BANS) + " Join " + (DBSchema.Tables.PLAYERS)
				+ " on player_id = ban_player_id Where player_uuid_lower=? and player_uuid_upper=? and ban_status='ACTIVE'";
		connection = DB.connect();
		ps = null;
		rs = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, player.getLeastSignificantBits());
			ps.setLong(2, player.getMostSignificantBits());
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.close(rs, ps, connection);
		}
		DB.close(rs, ps, connection);
		return false;
	}

	public static boolean unbanPlayer(ProxyPlayer player, ProxyPlayer staff) {
        Connection connection;
        ResultSet rs;
        PreparedStatement ps;
        String sql = "Select * From " + (DBSchema.Tables.BANS) + " Where ban_player_id=? and ban_status='ACTIVE'";
        connection = DB.connect();
        ps = null;
        rs = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, player.getId());
            rs = ps.executeQuery();
            if (rs.next()) {
            	ps.close();
            	rs.close();
                sql = "Update " + (DBSchema.Tables.BANS) + " Set ban_status='UNBANNED', ban_unban_staff_id=? Where ban_player_id=? and ban_status='ACTIVE'";
                ps = connection.prepareStatement(sql);
                ps.setInt(1, staff.getId());
                ps.setInt(2, player.getId());

                long cnt = ps.executeUpdate();
                if (cnt > 0) {
                	return true;
                }
            	return false;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            DB.close(rs, ps, connection);
        }
        DB.close(rs, ps, connection);
        return false;
    }
    
    
    public static boolean setNick(UUID uuid, String nick) {
        Connection connection;
        ResultSet rs;
        PreparedStatement ps;
        String sql = "Select * From " + (DBSchema.Tables.PLAYERS) + " Where (player_nick=? or player_name=?) and (player_uuid_upper <> ? And player_uuid_lower <> ?) Limit 1";
        connection = DB.connect();
        ps = null;
        rs = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, nick);
            ps.setString(2, nick);
            ps.setLong(3, uuid.getMostSignificantBits());
            ps.setLong(4, uuid.getLeastSignificantBits());
            rs = ps.executeQuery();
            if (nick.isEmpty() == false && rs.next()) {
            	return false;
            } else {
            	ps.close();
                sql = "Update " + (DBSchema.Tables.PLAYERS) + " Set player_nick=? Where player_uuid_upper=? And player_uuid_lower=?";
                ps = connection.prepareStatement(sql);
                ps.setString(1, nick);
                ps.setLong(2, uuid.getMostSignificantBits());
                ps.setLong(3, uuid.getLeastSignificantBits());
                long cnt = ps.executeUpdate();
                if (cnt > 0) {
                	return true;
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            DB.close(rs, ps, connection);
        }
        DB.close(rs, ps, connection);
        return false;
    }
    
    public static PlayerInfo getPlayerInfo(UUID uuid) {
            Connection connection;
            ResultSet rs;
            PreparedStatement ps;
            String sql = "Select * From " + (DBSchema.Tables.PLAYERS) + " Where player_uuid_upper=? And player_uuid_lower=?";
            connection = DB.connect();
            ps = null;
            rs = null;
            PlayerInfo out = new PlayerInfo();
            try {
                ps = connection.prepareStatement(sql);
                ps.setLong(1, uuid.getMostSignificantBits());
                ps.setLong(2, uuid.getLeastSignificantBits());
                rs = ps.executeQuery();
                if (rs.next()) {
                    out.player_id = rs.getInt("player_id");
                    out.player_join_date = rs.getLong("player_join_date");
                    out.player_last_ip = rs.getString("player_last_ip");
                    out.player_last_login = rs.getLong("player_last_login");
                    out.player_last_logout = rs.getLong("player_last_logout");
                    out.player_last_server = rs.getString("player_last_server");
                    out.player_name = rs.getString("player_name");
                    out.player_rank = rs.getString("player_rank");
                    out.player_uuid = rs.getString("player_uuid");
                    out.player_uuid_lower = rs.getLong("player_uuid_lower");
                    out.player_uuid_upper = rs.getLong("player_uuid_upper");
                    out.player_time = rs.getLong("player_time");
                    out.player_nick = rs.getString("player_nick");

                    PlayerInfo playerInfo = out;
                    DB.close(rs, ps, connection);
                    return playerInfo;
                }
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            finally {
                DB.close(rs, ps, connection);
            }
            DB.close(rs, ps, connection);
        return null;
    }

    public static PlayerInfo getPlayerInfo(String playerName) {
            Connection connection;
            ResultSet rs;
            PreparedStatement ps;
            String sql = "Select * From " + (DBSchema.Tables.PLAYERS) + " Where player_name=? or player_nick=?";
            connection = DB.connect();
            ps = null;
            rs = null;
            PlayerInfo out = new PlayerInfo();
            try {
                ps = connection.prepareStatement(sql);
                ps.setString(1, playerName);
                ps.setString(2, playerName);
                rs = ps.executeQuery();
                if (rs.next()) {
                    out.player_id = rs.getInt("player_id");
                    out.player_join_date = rs.getLong("player_join_date");
                    out.player_last_ip = rs.getString("player_last_ip");
                    out.player_last_login = rs.getLong("player_last_login");
                    out.player_last_logout = rs.getLong("player_last_logout");
                    out.player_last_server = rs.getString("player_last_server");
                    out.player_name = rs.getString("player_name");
                    out.player_rank = rs.getString("player_rank");
                    out.player_uuid = rs.getString("player_uuid");
                    out.player_uuid_lower = rs.getLong("player_uuid_lower");
                    out.player_uuid_upper = rs.getLong("player_uuid_upper");
                    out.player_time = rs.getLong("player_time");
                    out.player_nick = rs.getString("player_nick");
                    PlayerInfo playerInfo = out;
                    DB.close(rs, ps, connection);
                    return playerInfo;
                }
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            finally {
                DB.close(rs, ps, connection);
            }
            DB.close(rs, ps, connection);
        return null;
    }

    public static void addWarp(String name, TeleportLocation loc) {
            Connection connection;
            PreparedStatement ps;
            String set = "warp_name=?, warp_enabled=1, warp_server=?, warp_world=?, warp_x=?, warp_y=?, warp_z=?, warp_yaw=?, warp_pitch=?";
            String sql = "Insert Into " + (DBSchema.Tables.WARPS) + " Set " + set + " On Duplicate Key Update " + set;
            connection = DB.connect();
            ps = null;
            try {
                try {
                    int x = 1;
                    ps = connection.prepareStatement(sql);
                    ps.setString(x, name);
                    ps.setString(++x, loc.getServer());
                    ps.setString(++x, loc.getWorld());
                    ps.setDouble(++x, loc.getX());
                    ps.setDouble(++x, loc.getY());
                    ps.setDouble(++x, loc.getZ());
                    ps.setFloat(++x, loc.getYaw());
                    ps.setFloat(++x, loc.getPitch());
                    ps.setString(++x, name);
                    ps.setString(++x, loc.getServer());
                    ps.setString(++x, loc.getWorld());
                    ps.setDouble(++x, loc.getX());
                    ps.setDouble(++x, loc.getY());
                    ps.setDouble(++x, loc.getZ());
                    ps.setFloat(++x, loc.getYaw());
                    ps.setFloat(++x, loc.getPitch());
                    ++x;
                    ps.execute();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    DB.close(null, ps, connection);
                }
            }
            catch (Throwable throwable) {
                DB.close(null, ps, connection);
                throw throwable;
            }
            DB.close(null, ps, connection);
    }

    public static void enableWarp(String name, boolean enabled) {
            PreparedStatement ps;
            Connection connection;
            String sql = "Update " + (DBSchema.Tables.WARPS) + " Set warp_enabled=? Where warp_name=?";
            connection = DB.connect();
            ps = null;
            try {
                try {
                    int x = 1;
                    ps = connection.prepareStatement(sql);
                    ps.setInt(x, enabled ? 1 : 0);
                    ps.setString(++x, name);
                    ++x;
                    ps.execute();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    DB.close(null, ps, connection);
                }
            }
            catch (Throwable throwable) {
                DB.close(null, ps, connection);
                throw throwable;
            }
            DB.close(null, ps, connection);
    }

    public static void removeWarp(String name) {
            Connection connection;
            PreparedStatement ps;
            String sql = "Delete From " + (DBSchema.Tables.WARPS) + " Where warp_name=?";
            connection = DB.connect();
            ps = null;
            try {
                try {
                    int x = 1;
                    ps = connection.prepareStatement(sql);
                    ps.setString(x, name);
                    ++x;
                    ps.execute();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    DB.close(null, ps, connection);
                }
            }
            catch (Throwable throwable) {
                DB.close(null, ps, connection);
                throw throwable;
            }
            DB.close(null, ps, connection);
    }

    public static Warp getWarp(String warpName) {
            Connection connection;
            ResultSet rs;
            PreparedStatement ps;
            String sql = "Select * From " + (DBSchema.Tables.WARPS) + " Where warp_name=?";
            connection = DB.connect();
            ps = null;
            rs = null;
            try {
                ps = connection.prepareStatement(sql);
                ps.setString(1, warpName);
                rs = ps.executeQuery();
                if (rs.next()) {
                    String server = rs.getString("warp_server");
                    String world = rs.getString("warp_world");
                    double x = rs.getDouble("warp_x");
                    double y = rs.getDouble("warp_y");
                    double z = rs.getDouble("warp_z");
                    float yaw = rs.getFloat("warp_yaw");
                    float pitch = rs.getFloat("warp_pitch");
                    Warp warp = new Warp(rs.getString("warp_name"), new TeleportLocation(server, world, x, y, z, pitch, yaw));
                    DB.close(rs, ps, connection);
                    return warp;
                }
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            finally {
                DB.close(rs, ps, connection);
            }
            DB.close(rs, ps, connection);
        return null;
    }

    public static LinkedList<Warp> getWarps() {
        String sql = "Select * From " + (DBSchema.Tables.WARPS) + " order by warp_name";
        Connection connection = DB.connect();
        PreparedStatement ps = null;
        ResultSet rs = null;
        LinkedList<Warp> out = new LinkedList<Warp>();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                String server = rs.getString("warp_server");
                String world = rs.getString("warp_world");
                double x = rs.getDouble("warp_x");
                double y = rs.getDouble("warp_y");
                double z = rs.getDouble("warp_z");
                float yaw = rs.getFloat("warp_yaw");
                float pitch = rs.getFloat("warp_pitch");
                out.add(new Warp(rs.getString("warp_name"), new TeleportLocation(server, world, x, y, z, pitch, yaw)));
            }
        }
        catch (SQLException e) {
            try {
                e.printStackTrace();
            }
            catch (Throwable throwable) {
                DB.close(rs, ps, connection);
                throw throwable;
            }
            DB.close(rs, ps, connection);
        }
        DB.close(rs, ps, connection);
        return out;
    }

    public static void addPWarp(UUID owner, double cost, String name, TeleportLocation loc) {
            PreparedStatement ps;
            Connection connection;
            String sql = "INSERT INTO " + (DBSchema.Tables.PWARPS) + " (warp_player_id,warp_cost,warp_last_payment,warp_name,warp_server,warp_world,warp_x,warp_y,warp_z,warp_yaw,warp_pitch) " + "SELECT player_id, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? FROM " + (DBSchema.Tables.PLAYERS) + " WHERE player_uuid_lower=? AND player_uuid_upper=? " + "ON DUPLICATE KEY UPDATE warp_server=?,warp_world=?,warp_x=?,warp_y=?,warp_z=?,warp_yaw=?,warp_pitch=?";
            connection = DB.connect();
            ps = null;
            try {
                try {
                    int x = 1;
                    ps = connection.prepareStatement(sql);
                    ps.setDouble(++x, cost);
                    ps.setLong(++x, System.currentTimeMillis());
                    ps.setString(++x, name);
                    ps.setString(++x, loc.getServer());
                    ps.setString(++x, loc.getWorld());
                    ps.setDouble(++x, loc.getX());
                    ps.setDouble(++x, loc.getY());
                    ps.setDouble(++x, loc.getZ());
                    ps.setFloat(++x, loc.getYaw());
                    ps.setFloat(++x, loc.getPitch());
                    ps.setLong(++x, owner.getLeastSignificantBits());
                    ps.setLong(++x, owner.getMostSignificantBits());
                    ps.setString(++x, loc.getServer());
                    ps.setString(++x, loc.getWorld());
                    ps.setDouble(++x, loc.getX());
                    ps.setDouble(++x, loc.getY());
                    ps.setDouble(++x, loc.getZ());
                    ps.setFloat(++x, loc.getYaw());
                    ps.setFloat(++x, loc.getPitch());
                    ++x;
                    ps.execute();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    DB.close(null, ps, connection);
                }
            }
            catch (Throwable throwable) {
                DB.close(null, ps, connection);
                throw throwable;
            }
            DB.close(null, ps, connection);
    }

    public static void removePWarp(UUID uuid, String name) {
            PreparedStatement ps;
            Connection connection;
            String sql = "Delete " + (DBSchema.Tables.PWARPS) + " From " + (DBSchema.Tables.PWARPS) + " Inner Join " + (DBSchema.Tables.PLAYERS) + " on player_uuid_lower=? and player_uuid_upper=? Where warp_name=?";
            connection = DB.connect();
            ps = null;
            try {
                try {
                    int x = 1;
                    ps = connection.prepareStatement(sql);
                    ps.setLong(x, uuid.getLeastSignificantBits());
                    ps.setLong(++x, uuid.getMostSignificantBits());
                    ps.setString(++x, name);
                    ++x;
                    ps.execute();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    DB.close(null, ps, connection);
                }
            }
            catch (Throwable throwable) {
                DB.close(null, ps, connection);
                throw throwable;
            }
            DB.close(null, ps, connection);
    }

    public static Warp getPWarp(String warpName) {
        Connection connection;
        ResultSet rs;
        PreparedStatement ps;
        String sql = "Select * From " + (DBSchema.Tables.PWARPS) + " Where warp_name=?";
        connection = DB.connect();
        ps = null;
        rs = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, warpName);
            rs = ps.executeQuery();
            if (rs.next()) {
                String server = rs.getString("warp_server");
                String world = rs.getString("warp_world");
                double x = rs.getDouble("warp_x");
                double y = rs.getDouble("warp_y");
                double z = rs.getDouble("warp_z");
                float yaw = rs.getFloat("warp_yaw");
                float pitch = rs.getFloat("warp_pitch");
                Warp warp = new Warp(rs.getInt("warp_player_id"), rs.getDouble("warp_cost"), rs.getLong("warp_last_payment"), rs.getString("warp_name"), new TeleportLocation(server, world, x, y, z, pitch, yaw));
                DB.close(rs, ps, connection);
                return warp;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            DB.close(rs, ps, connection);
        }
        DB.close(rs, ps, connection);
        return null;
    }

    public static LinkedList<Warp> getPWarps(UUID uuid) {
            Connection connection;
            ResultSet rs;
            PreparedStatement ps;
            String sql = "Select * From " + (DBSchema.Tables.PWARPS) + " join " + (DBSchema.Tables.PLAYERS) + " on player_uuid_lower=? and player_uuid_upper=?  and player_id=warp_player_id Order by warp_name";
            connection = DB.connect();
            ps = null;
            rs = null;
            LinkedList<Warp> out = new LinkedList<Warp>();
            try {
                int i = 1;
                ps = connection.prepareStatement(sql);
                ps.setLong(i, uuid.getLeastSignificantBits());
                ps.setLong(++i, uuid.getMostSignificantBits());
                ++i;
                rs = ps.executeQuery();
                if (rs.next()) {
                    String server = rs.getString("warp_server");
                    String world = rs.getString("warp_world");
                    double x = rs.getDouble("warp_x");
                    double y = rs.getDouble("warp_y");
                    double z = rs.getDouble("warp_z");
                    float yaw = rs.getFloat("warp_yaw");
                    float pitch = rs.getFloat("warp_pitch");
                    out.add(new Warp(rs.getInt("warp_player_id"), rs.getDouble("warp_cost"), rs.getLong("warp_last_payment"), rs.getString("warp_name"), new TeleportLocation(server, world, x, y, z, pitch, yaw)));
                    LinkedList<Warp> linkedList = out;
                    DB.close(rs, ps, connection);
                    return linkedList;
                }
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            finally {
                DB.close(rs, ps, connection);
            }
            DB.close(rs, ps, connection);
        return null;
    }

    public static void addHome(UUID owner, String name, TeleportLocation loc) {
            Connection connection;
            PreparedStatement ps;
            String sql = "INSERT INTO " + (DBSchema.Tables.HOMES) + " (home_player_id,home_name,home_server,home_world,home_x,home_y,home_z,home_yaw,home_pitch) " + "SELECT player_id, ?, ?, ?, ?, ?, ?, ?, ? FROM " + (DBSchema.Tables.PLAYERS) + " WHERE player_uuid_lower=? AND player_uuid_upper=? " + "ON DUPLICATE KEY UPDATE home_server=?,home_world=?,home_x=?,home_y=?,home_z=?,home_yaw=?,home_pitch=?";
            connection = DB.connect();
            ps = null;
            try {
                try {
                    int x = 1;
                    ps = connection.prepareStatement(sql);
                    ps.setString(x, name);
                    ps.setString(++x, loc.getServer());
                    ps.setString(++x, loc.getWorld());
                    ps.setDouble(++x, loc.getX());
                    ps.setDouble(++x, loc.getY());
                    ps.setDouble(++x, loc.getZ());
                    ps.setFloat(++x, loc.getYaw());
                    ps.setFloat(++x, loc.getPitch());
                    ps.setLong(++x, owner.getLeastSignificantBits());
                    ps.setLong(++x, owner.getMostSignificantBits());
                    ps.setString(++x, loc.getServer());
                    ps.setString(++x, loc.getWorld());
                    ps.setDouble(++x, loc.getX());
                    ps.setDouble(++x, loc.getY());
                    ps.setDouble(++x, loc.getZ());
                    ps.setFloat(++x, loc.getYaw());
                    ps.setFloat(++x, loc.getPitch());
                    ++x;
                    ps.execute();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    DB.close(null, ps, connection);
                }
            }
            catch (Throwable throwable) {
                DB.close(null, ps, connection);
                throw throwable;
            }
            DB.close(null, ps, connection);
    }

    public static void removeHome(UUID owner, String name) {
        block5 : {
            PreparedStatement ps;
            Connection connection;
            String sql = "DELETE " + (DBSchema.Tables.HOMES) + " FROM " + (DBSchema.Tables.HOMES) + " INNER JOIN " + (DBSchema.Tables.PLAYERS) + " ON player_uuid_lower=? AND player_uuid_upper=? WHERE home_player_id = player_id AND home_name = ?";
            connection = DB.connect();
            ps = null;
            try {
                try {
                    int x = 1;
                    ps = connection.prepareStatement(sql);
                    ps.setLong(x, owner.getLeastSignificantBits());
                    ps.setLong(++x, owner.getMostSignificantBits());
                    ps.setString(++x, name);
                    ++x;
                    ps.execute();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    DB.close(null, ps, connection);
                    break block5;
                }
            }
            catch (Throwable throwable) {
                DB.close(null, ps, connection);
                throw throwable;
            }
            DB.close(null, ps, connection);
        }
    }

    public static Home getHome(UUID owner, String homeName) {
            ResultSet rs;
            PreparedStatement ps;
            Connection connection;
            String sql = "Select * From " + (DBSchema.Tables.HOMES) + " join " + (DBSchema.Tables.PLAYERS) + " on player_uuid_lower=? and player_uuid_upper=? and home_player_id=player_id Where home_name=? Order by home_name";
            connection = DB.connect();
            ps = null;
            rs = null;
            try {
                ps = connection.prepareStatement(sql);
                ps.setLong(1, owner.getLeastSignificantBits());
                ps.setLong(2, owner.getMostSignificantBits());
                ps.setString(3, homeName);
                rs = ps.executeQuery();
                if (rs.next()) {
                    String server = rs.getString("home_server");
                    String world = rs.getString("home_world");
                    double x = rs.getDouble("home_x");
                    double y = rs.getDouble("home_y");
                    double z = rs.getDouble("home_z");
                    float yaw = rs.getFloat("home_yaw");
                    float pitch = rs.getFloat("home_pitch");
                    Home home = new Home(rs.getString("home_name"), new TeleportLocation(server, world, x, y, z, pitch, yaw));
                    DB.close(rs, ps, connection);
                    return home;
                }
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            finally {
                DB.close(rs, ps, connection);
            }
            DB.close(rs, ps, connection);
        return null;
    }

    public static LinkedList<Home> getHomes(UUID owner) {
        LinkedList<Home> out;
            Connection connection;
            ResultSet rs;
            PreparedStatement ps;
            String sql = "Select * From " + (DBSchema.Tables.HOMES) + " join " + (DBSchema.Tables.PLAYERS) + " on player_uuid_lower=? and player_uuid_upper=? and player_id=home_player_id Order By home_name";
            connection = DB.connect();
            ps = null;
            rs = null;
            out = new LinkedList<Home>();
            try {
                try {
                    ps = connection.prepareStatement(sql);
                    ps.setLong(1, owner.getLeastSignificantBits());
                    ps.setLong(2, owner.getMostSignificantBits());
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        String server = rs.getString("home_server");
                        String world = rs.getString("home_world");
                        double x = rs.getDouble("home_x");
                        double y = rs.getDouble("home_y");
                        double z = rs.getDouble("home_z");
                        float yaw = rs.getFloat("home_yaw");
                        float pitch = rs.getFloat("home_pitch");
                        out.add(new Home(rs.getString("home_name"), new TeleportLocation(server, world, x, y, z, pitch, yaw)));
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    DB.close(rs, ps, connection);
                }
            }
            catch (Throwable throwable) {
                DB.close(rs, ps, connection);
                throw throwable;
            }
            DB.close(rs, ps, connection);
        return out;
    }

    public static void addSpawn(String group, TeleportLocation loc) {
            Connection connection;
            PreparedStatement ps;
            String set = "spawn_group=?, spawn_server=?, spawn_world=?, spawn_x=?, spawn_y=?, spawn_z=?, spawn_yaw=?, spawn_pitch=?";
            String sql = "Insert Into " + (DBSchema.Tables.SPAWNS) + " Set " + set + " On Duplicate Key Update " + set;
            connection = DB.connect();
            ps = null;
            try {
                try {
                    int x = 1;
                    ps = connection.prepareStatement(sql);
                    ps.setString(x, group);
                    ps.setString(++x, loc.getServer());
                    ps.setString(++x, loc.getWorld());
                    ps.setDouble(++x, loc.getX());
                    ps.setDouble(++x, loc.getY());
                    ps.setDouble(++x, loc.getZ());
                    ps.setFloat(++x, loc.getYaw());
                    ps.setFloat(++x, loc.getPitch());
                    ps.setString(++x, group);
                    ps.setString(++x, loc.getServer());
                    ps.setString(++x, loc.getWorld());
                    ps.setDouble(++x, loc.getX());
                    ps.setDouble(++x, loc.getY());
                    ps.setDouble(++x, loc.getZ());
                    ps.setFloat(++x, loc.getYaw());
                    ps.setFloat(++x, loc.getPitch());
                    ++x;
                    ps.execute();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    DB.close(null, ps, connection);
                }
            }
            catch (Throwable throwable) {
                DB.close(null, ps, connection);
                throw throwable;
            }
            DB.close(null, ps, connection);
    }

    public static LinkedList<Spawn> getSpawn(TeleportLocation source) {
        LinkedList<Spawn> out;
            Connection connection;
            ResultSet rs;
            PreparedStatement ps;
            String sql = "Select * From " + (DBSchema.Tables.SPAWNS) + " Where spawn_server=? order by if(spawn_group='default', 0, 1) desc";
            connection = DB.connect();
            ps = null;
            rs = null;
            out = new LinkedList<Spawn>();
            try {
                try {
                    ps = connection.prepareStatement(sql);
                    ps.setString(1, source.getServer());
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        String server = rs.getString("spawn_server");
                        String world = rs.getString("spawn_world");
                        double x = rs.getDouble("spawn_x");
                        double y = rs.getDouble("spawn_y");
                        double z = rs.getDouble("spawn_z");
                        float yaw = rs.getFloat("spawn_yaw");
                        float pitch = rs.getFloat("spawn_pitch");
                        out.add(new Spawn(rs.getString("spawn_group"), new TeleportLocation(server, world, x, y, z, pitch, yaw)));
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    DB.close(rs, ps, connection);
                }
            }
            catch (Throwable throwable) {
                DB.close(rs, ps, connection);
                throw throwable;
            }
            DB.close(rs, ps, connection);
        return out;
    }

    public static void removeSpawn(TeleportLocation source, String group) {
        block5 : {
            PreparedStatement ps;
            Connection connection;
            String sql = "DELETE FROM " + (DBSchema.Tables.SPAWNS) + " Where spawn_server=? and spawn_group=?";
            connection = DB.connect();
            ps = null;
            try {
                try {
                    int x = 1;
                    ps = connection.prepareStatement(sql);
                    ps.setString(x, source.getServer());
                    ps.setString(++x, group);
                    ps.execute();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                    DB.close(null, ps, connection);
                    break block5;
                }
            }
            catch (Throwable throwable) {
                DB.close(null, ps, connection);
                throw throwable;
            }
            DB.close(null, ps, connection);
        }
    }
}

