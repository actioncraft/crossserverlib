package io.dblosevn.xserver.bukkit.objects;

import io.dblosevn.xserver.bukkit.objects.Packet;
import io.dblosevn.xserver.bukkit.sockets.packets.BackPacket;
import io.dblosevn.xserver.bukkit.sockets.packets.ChatPacket;
import io.dblosevn.xserver.bukkit.sockets.packets.CommandPacket;
import io.dblosevn.xserver.bukkit.sockets.packets.ModeratorPacket;
import io.dblosevn.xserver.bukkit.sockets.packets.PingPacket;
import io.dblosevn.xserver.bukkit.sockets.packets.PluginEventPacket;
import io.dblosevn.xserver.bukkit.sockets.packets.ProxyStatusPacket;
import io.dblosevn.xserver.bukkit.sockets.packets.TeleportPacket;

public enum PacketType {
    PLUGIN_EVENT(PluginEventPacket.class, 1),
    CHAT(ChatPacket.class, 2),
    COMMAND(CommandPacket.class, 3),
    BACK_LOCATION(BackPacket.class, 4),
    PROXY_STATUS(ProxyStatusPacket.class, 5),
    TELEPORT(TeleportPacket.class, 6),
    PING(PingPacket.class, 7),
    MOD_PACKET(ModeratorPacket.class, 8),
    UNKNOWN(Packet.class, 0);
    
    private final Class<? extends Packet> clazz;
    private final int id;

    private PacketType(Class<? extends Packet> clazz, int id) {
        this.clazz = clazz;
        this.id = id;
    }

    public Class<? extends Packet> getClazz() {
        return this.clazz;
    }

    public static PacketType getById(int id) {
        PacketType[] arrpacketType = PacketType.values();
        int n = arrpacketType.length;
        int n2 = 0;
        while (n2 < n) {
            PacketType t = arrpacketType[n2];
            if (t.getId() == id) {
                return t;
            }
            ++n2;
        }
        return UNKNOWN;
    }

    public int getId() {
        return this.id;
    }
}

