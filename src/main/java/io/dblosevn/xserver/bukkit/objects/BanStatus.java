package io.dblosevn.xserver.bukkit.objects;

public enum BanStatus {
	ACTIVE(), EXPIRED(), REBAN(), UNBANNED(), UNKNOWN()
}
