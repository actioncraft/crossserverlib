package io.dblosevn.xserver.bukkit.util;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import me.lucko.luckperms.api.Contexts;
import me.lucko.luckperms.api.Group;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.Node;
import me.lucko.luckperms.api.User;
import me.lucko.luckperms.api.caching.MetaData;
import me.lucko.luckperms.api.context.ImmutableContextSet;
import me.lucko.luckperms.api.manager.UserManager;

public class PermUtil {
    public static User getUser(UUID uuid) {
        UserManager userManager = CrossServerLib.getPerms().getUserManager();
        if (userManager.isLoaded(uuid)) {
        	return userManager.getUser(uuid);
        }
        CompletableFuture<User> userFuture = userManager.loadUser(uuid);

        return userFuture.join(); // ouch!
    }
    public static User getUser(String name) {
        @SuppressWarnings("deprecation")
		OfflinePlayer player = Bukkit.getOfflinePlayer(name);
        if (null == player) {
        	return null;
        }
        return getUser(player.getUniqueId());
    }
    public static User getUser(OfflinePlayer player) {
        return getUser(player.getUniqueId());
    }
    public static User getUser(Player player) {
        return getUser(player.getUniqueId());
    }
    
    public static String getGroupForServer(User user, String server) {
    	LuckPermsApi api = CrossServerLib.getPerms();
    	
    	ImmutableContextSet set = ImmutableContextSet.singleton("server", server);
    	Contexts context = api.getContextManager().formContexts(set);
    	String group = user.getPrimaryGroup();
    	int maxWeight = 0;
    	for (Node node: user.getAllNodes(context)) {
    		if (node.isGroupNode()) {
    			Group tmp = api.getGroup(node.getGroupName());
    			if (tmp.getWeight().orElse(0) > maxWeight || null == group) {
    				group = tmp.getName();
    				maxWeight = tmp.getWeight().orElse(0);
    			}
    		}
    	}
    	return group;

    }
    
    public static MetaData getMetaDataForServer(User user, String server) {
    	LuckPermsApi api = CrossServerLib.getPerms();
    	
    	ImmutableContextSet set = ImmutableContextSet.singleton("server", server);
    	Contexts context = api.getContextManager().formContexts(set);

    	return user.getCachedData().getMetaData(context);
    }
    public static String getColorForGroup(String group) {
    	LuckPermsApi api = CrossServerLib.getPerms();

    	String server = CrossServerLib.get().getConfig().getString("serverName", "Lobby");
    	ImmutableContextSet set = ImmutableContextSet.singleton("server", server);
    	Contexts context = api.getContextManager().formContexts(set);
    	Map<String, String> map = api.getGroup(group).getCachedData().getMetaData(context).getMeta();
    	
    	if (map.containsKey("tabcolor")) {
    		return map.get("tabcolor");
    	}
    	return "8";
    }
    
    public static boolean hasOwnPerm(Player player, String permission) {
    	LuckPermsApi api = CrossServerLib.getPerms();
    	User user = getUser(player);
    	String server = CrossServerLib.get().getConfig().getString("serverName", "Lobby");
    	ImmutableContextSet set = ImmutableContextSet.singleton("server", server);
    	Contexts context = api.getContextManager().formContexts(set);
    	    	
    	for (Node node: user.getAllNodes(context)) {
    		if (node.getPermission().equalsIgnoreCase(permission)) {
    			return node.getValue();
    		}
    	}
    	return false;
    }
}
