package io.dblosevn.xserver.bukkit.util;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.events.global.CrossServerBackLocationEvent;
import io.dblosevn.xserver.bukkit.sockets.lib.Server;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class Util {
    private static String messagePrefix = CrossServerLib.get().getConfig().getString("chat.channels.broadcast.broadcastTag");

    public static String getTag() {
        return messagePrefix;
    }
    
    public static void pluginMessage(CommandSender p, String message) {
        p.sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)(String.valueOf(Util.getTag()) + message)));
    }

    public static void modMessage(String message) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (!p.hasPermission("crossserver.mod.message")) continue;
            p.sendMessage(ChatColor.translateAlternateColorCodes((char)'&', (String)(String.valueOf(Util.getTag()) + message)));
        }
    }

    public static String timeToString(long time) {
        String res = "";
        long days = TimeUnit.MILLISECONDS.toDays(time);
        long hours = TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(time));
        long minutes = TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time));
        long seconds = TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time));
        res = days == 0L ? (hours == 0L ? (minutes == 0L ? String.format("%02ds", seconds) : String.format("%dm %02ds", minutes, seconds)) : String.format("%dh %01dm", hours, minutes)) : String.format("%dd %dh", days, hours);
        return res;
    }
    public static long stringToDuration(String time, String delim) {
    	String parts[] = time.split(delim);
    	long out = 0;
    	for (String s: parts) {
    		String unit = s.substring(s.length() - 1);
    		int amount = Integer.parseInt(s.substring(0, s.length() - 1));
    		switch (unit) {
				case "s": out += (amount); break;
				case "m": out += (amount * 60); break;
    			case "h": out += (amount * 60 * 60); break;
    			case "d": out += (amount * 60 * 60 * 24); break;
    		}
    	}
    	return out;
    }
    public static void connect(Player player, String server) {
        try {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            DataOutputStream out = new DataOutputStream(b);
            Server.client.send(new CrossServerBackLocationEvent(player, server).getPacket());
            out.writeUTF("Connect");
            out.writeUTF(server);
            pluginMessage(player, CrossServerLib.primaryColor + "Connecting to: " + CrossServerLib.secondaryColor + server);
            player.sendPluginMessage((Plugin)CrossServerLib.get(), "BungeeCord", b.toByteArray());
        }
        catch (IOException b) {
            // empty catch block
        }
    }
}