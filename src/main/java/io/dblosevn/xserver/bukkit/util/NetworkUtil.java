package io.dblosevn.xserver.bukkit.util;

import java.io.UnsupportedEncodingException;

public class NetworkUtil {
    public static byte[] hexByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        int i = 0;
        while (i < len) {
            data[i / 2] = (byte)((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
            i += 2;
        }
        return data;
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder sbuf = new StringBuilder();
        int idx = 0;
        while (idx < bytes.length) {
            int intVal = bytes[idx] & 255;
            if (intVal < 16) {
                sbuf.append("0");
            }
            sbuf.append(Integer.toHexString(intVal).toUpperCase());
            ++idx;
        }
        return sbuf.toString();
    }

    public static String hexDump(byte[] array, int offset, int length) {
        StringBuilder builder = new StringBuilder();
        int rowOffset = offset;
        while (rowOffset < offset + length) {
            builder.append(String.format("%06d:  ", rowOffset));
            int index = 0;
            while (index < 16) {
                if (rowOffset + index < array.length) {
                    builder.append(String.format("%02x ", array[rowOffset + index]));
                } else {
                    builder.append("   ");
                }
                ++index;
            }
            if (rowOffset < array.length) {
                int asciiWidth = Math.min(16, array.length - rowOffset);
                builder.append("  |  ");
                try {
                    builder.append(new String(array, rowOffset, asciiWidth, "UTF-8").replaceAll("\r\n", " ").replaceAll("\n", " "));
                }
                catch (UnsupportedEncodingException unsupportedEncodingException) {
                    // empty catch block
                }
            }
            builder.append(String.format("%n", new Object[0]));
            rowOffset += 16;
        }
        return builder.toString();
    }
}

