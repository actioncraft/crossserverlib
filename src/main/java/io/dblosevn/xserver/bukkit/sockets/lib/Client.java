package io.dblosevn.xserver.bukkit.sockets.lib;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.objects.Packet;
import io.dblosevn.xserver.bukkit.sockets.lib.TcpClient;
import io.dblosevn.xserver.bukkit.sockets.lib.TcpClientEventHandler;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public abstract class Client {
    boolean shutdown = false;
    final TcpClient that_sock;
    boolean reconnecting = false;
    boolean connected = false;
    long timeout = 3000L;

    public Client(String host, int port, long timeout) {
        this(host, port);
        this.timeout = timeout;
    }

    public Client(String host, int port) {
        TcpClient sock;
        this.that_sock = sock = new TcpClient(host, port);
        Bukkit.getScheduler().runTaskAsynchronously((Plugin)CrossServerLib.get(), new Runnable(){

            @Override
            public void run() {
                sock.addEventHandler(new TcpClientEventHandler(){

                    @Override
                    public void onMessage(Packet packet) {
                        Client.this.onMessage(packet);
                    }

                    @Override
                    public void onOpen() {
                        Client.this.connected = true;
                        Client.this.onOpen();
                        System.out.println("* Connection Estabished.");
                        Client.this.reconnecting = false;
                    }

                    @Override
                    public void onClose() {
                    	Client.this.connected = false;
                        Client.this.onClose();
                        System.out.println("* Connection Lost.");
                        if (!Client.this.shutdown) {
                            try {
                                Thread.sleep(Client.this.timeout);
                            }
                            catch (Exception exception) {
                                // empty catch block
                            }
                            System.out.println("* Reconnecting..");
                            Client.this.reconnecting = true;
                            Client.this.that_sock.connect();
                        }
                    }
                });
                sock.connect();
            }

        });
    }

    protected abstract void onMessage(Packet var1);

    protected abstract void onOpen();

    protected abstract void onClose();

    protected abstract void onSend(Packet var1);

    public void shutdown() {
        this.shutdown = true;
        this.reconnecting = false;
        this.that_sock.close();
    }

    public void send(Packet packet) {
        this.onSend(packet);
        this.that_sock.send(packet);
    }

}

