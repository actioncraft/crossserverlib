package io.dblosevn.xserver.bukkit.sockets.packets;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import io.dblosevn.xserver.bukkit.objects.CrossServerEvent;
import io.dblosevn.xserver.bukkit.objects.Packet;

public class ChatPacket
extends Packet {
    public ChatPacket() {
    }

    public ChatPacket(CrossServerEvent event) {
        super("Chat Packet", 2, event);
    }

    @Override
    protected void buildPacketHeaders(ByteArrayDataOutput out) {
    }

    @Override
    protected void buildPacketData(ByteArrayDataOutput out) {
    }

    @Override
    protected void deSerializeHeaders(ByteArrayDataInput in) {
    }

    @Override
    protected void deSerializeData(ByteArrayDataInput in) {
        this.packetId = 2;
    }
}

