package io.dblosevn.xserver.bukkit.sockets.lib;

import io.dblosevn.xserver.bukkit.objects.Packet;

public interface TcpClientEventHandler {
    public void onMessage(Packet var1);

    public void onOpen();

    public void onClose();
}

