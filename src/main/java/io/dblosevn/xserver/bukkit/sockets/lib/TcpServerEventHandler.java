package io.dblosevn.xserver.bukkit.sockets.lib;

import io.dblosevn.xserver.bukkit.objects.Packet;

public interface TcpServerEventHandler {
    public void onMessage(int var1, Packet var2);

    public void onAccept(int var1);

    public void onClose(int var1);
}

