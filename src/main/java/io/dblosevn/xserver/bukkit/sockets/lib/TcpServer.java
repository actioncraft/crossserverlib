package io.dblosevn.xserver.bukkit.sockets.lib;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.objects.Packet;
import io.dblosevn.xserver.bukkit.sockets.lib.TcpClient;
import io.dblosevn.xserver.bukkit.sockets.lib.TcpClientEventHandler;
import io.dblosevn.xserver.bukkit.sockets.lib.TcpServerEventHandler;
import java.net.ServerSocket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class TcpServer {
    private ServerSocket server;
    private TcpServerEventHandler handler;
    private CopyOnWriteArrayList<TcpClient> clients = new CopyOnWriteArrayList<TcpClient>();
    private ConcurrentHashMap<TcpClient, String> status = new ConcurrentHashMap<TcpClient, String>();
    private boolean closer = false;
    private int readInterval;

    public CopyOnWriteArrayList<TcpClient> getClients() {
        return this.clients;
    }

    public int getReadInterval() {
        return this.readInterval;
    }

    public void setReadInterval(int msec) {
        this.readInterval = msec;
        for (TcpClient sock : this.clients) {
            sock.setReadInterval(msec);
        }
    }

    public TcpServer(int port) {
        try {
            this.server = new ServerSocket(port);
        }
        catch (Exception exception) {
            // empty catch block
        }
    }

    public void listen() {
        Bukkit.getScheduler().runTaskAsynchronously((Plugin)CrossServerLib.get(), new Runnable(){

            @Override
            public void run() {
                while (!TcpServer.this.closer) {
                    try {
                        final TcpClient sock = new TcpClient(TcpServer.this.server.accept());
                        TcpServer.this.clients.add(sock);
                        final int cid = TcpServer.this.clients.size() - 1;
                        TcpServer.this.handler.onAccept(cid);
                        if (TcpServer.this.handler != null) {
                            sock.addEventHandler(new TcpClientEventHandler(){

                                @Override
                                public void onMessage(Packet packet) {
                                    TcpServer.this.handler.onMessage(cid, packet);
                                }

                                @Override
                                public void onOpen() {
                                }

                                @Override
                                public void onClose() {
                                    if (!TcpServer.this.closer) {
                                        TcpServer.this.handler.onClose(cid);
                                    }
                                    TcpServer.this.status.remove(sock);
                                    TcpServer.this.clients.remove(sock);
                                }
                            });
                        }
                        sock.run();
                    }
                    catch (Exception sock) {
                        // empty catch block
                    }
                }
            }

        });
    }

    public TcpClient getClient(int id) {
        return this.clients.get(id);
    }

    public void close() {
        this.closer = true;
        try {
            this.server.close();
            for (TcpClient sock : this.clients) {
                sock.close();
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void addEventHandler(TcpServerEventHandler handler) {
        this.handler = handler;
    }

}

