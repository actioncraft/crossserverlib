package io.dblosevn.xserver.bukkit.sockets.lib;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.events.global.CrossServerPingEvent;
import io.dblosevn.xserver.bukkit.events.global.CrossServerProxyStatusEvent;
import io.dblosevn.xserver.bukkit.objects.Packet;
import io.dblosevn.xserver.bukkit.sockets.lib.Client;
import io.dblosevn.xserver.bukkit.sockets.lib.TcpClient;
import io.dblosevn.xserver.bukkit.sockets.lib.TcpServer;
import io.dblosevn.xserver.bukkit.sockets.lib.TcpServerEventHandler;
import io.dblosevn.xserver.bukkit.sockets.packets.ProxyStatusPacket;
import io.dblosevn.xserver.bungee.sockets.PacketType;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Event;
import org.bukkit.plugin.Plugin;

public class Server {
    public static TcpServer server;
    public static Client client;
    public static Logger logger;
    public static TcpClient proxyClient;

    static {
        logger = Logger.getLogger("Minecraft");
        proxyClient = null;
    }

    public static void onDisable() {
        if (server != null) {
            server.close();
        }
        client.shutdown();
        FileConfiguration config = CrossServerLib.get().getConfig();
        if (config.getBoolean("xserver.isServer")) {
            for (TcpClient c : server.getClients()) {
            	c.close();
            }
            
        	server.close();
        }
    }

    public static void onEnable() {
        FileConfiguration config = CrossServerLib.get().getConfig();
        if (config.getBoolean("xserver.isServer")) {
            server = new TcpServer(config.getInt("xserver.port"));
            server.addEventHandler(new TcpServerEventHandler(){

                @Override
                public void onMessage(int client_id, Packet packet) {
                    Server.send(packet);
                }

                @Override
                public void onClose(int client_id) {
                    Server.logger.info("Server - ProxyClient " + client_id + " disconnected");
                }

                @Override
                public void onAccept(int client_id) {
                    Server.logger.info("Server - ProxyClient has connected (ID: " + client_id + ")");
                }
            });
            server.listen();
            Bukkit.getScheduler().runTaskTimerAsynchronously((Plugin)CrossServerLib.get(), new Runnable(){

                @Override
                public void run() {
                    Server.send(new ProxyStatusPacket(new CrossServerProxyStatusEvent(1)));
                }
            }, 0L, 100L);
        }
        client = new Client(config.getString("xserver.host"), config.getInt("xserver.port")){

            @Override
            protected void onSend(Packet packet) {
            }

            @Override
            protected void onOpen() {
                Server.logger.info("ProxyClient - Connected to server!");
            }

            @Override
            protected void onMessage(Packet packet) {
                Bukkit.getServer().getPluginManager().callEvent((Event)packet.getEvent());
            }

            @Override
            protected void onClose() {
                Server.logger.info("ProxyClient - Disconnected");
            }
        };
        Bukkit.getScheduler().runTaskTimerAsynchronously((Plugin)CrossServerLib.get(), new Runnable(){

            @Override
            public void run() {
                if (Server.client.connected) {
                    Server.client.send(new CrossServerPingEvent().getPacket());
                }
            }
        }, 20L, 100L);
    }

    public static void send(Packet packet) {
        proxyClient = null;
        for (TcpClient c : server.getClients()) {
            if (c.isProxy()) {
                proxyClient = c;
                if (PacketType.getById(packet.getPacketId()).equals(PacketType.UNKNOWN) || packet.getType().equals(io.dblosevn.xserver.bukkit.objects.PacketType.PROXY_STATUS) && ((CrossServerProxyStatusEvent)packet.getEvent()).isResponse()) continue;
            }
            if (packet.getType().equals(io.dblosevn.xserver.bukkit.objects.PacketType.PING) || packet.getType().equals(io.dblosevn.xserver.bukkit.objects.PacketType.PROXY_STATUS) && !((CrossServerProxyStatusEvent)packet.getEvent()).isResponse() && !c.isProxy()) continue;
            c.send(packet);
        }
    }

}

