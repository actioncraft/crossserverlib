package io.dblosevn.xserver.bukkit.sockets.packets;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import io.dblosevn.xserver.bukkit.objects.CrossServerEvent;
import io.dblosevn.xserver.bukkit.objects.Packet;

public class ModeratorPacket
extends Packet {
    public ModeratorPacket() {
    }

    public ModeratorPacket(CrossServerEvent event) {
        super("Mod Packet", 8, event);
    }

    @Override
    protected void buildPacketHeaders(ByteArrayDataOutput out) {
    }

    @Override
    protected void buildPacketData(ByteArrayDataOutput out) {
    }

    @Override
    protected void deSerializeHeaders(ByteArrayDataInput in) {
    }

    @Override
    protected void deSerializeData(ByteArrayDataInput in) {
        this.packetId = 8;
    }
}

