package io.dblosevn.xserver.bukkit.sockets.lib;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.events.global.CrossServerProxyStatusEvent;
import io.dblosevn.xserver.bukkit.objects.Packet;
import io.dblosevn.xserver.bukkit.objects.PacketRegistry;
import io.dblosevn.xserver.bukkit.objects.PacketType;
import io.dblosevn.xserver.bukkit.sockets.lib.TcpClientEventHandler;
import io.dblosevn.xserver.bukkit.util.NetworkUtil;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;

import javax.xml.bind.DatatypeConverter;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

public class TcpClient {
    private String host;
    private int port;
    private Socket sock;
    private int readInterval = 0;
    private boolean isProxy = false;
    private BufferedWriter bWriter;
    private BufferedReader bReader;
    private InputStreamReader iReader;
    private TcpClientEventHandler handler;
    private boolean closer = false;
    private BukkitTask readThread = null;

    public int getReadInterval() {
        return this.readInterval;
    }

    public void setReadInterval(int msec) {
        this.readInterval = msec;
    }

    public TcpClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public TcpClient(Socket connected_socket) {
        this.sock = connected_socket;
    }

    public boolean run() {
        this.closer = false;
        try {
            this.bWriter = new BufferedWriter(new OutputStreamWriter(this.sock.getOutputStream()));
            this.iReader = new InputStreamReader(this.sock.getInputStream());
            this.bReader = new BufferedReader(this.iReader);
        }
        catch (ConnectException ex) {
            if (this.handler != null) {
                this.handler.onClose();
            }
            return false;
        }
        catch (Exception ex) {
            this.close();
            if (this.handler != null) {
                this.handler.onClose();
            }
            return false;
        }
        final TcpClient that = this;
        this.readThread = Bukkit.getScheduler().runTaskAsynchronously((Plugin)CrossServerLib.get(), new Runnable(){

            @Override
            public void run() {
                while (!TcpClient.this.closer && !Thread.interrupted()) {
                    try {
                        Thread.sleep(TcpClient.this.readInterval);
                        String line = TcpClient.this.bReader.readLine();
                        Packet packet = PacketRegistry.deserialize(DatatypeConverter.parseHexBinary(line.substring(8)));
                        if (packet == null) continue;
                        if (packet.getType().equals(PacketType.PROXY_STATUS) && ((CrossServerProxyStatusEvent)packet.getEvent()).isResponse()) {
                        	TcpClient.this.isProxy = true;
                        }
                        if (TcpClient.this.handler == null) continue;
                        TcpClient.this.handler.onMessage(packet);
                    }
                    catch (SocketException ex) {
                        that.close();
                    }
                    catch (IOException ex) {
                        that.close();
                    }
                    catch (Exception ex) {
                        that.close();
                    }
                }
            }
        });
        if (this.handler != null) {
            this.handler.onOpen();
        }
        return true;
    }

    public boolean connect() {
        if (this.sock != null) {
            return false;
        }
        try {
            this.sock = new Socket(this.host, this.port);
        }
        catch (ConnectException ex) {
            if (this.handler != null) {
                this.handler.onClose();
            }
            return false;
        }
        catch (Exception ex) {
            this.close();
            if (this.handler != null) {
                this.handler.onClose();
            }
            return false;
        }
        if (this.readThread != null) {
            this.readThread.cancel();
        }
        return this.run();
    }

    public void close() {
        try {
            this.closer = true;
            this.sock.close();
            this.sock = null;
            if (this.readThread != null) {
                this.readThread.cancel();
            }
            if (this.handler != null) {
                this.handler.onClose();
            }
        }
        catch (Exception exception) {
            // empty catch block
        }
    }

    public boolean send(Packet line) {
        if (this.sock == null) {
            return false;
        }
        try {
            this.bWriter.write(String.valueOf(NetworkUtil.bytesToHex(line.getBytes())) + "\r\n");
            this.bWriter.flush();
        }
        catch (Exception ex) {
            this.close();
            if (this.handler != null) {
                this.handler.onClose();
            }
            return false;
        }
        return true;
    }

    public void addEventHandler(TcpClientEventHandler handler) {
        this.handler = handler;
    }

    public boolean isProxy() {
        return this.isProxy;
    }

}

