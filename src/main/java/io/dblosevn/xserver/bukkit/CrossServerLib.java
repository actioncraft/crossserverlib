package io.dblosevn.xserver.bukkit;

import io.dblosevn.xserver.bukkit.api.DB;
import io.dblosevn.xserver.bukkit.api.DBSchema;
import io.dblosevn.xserver.bukkit.commands.ChatChannelCommand;
import io.dblosevn.xserver.bukkit.commands.OnlineCommand;
import io.dblosevn.xserver.bukkit.commands.PMChannelCommand;
import io.dblosevn.xserver.bukkit.commands.SeenCommand;
import io.dblosevn.xserver.bukkit.commands.ServerCommand;
import io.dblosevn.xserver.bukkit.commands.StopCommand;
import io.dblosevn.xserver.bukkit.commands.SyncCommandCommand;
import io.dblosevn.xserver.bukkit.commands.TPCommand;
import io.dblosevn.xserver.bukkit.commands.WhitelistCommand;
import io.dblosevn.xserver.bukkit.commands.ban.BanCommand;
import io.dblosevn.xserver.bukkit.commands.ban.KickCommand;
import io.dblosevn.xserver.bukkit.commands.ban.TempBanCommand;
import io.dblosevn.xserver.bukkit.commands.ban.UnBanCommand;
import io.dblosevn.xserver.bukkit.commands.homes.DelHomeCommand;
import io.dblosevn.xserver.bukkit.commands.homes.SetHomeCommand;
import io.dblosevn.xserver.bukkit.commands.spawns.DelSpawnCommand;
import io.dblosevn.xserver.bukkit.commands.spawns.SetSpawnCommand;
import io.dblosevn.xserver.bukkit.commands.spawns.SpawnsCommand;
import io.dblosevn.xserver.bukkit.commands.warps.DelWarpCommand;
import io.dblosevn.xserver.bukkit.commands.warps.SetWarpCommand;
import io.dblosevn.xserver.bukkit.listeners.BungeePluginMessageListener;
import io.dblosevn.xserver.bukkit.listeners.ChatListener;
import io.dblosevn.xserver.bukkit.listeners.ColorListener;
import io.dblosevn.xserver.bukkit.listeners.CommandListener;
import io.dblosevn.xserver.bukkit.listeners.EssListener;
import io.dblosevn.xserver.bukkit.listeners.PlayerManagementListener;
import io.dblosevn.xserver.bukkit.sockets.lib.Server;
import me.lucko.luckperms.api.LuckPermsApi;
import net.milkbowl.vault.chat.Chat;

import java.io.InputStreamReader;
import java.lang.reflect.Field;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandMap;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.bringholm.nametagchanger.NameTagChanger;
import com.earth2me.essentials.IEssentials;

public class CrossServerLib extends JavaPlugin {
	BungeePluginMessageListener blistener;
	private static Chat chat = null;
	private static LuckPermsApi perms = null;
	private static IEssentials essentials = null;

	public static CrossServerLib instance;
	public static ChatColor primaryColor;
	public static ChatColor secondaryColor;
	
	public void onEnable() {
		instance = this;
		saveDefaultConfig();
		InputStreamReader defConfigStream = new InputStreamReader(getResource("config.yml"));
		YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
		getConfig().setDefaults(defConfig);
		getConfig().options().copyDefaults(true);
		saveConfig();
		setupChat();
		setupPerms();
		setupEssentials();
		primaryColor = ChatColor.valueOf(getConfig().getString("textColors.primary", "DARK_PURPLE"));
		secondaryColor = ChatColor.valueOf(getConfig().getString("textColors.secondary", "YELLOW"));
		
		try {
			DB.initialize();
			DBSchema.createTables();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Server.onEnable();
		if (getConfig().getBoolean("chat.enabled")) {
			getServer().getPluginManager().registerEvents(new ChatListener(), this);
		}
		if (getConfig().getBoolean("tabColors")) {
			getServer().getPluginManager().registerEvents(new ColorListener(), this);
			ColorListener.setTabColors();
		}
		ChatChannelCommand chatCommand = new ChatChannelCommand();
		if (getConfig().getBoolean("chat.channels.global.enabled", true))
			getCommand("global").setExecutor(chatCommand);
		if (getConfig().getBoolean("chat.channels.local.enabled", true))
			getCommand("local").setExecutor(chatCommand);
		if (getConfig().getBoolean("chat.channels.staff.enabled", true))
			getCommand("staff").setExecutor(chatCommand);
		if (getConfig().getBoolean("chat.channels.admin.enabled", true))
			getCommand("admin").setExecutor(chatCommand);
		if (getConfig().getBoolean("chat.channels.broadcast.enabled", true))
			getCommand("broadcast").setExecutor(chatCommand);

		if (getConfig().getBoolean("chat.channels.pm.enabled", true)) {
			PMChannelCommand pmCommand = new PMChannelCommand();
			getCommand("msg").setExecutor(pmCommand);
			getCommand("pm").setExecutor(pmCommand);
			getCommand("r").setExecutor(pmCommand);
			getCommand("reply").setExecutor(pmCommand);
			getCommand("tell").setExecutor(pmCommand);
			getCommand("w").setExecutor(pmCommand);
			getCommand("whisper").setExecutor(pmCommand);
		}
		
		TPCommand tpCommand = new TPCommand();
		getCommand("back").setExecutor(tpCommand);
		getCommand("tp").setExecutor(tpCommand);
		getCommand("tphere").setExecutor(tpCommand);
		getCommand("tpo").setExecutor(tpCommand);
		getCommand("tpohere").setExecutor(tpCommand);
		getCommand("tppos").setExecutor(tpCommand);
		getCommand("tpa").setExecutor(tpCommand);
		getCommand("tpahere").setExecutor(tpCommand);
		getCommand("tpaccept").setExecutor(tpCommand);
		getCommand("tpdeny").setExecutor(tpCommand);
		getCommand("tpacancel").setExecutor(tpCommand);
		getCommand("home").setExecutor(tpCommand);
		getCommand("warp").setExecutor(tpCommand);
		getCommand("spawn").setExecutor(tpCommand);
		
		getCommand("sethome").setExecutor(new SetHomeCommand());
		getCommand("delhome").setExecutor(new DelHomeCommand());

		getCommand("setwarp").setExecutor(new SetWarpCommand());
		getCommand("delwarp").setExecutor(new DelWarpCommand());
	
		getCommand("setspawn").setExecutor(new SetSpawnCommand());
		getCommand("delspawn").setExecutor(new DelSpawnCommand());
		getCommand("spawns").setExecutor(new SpawnsCommand());
		
		getCommand("synccommand").setExecutor(new SyncCommandCommand());
		getCommand("online").setExecutor(new OnlineCommand());
		getCommand("seen").setExecutor(new SeenCommand());
		getCommand("stop").setExecutor(new StopCommand());
		getCommand("restart").setExecutor(getCommand("stop").getExecutor());
		getCommand("server").setExecutor(new ServerCommand());
		getCommand("whitelist").setExecutor(new WhitelistCommand());

		getCommand("kick").setExecutor(new KickCommand());
		getCommand("ban").setExecutor(new BanCommand());
		getCommand("tempban").setExecutor(new TempBanCommand());
		getCommand("unban").setExecutor(new UnBanCommand());
		
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		getServer().getPluginManager().registerEvents(new CommandListener(), this);
		getServer().getPluginManager().registerEvents(new PlayerManagementListener(), this);
		getServer().getPluginManager().registerEvents(new EssListener(), this);
	}

	public void onDisable() {
		ColorListener.onDisable();
		if (NameTagChanger.INSTANCE.isEnabled()) {
		    NameTagChanger.INSTANCE.disable();
		}
		
		getServer().getMessenger().unregisterIncomingPluginChannel(instance);
		getServer().getMessenger().unregisterOutgoingPluginChannel(instance);
		Server.onDisable();
		DB.close();
	}

	private boolean setupPerms() {
		RegisteredServiceProvider<LuckPermsApi> provider = getServer().getServicesManager().getRegistration(LuckPermsApi.class);
		if (provider != null) {
		    perms = provider.getProvider();
		    return true;
		}
		return false;
	}

	private boolean setupChat() {
		RegisteredServiceProvider<Chat> provider = getServer().getServicesManager().getRegistration(Chat.class);
		if (provider != null) {
		    chat = provider.getProvider();
		    return true;
		}
		return false;
	}

	private boolean setupEssentials() {
		IEssentials ess = (IEssentials) getServer().getPluginManager().getPlugin("Essentials");
		if (ess != null) {
		    essentials = ess;
		    return true;
		}
		return false;
	}

	public CommandMap getCommandMap() {
		try {
			Field bukkitCommandMap = getServer().getClass().getDeclaredField("commandMap");
			bukkitCommandMap.setAccessible(true);
			return (CommandMap) bukkitCommandMap.get(getServer());
		} catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static LuckPermsApi getPerms() {
		return perms;
	}
	public static Chat getChat() {
		return chat;
	}
	public static IEssentials getEss() {
		return essentials;
	}

	public static CrossServerLib get() {
		return instance;
	}
}
