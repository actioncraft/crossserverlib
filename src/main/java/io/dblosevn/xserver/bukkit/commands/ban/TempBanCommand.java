package io.dblosevn.xserver.bukkit.commands.ban;

import io.dblosevn.xserver.bukkit.commands.TabCommand;
import io.dblosevn.xserver.bukkit.events.global.CrossServerModeratorEvent;
import io.dblosevn.xserver.bukkit.objects.ModAction;
import io.dblosevn.xserver.bukkit.objects.ProxyPlayer;
import io.dblosevn.xserver.bukkit.objects.SQL;
import io.dblosevn.xserver.bukkit.sockets.lib.Server;
import io.dblosevn.xserver.bukkit.util.Util;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TempBanCommand extends TabCommand {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("crossserver.mod.tempban")) {
			Util.pluginMessage(sender, "&4You do not have permission to do that.");
			return true;
		}
		if (args.length < 3) {
			Util.pluginMessage(sender, "&4player, length and ban message required.");
			return true;
		}
		String source = sender.getName();
		if (!(sender instanceof Player)) {
			source = "*console*";
		}
		String target = args[0];
		String message = "";
		long expires = Util.stringToDuration(args[1], ",");
		int x = 2;
		while (x < args.length) {
			message += " " + args[x];
			++x;
		}
		
		message = message.trim();
		CrossServerModeratorEvent event = new CrossServerModeratorEvent(source, target, message,
				ModAction.TEMP, expires);
		SQL.banPlayer(new ProxyPlayer(source), new ProxyPlayer(target), event.getAction(), event.getExpires(), event.getMessage());
		Server.client.send(event.getPacket());
		return true;
	}
}
