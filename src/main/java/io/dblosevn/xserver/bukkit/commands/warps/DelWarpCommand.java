package io.dblosevn.xserver.bukkit.commands.warps;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.objects.SQL;
import io.dblosevn.xserver.bukkit.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DelWarpCommand implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			Util.pluginMessage(sender, ChatColor.RED + "You may only run this command in game.");
			return true;
		}
		if (args.length == 1) {
			SQL.removeWarp(args[0]);
			Util.pluginMessage(sender,
					CrossServerLib.primaryColor + "Successfully created the warp " + CrossServerLib.secondaryColor + args[0]);
		} else {
			Util.pluginMessage(sender, CrossServerLib.primaryColor + "Usage: " + CrossServerLib.secondaryColor + "/delwarp [name]");
		}
		return true;
	}
}
