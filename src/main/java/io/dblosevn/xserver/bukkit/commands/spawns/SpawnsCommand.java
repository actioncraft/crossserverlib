package io.dblosevn.xserver.bukkit.commands.spawns;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.objects.SQL;
import io.dblosevn.xserver.bukkit.objects.Spawn;
import io.dblosevn.xserver.bukkit.objects.TeleportLocation;
import io.dblosevn.xserver.bukkit.util.Util;
import static io.dblosevn.xserver.bukkit.CrossServerLib.primaryColor;
import static io.dblosevn.xserver.bukkit.CrossServerLib.secondaryColor;

import java.util.LinkedList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class SpawnsCommand implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		LinkedList<Spawn> spawns = SQL.getSpawn(new TeleportLocation(CrossServerLib.get().getConfig().getString("serverName"), "world", 0, 0, 0, 0, 0));
		Util.pluginMessage(sender, secondaryColor + "-----------------" + primaryColor + "Spawns" + secondaryColor + "-----------------");
		Util.pluginMessage(sender, "");
		for (Spawn s: spawns) {
			TeleportLocation l = s.getLoc();
			Util.pluginMessage(sender, String.format(primaryColor + "Perm: " + secondaryColor + "spawn.%s" + primaryColor + " Loc: " + secondaryColor + "%s %.2f" + primaryColor + ", " + secondaryColor + "%.2f" + primaryColor + ", " + secondaryColor + "%.2f %.2f %.2f",
					s.getGroup(), l.getWorld(), l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch()));
		}
		Util.pluginMessage(sender, secondaryColor + "--------------------------------------");
		return true;
	}
}
