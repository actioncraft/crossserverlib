package io.dblosevn.xserver.bukkit.commands;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.commands.TabCommand;
import io.dblosevn.xserver.bukkit.objects.ProxyPlayer;
import io.dblosevn.xserver.bukkit.objects.SQL;
import io.dblosevn.xserver.bukkit.util.Util;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class SeenCommand
extends TabCommand {
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String player = args.length == 0 ? sender.getName() : args[0];
        SeenCommand.seen(sender, player);
        return true;
    }

    public static void seen(CommandSender sender, String playerName) {
        ProxyPlayer player = new ProxyPlayer(playerName);
        if (player.isValid()) {
            boolean online = CrossServerAPI.findPlayer(player.getName()) != null;
            long playTime = player.getPlayTime();
            playTime = online ? playTime + (System.currentTimeMillis() - player.getLastLogin()) : playTime;
            String onlineString = online ? CrossServerLib.secondaryColor + "is " + ChatColor.GREEN + "online" : CrossServerLib.secondaryColor + "seen " + ChatColor.WHITE + Util.timeToString(System.currentTimeMillis() - player.getLastLogout()) + " ago";
            Date joinDate = new Date(player.getJoinDate());
            SimpleDateFormat f = new SimpleDateFormat("MMM dd, YYYY");
            String name = player.getName();
            if (SQL.isBanned(player)) {
            	name = ChatColor.RED.toString() + ChatColor.STRIKETHROUGH + name + ChatColor.RESET;
            }
            String joined = f.format(joinDate);
            sender.sendMessage("");
            String nick = (player.getNick().equals("")) ? "" : (" &7[" + ChatColor.AQUA + player.getDisplayName() + "&7]");
            Util.pluginMessage(sender, player.getColor() + name + nick + ChatColor.GRAY + " (" + player.getId() + ") " + CrossServerLib.secondaryColor + onlineString + CrossServerLib.secondaryColor + " in " + player.getServer());
            Util.pluginMessage(sender, ChatColor.AQUA + "Balance: " + ChatColor.WHITE + "0");
            Util.pluginMessage(sender, ChatColor.AQUA + "Rank: " + player.getColor() + player.getRank());
            Util.pluginMessage(sender, ChatColor.AQUA + "Joined:" + ChatColor.RESET + " " + joined);
            Util.pluginMessage(sender, ChatColor.AQUA + "Play Time: " + ChatColor.WHITE + Util.timeToString(playTime));
            sender.sendMessage("");
        } else {
            Util.pluginMessage(sender, ChatColor.RED + "Player not found.");
        }
    }
}

