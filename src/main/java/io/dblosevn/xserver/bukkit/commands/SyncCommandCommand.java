package io.dblosevn.xserver.bukkit.commands;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.commands.TabCommand;
import io.dblosevn.xserver.bukkit.events.global.CrossServerCommandEvent;
import io.dblosevn.xserver.bukkit.sockets.lib.Server;
import io.dblosevn.xserver.bukkit.sockets.packets.CommandPacket;
import io.dblosevn.xserver.bukkit.util.Util;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.help.HelpTopic;

public class SyncCommandCommand extends TabCommand {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("crossserver.commands.synccommand")) {
			return false;
		}
		CrossServerCommandEvent event = new CrossServerCommandEvent(sender, String.join((CharSequence) " ", args));
		CommandPacket packet = new CommandPacket(event);
		Util.pluginMessage(sender, CrossServerLib.primaryColor + "Running command: " + CrossServerLib.secondaryColor + "/"
				+ String.join((CharSequence) " ", args) + CrossServerLib.primaryColor + " on all servers.");
		Server.client.send(packet);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		ArrayList<String> list = (ArrayList<String>) super.onTabComplete(sender, command, alias, args);
		if (!sender.hasPermission("crossserver.commands.synccommand")) {
			return list;
		}
		String token = "";
		if (args.length <= 0) {
			return list;
		}
		token = args[args.length - 1].toLowerCase();
		for (HelpTopic cmd : Bukkit.getServer().getHelpMap().getHelpTopics()) {
			if (!cmd.getName().replaceFirst("/", "").toLowerCase().startsWith(token))
				continue;
			list.add(cmd.getName().replaceFirst("/", ""));
		}
		return list;
	}
}
