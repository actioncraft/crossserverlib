package io.dblosevn.xserver.bukkit.commands;

import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class OnlineCommand implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		String template = ChatColor.AQUA + "%s " + ChatColor.GRAY + "(%s)";
		sender.sendMessage(String.valueOf(ChatColor.GRAY.toString()) + ChatColor.UNDERLINE
				+ "                                                                   ");
		sender.sendMessage("");
		sender.sendMessage(
				ChatColor.DARK_GREEN + "Servers Online: " + String.format(template, "Server", "Players Online"));
		int col = 0;
		String message = "";
		for (String server : CrossServerAPI.getServers()) {
			if (col == 3) {
				sender.sendMessage(message);
				message = "";
				col = 0;
			}
			message = String.valueOf(message) + " "
					+ String.format(template, server, CrossServerAPI.getServerPlayers(server).size());
			++col;
		}
		if (message.length() != 0) {
			sender.sendMessage(message);
		}
		sender.sendMessage("");
		sender.sendMessage(String.valueOf(ChatColor.GRAY.toString()) + ChatColor.UNDERLINE
				+ "                                                                   ");
		return true;
	}
}
