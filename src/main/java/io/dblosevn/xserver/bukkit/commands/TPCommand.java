package io.dblosevn.xserver.bukkit.commands;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.events.global.CrossServerTeleportEvent;
import io.dblosevn.xserver.bukkit.objects.*;
import io.dblosevn.xserver.bukkit.sockets.lib.Server;
import io.dblosevn.xserver.bukkit.sockets.packets.TeleportPacket;
import io.dblosevn.xserver.bukkit.util.PermUtil;
import io.dblosevn.xserver.bukkit.util.Util;
import java.util.*;

import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;

public class TPCommand extends TabCommand {

	public TPCommand() {
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String args[]) {
		String usage;
		boolean playerOffline;
		boolean valid;
		boolean hasPermission;
		CrossServerTeleportEvent event;
		if (!(sender instanceof Player))
			Util.pluginMessage(sender, ChatColor.RED + "You may only run this command in game.");
		usage = String.format(CrossServerLib.secondaryColor + "%s %s<player>", label, ChatColor.RED);

		TeleportType type = TeleportType.UNKNOWN;
		int minArgs = 1;
		int maxArgs = 1;
		playerOffline = false;
		valid = false;
		hasPermission = true;
		event = null;
		Player player = (Player) sender;
		
		switch (label.toLowerCase()) {
		case "tpa":
			type = TeleportType.TELEPORT_REQUEST;
			valid = valid(args, minArgs, maxArgs);
			if (valid) {
				if (CrossServerAPI.online(args[0])) {
					if (!sender.hasPermission("crossserver.teleport.tpa")) {
						hasPermission = false;
					} else {
						event = new CrossServerTeleportEvent(player, args[0], type);
					}
				} else {
					playerOffline = true;
				}
			}
		break;
		case "tpahere":
			type = TeleportType.TELEPORT_HERE_REQUEST;
			valid = valid(args, minArgs, maxArgs);
			if (valid)
				if (CrossServerAPI.online(args[0])) {
					if (!sender.hasPermission("crossserver.teleport.tpahere")) {
						hasPermission = false;
					} else {
						event = new CrossServerTeleportEvent(player, args[0], type);
					}
				} else {
					playerOffline = true;
				}
		break;
		case "tpaccept":
			usage = String.format(CrossServerLib.secondaryColor + "/%s ",  label, ChatColor.RED);
			minArgs = 0;
			maxArgs = 0;
			type = TeleportType.TELEPORT_ACCEPT;
			valid = valid(args, minArgs, maxArgs);
			if (valid) {
				if (!sender.hasPermission("crossserver.teleport.tpa")) {
					hasPermission = false;
				} else {
					event = new CrossServerTeleportEvent((Player) sender, type);
				}
			}
		break;
		case "tpdeny":
			usage = String.format(CrossServerLib.secondaryColor + "/%s ", label, ChatColor.RED );
			minArgs = 0;
			maxArgs = 0;
			type = TeleportType.TELEPORT_DENY;
			valid = valid(args, minArgs, maxArgs);
			if (valid) {
				if (!sender.hasPermission("crossserver.teleport.tpa")) {
					hasPermission = false;
				} else {
					event = new CrossServerTeleportEvent((Player) sender, type);
				}
			}
		break;
		case "tpacancel":
			usage = String.format((new StringBuilder()).append(CrossServerLib.secondaryColor).append("/%s ").toString(),
					new Object[] { label, ChatColor.RED });
			minArgs = 0;
			maxArgs = 0;
			type = TeleportType.TELEPORT_CANCEL;
			valid = valid(args, minArgs, maxArgs);
			if (valid)
				if (!sender.hasPermission("crossserver.teleport.tpa"))
					hasPermission = false;
				else
					event = new CrossServerTeleportEvent((Player) sender, type);
		break;

		case "back":
				minArgs = 0;
				maxArgs = 0;
				valid = true;
				if (player.hasMetadata("tpbacklocation")) {
					for (MetadataValue mdv: player.getMetadata("tpbacklocation")) {
						if (mdv.getOwningPlugin().equals(CrossServerLib.get()))
							if (!sender.hasPermission("crossserver.teleport.back")) {
								hasPermission = false;
							} else {
								event = new CrossServerTeleportEvent(player, (TeleportLocation) mdv.value());
							}
					}
				}
		break;

		case "home":
			minArgs = 0;
			maxArgs = 2;
			usage = (String.format(CrossServerLib.secondaryColor + "/%s List all homes", label, ChatColor.RED) + "\r\n");
			usage += String.format(CrossServerLib.secondaryColor + "/%s %s<homename>", label, ChatColor.RED );
			valid = false;
			if (args.length == 0) {
				valid = true;
				LinkedList<Home> homes = SQL.getHomes(player.getUniqueId());
				if (homes.size() == 0) {
					Util.pluginMessage(player, ChatColor.RED + "You have no homes.");
					return true;
				}
				if (homes.size() == 1) {
					event = new CrossServerTeleportEvent(player, homes.get(0).getLoc());
				} else {
					LinkedList<String> homeList = new LinkedList<String>();
					for (Home h: homes) {
						homeList.add(h.getName());
					}

					Util.pluginMessage(player,
							CrossServerLib.primaryColor + "Current homes: " + CrossServerLib.secondaryColor + String.join(", ", homeList.toArray(new String[0])));
					return true;
				}
			} else if (args.length == 1) {
				valid = true;
				if (player.hasPermission("crossserver.teleport.home")) {
					Home home = SQL.getHome(player.getUniqueId(), args[0]);
					if (home == null)
						Util.pluginMessage(player, ChatColor.RED + "That home does not exist.");
					else
						event = new CrossServerTeleportEvent(player, home.getLoc());
				} else {
					hasPermission = false;
				}
			}
		break;

		case "warp":
				minArgs = 0;
				maxArgs = 1;
				usage = String.format(CrossServerLib.secondaryColor + "/%s List all warps", label, ChatColor.RED) + "\r\n";
				usage += String.format(CrossServerLib.secondaryColor +"/%s %s<warpname>", label, ChatColor.RED );
				valid = false;
				if (args.length == 0) {
					valid = true;
					LinkedList<Warp> warps = SQL.getWarps();
					if (warps.size() == 0) {
						Util.pluginMessage(player, ChatColor.RED + "There are no warps.");
						return true;
					}
					LinkedList<String> warpList = new LinkedList<String>();
					for (Warp h: warps) {
						warpList.add(h.getName());
					}
					
					Util.pluginMessage(player, CrossServerLib.primaryColor + "Current warps: " + CrossServerLib.secondaryColor + String.join(", ", warpList.toArray(new String[0])));
					return true;
				}
				if (args.length == 1) {
					valid = true;
					if (player.hasPermission("crossserver.teleport.warp")) {
						Warp warp = SQL.getWarp(args[0]);
						if (warp == null) {
							Util.pluginMessage(player, ChatColor.RED + "That warp does not exist.");
						} else {
							event = new CrossServerTeleportEvent(player, warp.getLoc());
						}
					} else {
						hasPermission = false;
					}
				}
		break;
		case "spawn":
			LinkedList<Spawn> spawns = SQL.getSpawn(new TeleportLocation(player.getLocation()));
	        for (Spawn s : spawns) {
	            if (!s.getGroup().equalsIgnoreCase("default")) {
	            	if (!PermUtil.hasOwnPerm(player, "spawn." + s.getGroup())) {
	            		continue;
	            	}
	            }
				valid = true;
				event = new CrossServerTeleportEvent(player, s.getLoc());
	            break;
	        }
		break;
		case "tppos":
			type = TeleportType.TO_LOCATION;
			usage = String.format(CrossServerLib.secondaryColor + "%s %s<x> <y> <z>", label, ChatColor.RED) +"\r\n";
			usage += String.format(CrossServerLib.secondaryColor + "%s %s<x> <y> <z> <yaw> <pitch>", label, ChatColor.RED) + "\r\n";
			usage += String.format(CrossServerLib.secondaryColor + "%s %s<world> <x> <y> <z>", label, ChatColor.RED) + "\r\n";
			usage += String.format(CrossServerLib.secondaryColor + "%s %s<world> <x> <y> <z> <yaw> <pitch>", label, ChatColor.RED) + "\r\n";
			usage += String.format(CrossServerLib.secondaryColor + "%s %s<server> <world> <x> <y> <z>", label, ChatColor.RED) + "\r\n";
			usage += String.format(CrossServerLib.secondaryColor + "%s %s<server> <world> <x> <y> <z> <yaw> <pitch>", label, ChatColor.RED);
			minArgs = 3;
			maxArgs = 7;
			valid = valid(args, minArgs, maxArgs);
			TeleportLocation to = null;
			
			try {
				if (args.length == 3 && NumberUtils.isNumber(args[0]) && NumberUtils.isNumber(args[1])
						&& NumberUtils.isNumber(args[2]))
					to = new TeleportLocation(CrossServerAPI.getServer(), player.getWorld().getName(),
							Double.parseDouble(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]),
							0.0F, 0.0F);
				else if (args.length == 5 && NumberUtils.isNumber(args[0]) && NumberUtils.isNumber(args[1])
						&& NumberUtils.isNumber(args[2]) && NumberUtils.isNumber(args[3])
						&& NumberUtils.isNumber(args[4]))
					to = new TeleportLocation(CrossServerAPI.getServer(), player.getWorld().getName(),
							Double.parseDouble(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]),
							Float.parseFloat(args[3]), Float.parseFloat(args[4]));
				else if (args.length == 4 && NumberUtils.isNumber(args[1]) && NumberUtils.isNumber(args[2])
						&& NumberUtils.isNumber(args[3]))
					to = new TeleportLocation(CrossServerAPI.getServer(), args[0], Double.parseDouble(args[1]),
							Double.parseDouble(args[2]), Double.parseDouble(args[3]), 0.0F, 0.0F);
				else if (args.length == 6 && NumberUtils.isNumber(args[1]) && NumberUtils.isNumber(args[2])
						&& NumberUtils.isNumber(args[3]) && NumberUtils.isNumber(args[4])
						&& NumberUtils.isNumber(args[5]))
					to = new TeleportLocation(CrossServerAPI.getServer(), args[0], Double.parseDouble(args[1]),
							Double.parseDouble(args[2]), Double.parseDouble(args[3]), Float.parseFloat(args[4]),
							Float.parseFloat(args[5]));
				else if (args.length == 5 && NumberUtils.isNumber(args[2]) && NumberUtils.isNumber(args[3])
						&& NumberUtils.isNumber(args[4]))
					to = new TeleportLocation(args[0], args[1], Double.parseDouble(args[2]),
							Double.parseDouble(args[3]), Double.parseDouble(args[4]), 0.0F, 0.0F);
				else if (args.length == 7 && NumberUtils.isNumber(args[2]) && NumberUtils.isNumber(args[3])
						&& NumberUtils.isNumber(args[4]) && NumberUtils.isNumber(args[5])
						&& NumberUtils.isNumber(args[6]))
					to = new TeleportLocation(args[0], args[1], Double.parseDouble(args[2]),
							Double.parseDouble(args[3]), Double.parseDouble(args[4]), Float.parseFloat(args[5]),
							Float.parseFloat(args[6]));
				else
					valid = false;
			} catch (Exception e) {
				valid = false;
			}
			
			if (valid) {
				if (!sender.hasPermission("crossserver.teleport.tppos")) {
					hasPermission = false;
				}
				event = new CrossServerTeleportEvent(player, to);
			}
		break;

		case "tp":
		case "tpo":
			type = TeleportType.PLAYER_TP;
			valid = valid(args, minArgs, maxArgs);
			if (valid)
				if (CrossServerAPI.online(args[0])) {
					if (!sender.hasPermission("crossserver.teleport.tp"))
						hasPermission = false;
					else
						event = new CrossServerTeleportEvent(player, args[0], type);
				} else {
					playerOffline = true;
				}
		break;
		case "tpohere":
		case "tphere":
			type = TeleportType.PLAYER_TPHERE;
			valid = valid(args, minArgs, maxArgs);
			if (valid) {
				if (CrossServerAPI.online(args[0])) {
					if (!sender.hasPermission("crossserver.teleport.tp")) {
						hasPermission = false;
					} else {
						event = new CrossServerTeleportEvent(player, args[0], type);
					}
				} else {
					playerOffline = true;
				}
			}
			break;
		}
		
		if (!hasPermission) {
			Util.pluginMessage(sender, (new StringBuilder()).append(ChatColor.RED)
					.append("You dont have permission to do that!").toString());
			return true;
		}
		if (!valid) {
			Util.pluginMessage(sender, (new StringBuilder()).append(ChatColor.RED).append("Usage: ").toString());
			String as[];
			int j = (as = usage.split("\r\n")).length;
			for (int i = 0; i < j; i++) {
				String m = as[i];
				Util.pluginMessage(sender, m);
			}

			return true;
		}
		if (playerOffline) {
			Util.pluginMessage(sender,
					(new StringBuilder()).append(ChatColor.RED).append("Player offline!").toString());
			return true;
		}
		if (event != null) {
			if (event.isLocal()) {
				event.teleport();
			} else {
				Server.client.send(new TeleportPacket(event));
			}
		}
		return true;
	}

	private boolean valid(String args[], int minArgs, int maxArgs) {
		return args.length >= minArgs && args.length <= maxArgs;
	}

	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String args[]) {
		ArrayList<String> list = (ArrayList<String>) super.onTabComplete(sender, command, alias, args);
		if (!(sender instanceof Player))
			return list;
		Player player = (Player) sender;
		if (alias.toLowerCase().equals("home")) {
			list.clear();
			LinkedList<Home> homes = SQL.getHomes(player.getUniqueId());
			for (Home h: homes) {
				if (args.length == 0 || h.getName().toLowerCase().startsWith(args[args.length - 1])) {
					list.add(h.getName());
				}
			}

		}
		if (alias.toLowerCase().equals("warp")) {
			list.clear();
			LinkedList<Warp> warps = SQL.getWarps();
			for (Warp w: warps) {
				if (args.length == 0 || w.getName().toLowerCase().startsWith(args[args.length - 1])) {
					list.add(w.getName());
				}
			}

		}
		return list;
	}
}