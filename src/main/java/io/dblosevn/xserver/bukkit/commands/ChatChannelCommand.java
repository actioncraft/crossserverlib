package io.dblosevn.xserver.bukkit.commands;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.chat.BaseChatChannel;
import io.dblosevn.xserver.bukkit.chat.ChatUtil;
import io.dblosevn.xserver.bukkit.commands.TabCommand;
import io.dblosevn.xserver.bukkit.events.global.CrossServerChatEvent;
import io.dblosevn.xserver.bukkit.sockets.lib.Server;
import io.dblosevn.xserver.bukkit.sockets.packets.ChatPacket;
import io.dblosevn.xserver.bukkit.util.Util;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

public class ChatChannelCommand extends TabCommand {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		BaseChatChannel channel = ChatUtil.lookup("/" + label + " ");
		String message = String.join((CharSequence) " ", args);
		if (channel != null && channel.getName().equals("broadcast")) {
			if (!channel.hasPermission(sender)) {
				Util.pluginMessage(sender, ChatColor.RED + "You do not have permission to use this channel.");
				return true;
			}
			CrossServerChatEvent event = new CrossServerChatEvent(sender, channel.getName(), message);
			ChatPacket packet = new ChatPacket(event);
			Server.client.send(packet);
			return true;
		}
		if (!(sender instanceof Player)) {
			Util.pluginMessage(sender, ChatColor.RED + "This command cannot be sent from console.");
			return true;
		}
		if (args.length == 0) {
			Util.pluginMessage(sender, ChatColor.RED + "You must type a message.");
			return true;
		}
		if (channel != null) {
			Player player = (Player) sender;
			if (!channel.hasPermission(player)) {
				player.sendMessage(String.valueOf(CrossServerLib.get().getConfig().getString("messagePrefix"))
						+ "You do not have permission to use this channel.");
				return true;
			}
			if (channel.getName() == "pm") {
				return true;
			}
			CrossServerChatEvent event = new CrossServerChatEvent(player, channel.getName(), message);
			if (!channel.isLocal()) {
				ChatPacket packet = new ChatPacket(event);
				Server.client.send(packet);
			} else {
				Bukkit.getServer().getPluginManager().callEvent((Event) event);
			}
		}
		return true;
	}
}
