package io.dblosevn.xserver.bukkit.commands.homes;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.objects.Home;
import io.dblosevn.xserver.bukkit.objects.SQL;
import io.dblosevn.xserver.bukkit.util.Util;
import java.util.LinkedList;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DelHomeCommand implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			Util.pluginMessage(sender, ChatColor.RED + "You may only run this command in game.");
			return true;
		}
		Player player = (Player) sender;
		String home = "";
		if (args.length == 0) {
			LinkedList<Home> homes = SQL.getHomes(player.getUniqueId());
			if (homes.size() == 1) {
				home = homes.get(0).getName();
			} else if (homes.size() == 0) {
				home = "home";
			}
		} else if (args.length == 1) {
			home = args[0];
		}
		if (!home.isEmpty()) {
			SQL.removeHome(player.getUniqueId(), home);
			Util.pluginMessage(sender,
					CrossServerLib.primaryColor + "Successfully removed the home " + CrossServerLib.secondaryColor + home);
		} else {
			Util.pluginMessage(sender, CrossServerLib.primaryColor + "Usage: " + CrossServerLib.secondaryColor + "/delhome [name]");
		}
		return true;
	}
}
