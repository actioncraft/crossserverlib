package io.dblosevn.xserver.bukkit.commands;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.util.Util;
import java.util.*;
import org.bukkit.*;
import org.bukkit.command.*;

public class WhitelistCommand
    implements CommandExecutor, TabCompleter
{

    public WhitelistCommand() {
    }

    @SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command command, String label, String args[]) {
        if(!sender.hasPermission("crossserver.whitelist")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to perform this command.");
            return true;
        }

        String usage = String.format(CrossServerLib.secondaryColor + "/%s %s<on|off>", label, ChatColor.RED) + "\n";
        usage += String.format(CrossServerLib.secondaryColor + "/%s %splayers", label, ChatColor.RED) + "\n";
        usage += String.format(CrossServerLib.secondaryColor + "/%s %saddplayer <player>", label, ChatColor.RED) + "\n";
        usage += String.format(CrossServerLib.secondaryColor + "/%s %sremoveplayer <player>", label, ChatColor.RED) + "\n";
        usage += String.format(CrossServerLib.secondaryColor + "/%s %sgroups", label, ChatColor.RED) + "\n";
        usage += String.format(CrossServerLib.secondaryColor + "/%s %saddgroup <groupname>", label, ChatColor.RED) + "\n";
        usage += String.format(CrossServerLib.secondaryColor + "/%s %sremovegroup <groupname>", label, ChatColor.RED) + "\n";
        usage += String.format(CrossServerLib.secondaryColor + "/%s %saction kick", label, ChatColor.RED) + "\n";
        usage += String.format(CrossServerLib.secondaryColor + "/%s %saction teleport <servername>", label, ChatColor.RED);

        if(args.length == 0) {
            Util.pluginMessage(sender, usage);
            return true;
        }
        ArrayList<String> players;
        ArrayList<String> groups;
        ArrayList<String> out;
        OfflinePlayer player;

        switch((args[0].toLowerCase()))
        {

        case "action": 
            if(args.length > 1) {
                switch(args[1].toLowerCase()) {
	                case "teleport": 
	                        if(args.length == 3) {
	                            ArrayList<String> servers = CrossServerAPI.getServers();
	                            for(String s: servers) {
	                                if(s.toLowerCase().equals(args[2].toLowerCase())) {
	                                    CrossServerLib.get().getConfig().set("whitelist.action.kick", false);
	                                    CrossServerLib.get().getConfig().set("whitelist.action.server", args[2].toLowerCase());
	                                    CrossServerLib.get().saveConfig();
	                                    Util.pluginMessage(sender, ChatColor.RED + "Whitelist action set to teleport to: " + CrossServerLib.secondaryColor + args[2]);
	                                    return true;
	                                }
	                            }
	
	                            Util.pluginMessage(sender, ChatColor.RED + "Server not found.");
	                        } else {
	                            usage = String.format(CrossServerLib.secondaryColor + "/%s %saction teleport <servername>", label, ChatColor.RED);
	                            Util.pluginMessage(sender, usage);
	                        }
                    break;
	                case "kick": 
                        CrossServerLib.get().getConfig().set("whitelist.action.kick", true);
                        CrossServerLib.get().saveConfig();
                        Util.pluginMessage(sender, ChatColor.RED + "Whitelist action set to kick.");
                    break;
                }
            } else {
                boolean shouldKick = CrossServerLib.get().getConfig().getBoolean("whitelist.action.kick", true);
                String server = CrossServerLib.get().getConfig().getString("whitelist.action.server", "hub");
                if(shouldKick) {
                    Util.pluginMessage(sender, ChatColor.RED + "Whitelist action is kick.");
                } else {
                    Util.pluginMessage(sender, ChatColor.RED + "Whitelist action is to teleport to server: " + server);
                }
            }
            return true;

        case "groups":
                groups = (ArrayList<String>)CrossServerLib.get().getConfig().getStringList("whitelist.groups");
                Util.pluginMessage(sender, CrossServerLib.primaryColor + "Whitelisted groups: " + CrossServerLib.secondaryColor + String.join(", ", groups.toArray(new String[0])));
                return true;

        case "addgroup":
            if(args.length == 2) {
                groups = (ArrayList<String>) CrossServerLib.get().getConfig().getStringList("whitelist.groups");
                if(!groups.contains(args[1].toLowerCase())) {
                    groups.add(args[1].toLowerCase());
                    CrossServerLib.get().getConfig().set("whitelist.groups", groups);
                    CrossServerLib.get().saveConfig();
                    Util.pluginMessage(sender, CrossServerLib.primaryColor + "Group added to whitelist.");
                } else {
                    Util.pluginMessage(sender, ChatColor.RED + "Group already whitelisted.");
                }
                return true;
            }
            usage = String.format(CrossServerLib.secondaryColor + "/%s %saddgroup <group>", label, ChatColor.RED + "\n");
           break;

        case "players":
            players = (ArrayList<String>) CrossServerLib.get().getConfig().getStringList("whitelist.players");
            out = new ArrayList<String>();
            for (String u: players.toArray(new String[0])) {
            	out.add(Bukkit.getOfflinePlayer(UUID.fromString(u)).getName());
            }

            Util.pluginMessage(sender, CrossServerLib.primaryColor + "Whitelisted players: " + CrossServerLib.secondaryColor + String.join(", ", out.toArray(new String[0])));
            return true;

        case "removegroup":
            if(args.length == 2) {
                groups = (ArrayList<String>) CrossServerLib.get().getConfig().getStringList("whitelist.groups");
                if(groups.contains(args[1].toLowerCase())) {
                    groups.remove(args[1].toLowerCase());
                    CrossServerLib.get().getConfig().set("whitelist.groups", groups);
                    CrossServerLib.get().saveConfig();
                    Util.pluginMessage(sender, CrossServerLib.primaryColor + "Group removed to whitelist.");
                } else {
                    Util.pluginMessage(sender, ChatColor.RED + "Group wasn't whitelisted.");
                }
                return true;
            }
            usage = String.format(CrossServerLib.secondaryColor + "/%s %saddgroup <group>", label, ChatColor.RED) + "\n";
        break;

        case "removeplayer":
            if(args.length == 2) {
                player = Bukkit.getOfflinePlayer(args[1]);
                if(player != null) {
                    players = (ArrayList<String>) CrossServerLib.get().getConfig().getStringList("whitelist.players");
                    if(players.contains(player.getUniqueId().toString())) {
                        players.remove(player.getUniqueId().toString());
                        CrossServerLib.get().getConfig().set("whitelist.players", players);
                        CrossServerLib.get().saveConfig();
                        Util.pluginMessage(sender, CrossServerLib.primaryColor + "Player removed from whitelist.");
                    } else {
                        Util.pluginMessage(sender, ChatColor.RED + "Player not whitelisted.");
                    }
                } else {
                    Util.pluginMessage(sender, (new StringBuilder()).append(ChatColor.RED).append("Player not found.").toString());
                }
                return true;
            }
            usage = String.format(CrossServerLib.secondaryColor + "/%s %sremoveplayer <player>", label, ChatColor.RED) + "\n";
           break;

        case "on":
            CrossServerLib.get().getConfig().set("whitelist.enabled", true);
            CrossServerLib.get().saveConfig();
            Util.pluginMessage(sender, CrossServerLib.primaryColor + "Whitelist enabled.");
            return true;

        case "off":
            CrossServerLib.get().getConfig().set("whitelist.enabled", false);
            CrossServerLib.get().saveConfig();
            Util.pluginMessage(sender, CrossServerLib.primaryColor + "Whitelist disabled.");
            return true;

        case "addplayer":
            if(args.length == 2) {
                player = Bukkit.getOfflinePlayer(args[1]);
                if(player != null) {
                    players = (ArrayList<String>)CrossServerLib.get().getConfig().getStringList("whitelist.players");
                    if(!players.contains(player.getUniqueId().toString())) {
                        players.add(player.getUniqueId().toString());
                        CrossServerLib.get().getConfig().set("whitelist.players", players);
                        CrossServerLib.get().saveConfig();
                        Util.pluginMessage(sender, CrossServerLib.primaryColor + "Player added to whitelist.");
                    } else {
                        Util.pluginMessage(sender, ChatColor.RED + "Player already whitelisted.");
                    }
                } else {
                    Util.pluginMessage(sender, ChatColor.RED + "Player not found.");
                }
                return true;
            }
            usage = String.format(CrossServerLib.secondaryColor + "/%s %saddplayer <player>", label, ChatColor.RED) + "\n";
            break;
        }
        Util.pluginMessage(sender, usage);
        return true;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String args[]) {
        ArrayList<String> opts = CrossServerAPI.getServers();
        ArrayList<String> out = new ArrayList<String>();
        if(args.length == 0)
            return opts;
        for(String s: opts) {
            if(s.toLowerCase().startsWith(args[args.length - 1].toLowerCase()))
                out.add(s);
        }

        return out;
    }
}
