package io.dblosevn.xserver.bukkit.commands.spawns;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.objects.SQL;
import io.dblosevn.xserver.bukkit.objects.TeleportLocation;
import io.dblosevn.xserver.bukkit.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawnCommand implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			Util.pluginMessage(sender, ChatColor.RED + "You may only run this command in game.");
			return true;
		}
		Player player = (Player) sender;
		if (args.length == 1) {
			SQL.addSpawn(args[0], new TeleportLocation(player.getLocation()));
			Util.pluginMessage(sender,
					CrossServerLib.primaryColor + "Successfully created the spawn for group " + CrossServerLib.secondaryColor + args[0]);
		} else {
			SQL.addSpawn("default", new TeleportLocation(player.getLocation()));
			Util.pluginMessage(sender, CrossServerLib.primaryColor + "Successfully created the spawn");
		}
		return true;
	}
}
