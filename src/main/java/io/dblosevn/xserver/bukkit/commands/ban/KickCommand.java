package io.dblosevn.xserver.bukkit.commands.ban;

import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.commands.TabCommand;
import io.dblosevn.xserver.bukkit.events.global.CrossServerModeratorEvent;
import io.dblosevn.xserver.bukkit.objects.ModAction;
import io.dblosevn.xserver.bukkit.sockets.lib.Server;
import io.dblosevn.xserver.bukkit.util.Util;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KickCommand extends TabCommand {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("crossserver.mod.kick")) {
			Util.pluginMessage(sender, "&4You do not have permission to do that.");
			return true;
		}
		if (args.length < 2) {
			Util.pluginMessage(sender, "&4player and kick message required.");
			return true;
		}
		String source = sender.getName();
		if (!(sender instanceof Player)) {
			source = "*console*";
		}
		String target = args[0];
		String message = "";
		int x = 1;
		while (x < args.length) {
			message += " " + args[x];
			++x;
		}
		message = message.trim();
		if (!CrossServerAPI.online(target)) {
			Util.pluginMessage(sender, "&4Player offline.");
			return true;
		}
		CrossServerModeratorEvent event = new CrossServerModeratorEvent(source, target, message,
				ModAction.KICK);
		Server.client.send(event.getPacket());
		return true;
	}
}
