package io.dblosevn.xserver.bukkit.commands.ban;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.commands.TabCommand;
import io.dblosevn.xserver.bukkit.events.global.CrossServerModeratorEvent;
import io.dblosevn.xserver.bukkit.objects.ModAction;
import io.dblosevn.xserver.bukkit.objects.ProxyPlayer;
import io.dblosevn.xserver.bukkit.objects.SQL;
import io.dblosevn.xserver.bukkit.sockets.lib.Server;
import io.dblosevn.xserver.bukkit.util.Util;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UnBanCommand extends TabCommand {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("crossserver.mod.kick")) {
			Util.pluginMessage(sender, "&4You do not have permission to do that.");
			return true;
		}
		if (args.length < 1) {
			Util.pluginMessage(sender, "&4player is required.");
			return true;
		}
		String source = sender.getName();
		if (!(sender instanceof Player)) {
			source = "*console*";
		}
		String target = args[0];
		String message = "";
		int x = 1;
		while (x < args.length) {
			message += " " + args[x];
			++x;
		}
		message = message.trim();
		boolean unbanned = SQL.unbanPlayer(new ProxyPlayer(target), new ProxyPlayer(source));	
		if (unbanned) {
			CrossServerModeratorEvent event = new CrossServerModeratorEvent(source, target, message,
					ModAction.UNBAN);
			Server.client.send(event.getPacket());
		} else {
			Util.pluginMessage(sender, CrossServerLib.secondaryColor + target + ChatColor.RED + " was not banned");
		}
		return true;
	}
}
