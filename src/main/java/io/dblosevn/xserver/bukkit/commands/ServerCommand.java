package io.dblosevn.xserver.bukkit.commands;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.events.global.CrossServerBackLocationEvent;
import io.dblosevn.xserver.bukkit.sockets.lib.Server;
import io.dblosevn.xserver.bukkit.util.Util;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class ServerCommand implements CommandExecutor, TabCompleter {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("crossserver.teleport.server")) {
			sender.sendMessage(ChatColor.RED + "You do not have permission to perform this command.");
			return true;
		}
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "You cannot run this command from console.");
			return true;
		}
		Player player = (Player) sender;
		try {
			if (args.length == 1) {
				ByteArrayOutputStream b = new ByteArrayOutputStream();
				DataOutputStream out = new DataOutputStream(b);
				Server.client.send(new CrossServerBackLocationEvent(player, args[0]).getPacket());
				out.writeUTF("Connect");
				out.writeUTF(args[0]);
				Util.pluginMessage(player, CrossServerLib.primaryColor + "Connecting to: " + CrossServerLib.secondaryColor + args[0]);
				player.sendPluginMessage((Plugin) CrossServerLib.get(), "BungeeCord", b.toByteArray());
			} else {
				Util.pluginMessage(player, CrossServerLib.secondaryColor + "Available Servers: " + CrossServerLib.primaryColor
						+ String.join((CharSequence) ", ", CrossServerAPI.getServers().toArray(new String[0])));
			}
		} catch (IOException b) {
			// empty catch block
		}
		return true;
	}

	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		ArrayList<String> opts = CrossServerAPI.getServers();
		ArrayList<String> out = new ArrayList<String>();
		if (args.length == 0) {
			return opts;
		}
		for (String s : opts) {
			if (!s.toLowerCase().startsWith(args[args.length - 1].toLowerCase()))
				continue;
			out.add(s);
		}
		return out;
	}
}
