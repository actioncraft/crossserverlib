package io.dblosevn.xserver.bukkit.commands;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import io.dblosevn.xserver.bukkit.chat.BaseChatChannel;
import io.dblosevn.xserver.bukkit.chat.ChatUtil;
import io.dblosevn.xserver.bukkit.commands.TabCommand;
import io.dblosevn.xserver.bukkit.events.global.CrossServerPMEvent;
import io.dblosevn.xserver.bukkit.sockets.lib.Server;
import io.dblosevn.xserver.bukkit.sockets.packets.ChatPacket;
import io.dblosevn.xserver.bukkit.util.Util;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PMChannelCommand extends TabCommand {
	public static HashMap<String, String> replyTo = new HashMap<String, String>();

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			Util.pluginMessage(sender, ChatColor.RED + "This command cannot be sent from console.");
			return true;
		}
		BaseChatChannel channel = ChatUtil.get("pm");
		String message = String.join((CharSequence) " ", args);
		Player player = (Player) sender;
		if (channel.getName() == "pm") {
			CrossServerPMEvent event;
			String r;
			String usage = String.format("Usage: %s %s<recipient> <message>",
					new Object[] { CrossServerLib.secondaryColor + label, ChatColor.RED });
			if (label.equals("r") || label.equals("reply")) {
				usage = String.format("Usage: %s <message>", new Object[] { CrossServerLib.secondaryColor + label, ChatColor.RED });
				if (!replyTo.containsKey(player.getName())) {
					Util.pluginMessage(sender, ChatColor.RED + "You dont have anyone to reply too.");
					return true;
				}
				r = replyTo.get(player.getName());
			} else {
				LinkedList<String> m = new LinkedList<String>(Arrays.asList(message.split(" ")));
				r = m.removeFirst();
				message = String.join((CharSequence) " ", m.toArray(new String[0]));
			}
			if (r.isEmpty()) {
				Util.pluginMessage(sender, ChatColor.RED + usage);
				return true;
			}
			if (CrossServerAPI.findPlayer(r) == null) {
				Util.pluginMessage(sender, ChatColor.RED + "Player Offline.");
				return true;
			}
			CrossServerPMEvent pmEvent = event = new CrossServerPMEvent(player, r, channel.getName(), message);
			String out = ChatColor.BLUE + "To " + pmEvent.getRecipient().getPrefix()
					+ pmEvent.getRecipient().getDisplayName() + ChatColor.WHITE + ": " + ChatColor.BLUE + message;
			player.sendMessage(ChatColor.translateAlternateColorCodes((char) '&', (String) out));
			ChatPacket packet = new ChatPacket(event);
			Server.client.send(packet);
		}
		return true;
	}
}
