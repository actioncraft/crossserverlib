package io.dblosevn.xserver.bukkit.commands;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class StopCommand implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, final String label, final String[] args) {
		System.out.println("StopCommand");
		if (!sender.hasPermission(command.getPermission())) {
			return true;
		}
		Bukkit.getScheduler().runTask((Plugin) CrossServerLib.get(), new Runnable() {

			@Override
			public void run() {
				String reason = ChatColor.RED
						+ (label.toLowerCase().endsWith("stop") ? "Server Stopped" : "Server Restarting");
				if (args.length != 0) {
					reason = String.valueOf(reason) + ChatColor.RESET + " - " + ChatColor
							.translateAlternateColorCodes((char) '&', (String) String.join((CharSequence) " ", args));
				}
				for (Player p : Bukkit.getOnlinePlayers()) {
					p.kickPlayer(reason);
				}
				Bukkit.getServer().shutdown();
			}
		});
		return true;
	}

}
