package io.dblosevn.xserver.bukkit.commands.spawns;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.objects.SQL;
import io.dblosevn.xserver.bukkit.objects.Spawn;
import io.dblosevn.xserver.bukkit.objects.TeleportLocation;
import io.dblosevn.xserver.bukkit.util.Util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

public class DelSpawnCommand implements CommandExecutor, TabCompleter {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 1) {
			SQL.removeSpawn(new TeleportLocation(CrossServerLib.get().getConfig().getString("serverName"), "world", 0, 0, 0, 0, 0), args[0]);
			Util.pluginMessage(sender,
					CrossServerLib.primaryColor + "Successfully deleted the spawn for group " + CrossServerLib.secondaryColor + args[0]);
		} else {
			Util.pluginMessage(sender, String.format(ChatColor.RED + "You must specify the group to delete, %s/spawns %sto list current spawns.", CrossServerLib.secondaryColor, ChatColor.RED));
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		LinkedList<Spawn> spawns = SQL.getSpawn(new TeleportLocation(CrossServerLib.get().getConfig().getString("serverName"), "world", 0, 0, 0, 0, 0));
		ArrayList<String> out = new ArrayList<String>();
		String search = (args.length == 0) ? "" : args[args.length -1].toLowerCase();
		for (Spawn s: spawns) {
			if (args.length > 0) {
				if (s.getGroup().toLowerCase().startsWith(search)) {
					out.add(s.getGroup());
				}
			} else {
				out.add(s.getGroup());
			}
		}
		return out;
	}
}
