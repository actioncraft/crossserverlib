package io.dblosevn.xserver.bukkit.commands;

import io.dblosevn.xserver.bukkit.api.CrossServerAPI;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

public abstract class TabCommand implements CommandExecutor, TabCompleter {
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		String token = "";
		ArrayList<String> list = new ArrayList<String>();
		if (args.length > 0) {
			token = args[args.length - 1];
		}
		for (String p : CrossServerAPI.getAllPlayerNames()) {
			if (!p.toLowerCase().startsWith(token) && !token.isEmpty())
				continue;
			list.add(CrossServerAPI.findPlayer(p).getDisplayName(true));
		}
		return list;
	}
}
