package io.dblosevn.xserver.bukkit.chat.channels;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.chat.BaseChatChannel;
import io.dblosevn.xserver.bukkit.events.global.CrossServerChatEvent;
import io.dblosevn.xserver.bukkit.util.Util;
import java.util.ArrayList;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class LocalChannel
extends BaseChatChannel {
    public LocalChannel() {
        super("local", "l", "crossserver.chat.channels.local", "&7(&bL&7) ", true);
    }

    @Override
    public boolean canSend(Player sender) {
        if (!this.hasPermission(sender)) {
            return false;
        }
        int radius = CrossServerLib.get().getConfig().getInt("localChatRadius");
        Entity[] arrentity = sender.getNearbyEntities((double)radius, (double)radius, (double)radius).toArray(new Entity[0]);
        int n = arrentity.length;
        int n2 = 0;
        while (n2 < n) {
            Entity e = arrentity[n2];
            if (e instanceof Player) {
                return true;
            }
            ++n2;
        }
        Util.pluginMessage(sender, "&4There are no nearby players.");
        return false;
    }

    @Override
    public ArrayList<Player> getRecipeients(CrossServerChatEvent e) {
        ArrayList<Player> players = new ArrayList<Player>();
        if (CrossServerLib.get().getConfig().getBoolean("chat.channels.local.useRaduis")) {
            int radius = CrossServerLib.get().getConfig().getInt("localChatRadius");
            Player sender = Bukkit.getPlayer((UUID)e.getSender().getUuid());
            Entity[] arrentity = sender.getNearbyEntities((double)radius, (double)radius, (double)radius).toArray(new Entity[0]);
            int n = arrentity.length;
            int n2 = 0;
            while (n2 < n) {
                Player p;
                Entity en = arrentity[n2];
                if (e instanceof Player && this.hasPermission((p = (Player)en))) {
                    players.add(p);
                }
                ++n2;
            }
            if (!players.isEmpty()) {
                players.add(sender);
            }
        } else {
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (!this.hasPermission(p)) continue;
                players.add(p);
            }
        }
        return players;
    }
}

