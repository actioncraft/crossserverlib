package io.dblosevn.xserver.bukkit.chat.channels;

import io.dblosevn.xserver.bukkit.chat.BaseChatChannel;
import io.dblosevn.xserver.bukkit.events.global.CrossServerChatEvent;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class StaffChannel extends BaseChatChannel {
	public StaffChannel() {
		super("staffchat", "s", "crossserver.chat.channels.staff", "&7(&9S&7/&b%w&7) ", false);
	}

	@Override
	public boolean canSend(Player sender) {
		return this.hasPermission(sender);
	}

	@Override
	public ArrayList<Player> getRecipeients(CrossServerChatEvent e) {
		ArrayList<Player> players = new ArrayList<Player>();
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (!this.hasPermission(p))
				continue;
			players.add(p);
		}
		return players;
	}
}
