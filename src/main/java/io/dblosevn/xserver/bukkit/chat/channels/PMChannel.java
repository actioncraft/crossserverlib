package io.dblosevn.xserver.bukkit.chat.channels;

import io.dblosevn.xserver.bukkit.chat.BaseChatChannel;
import io.dblosevn.xserver.bukkit.events.global.CrossServerChatEvent;
import io.dblosevn.xserver.bukkit.events.global.CrossServerPMEvent;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class PMChannel extends BaseChatChannel {
	public PMChannel() {
		super("pm", "msg", "crossserver.chatchannels.pm", "&9From ", false);
	}

	@Override
	public boolean canSend(Player sender) {
		return this.hasPermission(sender);
	}

	@Override
	public ArrayList<Player> getRecipeients(CrossServerChatEvent e) {
		CrossServerPMEvent e2 = (CrossServerPMEvent) e;
		ArrayList<Player> players = new ArrayList<Player>();
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (!e2.getRecipient().getUuid().equals(p.getUniqueId()))
				continue;
			players.add(p);
		}
		return players;
	}

	@Override
	public String buildMessage(CrossServerChatEvent e) {
		String prefix = e.getSender().getPrefix();
		String out = String.valueOf(this.getPrefix()) + prefix + e.getSender().getDisplayName() + ChatColor.WHITE + ": "
				+ ChatColor.BLUE + e.getMessage();
		return ChatColor.translateAlternateColorCodes((char) '&', (String) out);
	}
}
