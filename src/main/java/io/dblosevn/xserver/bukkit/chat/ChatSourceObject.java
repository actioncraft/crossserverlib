package io.dblosevn.xserver.bukkit.chat;

import com.google.gson.JsonObject;
import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.chat.BaseChatChannel;
import io.dblosevn.xserver.bukkit.chat.ChatUtil;
import io.dblosevn.xserver.bukkit.objects.ProxyPlayer;
import io.dblosevn.xserver.bukkit.util.PermUtil;
import me.lucko.luckperms.api.caching.MetaData;
import net.md_5.bungee.api.ChatColor;

import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class ChatSourceObject {
	private BaseChatChannel channel;
	private String server;
	private UUID uuid;
	private String playerName;
	private String prefix;
	private String suffix;
	private boolean colorMessages = false;
	private ProxyPlayer pPlayer = null;
	public ChatSourceObject(Player p, String channel) {
		this.setUuid(p.getUniqueId());
		this.setServer(CrossServerLib.get().getConfig().getString("serverName", "Lobby"));
		this.setPlayerName(p.getName());
		pPlayer = new ProxyPlayer(playerName, uuid.toString(), server);
		MetaData meta = PermUtil.getMetaDataForServer(PermUtil.getUser(uuid), server);
		this.setPrefix(meta.getPrefix());
		this.setSuffix(meta.getSuffix());
		if (p.hasPermission("crossserver.chat.color")) {
			this.setColorMessages(true);
		}
		
		this.setChannel(channel);
	}

	public ChatSourceObject(CommandSender p, String channel) {
		this.setServer(CrossServerLib.get().getConfig().getString("serverName", "Lobby"));
		this.setPlayerName(p.getName());
		if (p instanceof ConsoleCommandSender) {
			pPlayer = new ProxyPlayer("*console*");
			this.setPlayerName(pPlayer.getName());
			this.setPrefix(ChatColor.AQUA.toString());
			this.setSuffix(ChatColor.RESET.toString());
			this.setUuid(pPlayer.getUuid());
			this.setColorMessages(true);
		} else {
			Player player = (Player) p;
			this.setUuid(player.getUniqueId());
			pPlayer = new ProxyPlayer(playerName);
			MetaData meta = PermUtil.getMetaDataForServer(PermUtil.getUser(uuid), server);
			this.setPrefix(meta.getPrefix());
			this.setSuffix(meta.getSuffix());
			if (p.hasPermission("crossserver.chat.color")) {
				this.setColorMessages(true);
			}
		}
		this.setChannel(channel);
	}

	public ChatSourceObject(String player, String channel) {
		this.setServer(CrossServerLib.get().getConfig().getString("serverName", "Lobby"));
		pPlayer = new ProxyPlayer(player);
		this.setPlayerName(pPlayer.getName());
		this.setUuid(pPlayer.getUuid());
		MetaData meta = PermUtil.getMetaDataForServer(PermUtil.getUser(uuid), server);
		this.setPrefix(meta.getPrefix());
		this.setSuffix(meta.getSuffix());
		this.setChannel(channel);
	}

	public ChatSourceObject(JsonObject o) {
		this.setPlayerName(o.get("p").getAsString());
		this.setUuid(UUID.fromString(o.get("u").getAsString()));
		pPlayer = new ProxyPlayer(playerName);
		playerName = pPlayer.getName();
		this.setServer(o.get("s").getAsString());
		this.setPrefix(o.get("pf").getAsString());
		this.setSuffix(o.get("sf").getAsString());
		this.setColorMessages(o.get("cm").getAsBoolean());
		this.setChannel(o.get("c").getAsString());
	}

	public static ChatSourceObject deserialize(JsonObject o) {
		return new ChatSourceObject(o);
	}

	public JsonObject serialize() {
		JsonObject o = new JsonObject();
		o.addProperty("u", this.uuid.toString());
		o.addProperty("s", this.server);
		o.addProperty("p", this.playerName);
		o.addProperty("pf", this.prefix);
		o.addProperty("sf", this.suffix);
		o.addProperty("cm", Boolean.valueOf(this.colorMessages));
		o.addProperty("c", this.channel.getName());
		return o;
	}

	public BaseChatChannel getChannel() {
		return this.channel;
	}

	public OfflinePlayer getPlayer() {
		return Bukkit.getOfflinePlayer((UUID) this.uuid);
	}

	public void setChannel(String channel) {
		this.channel = ChatUtil.get(channel);
	}

	public void setChannel(BaseChatChannel channel) {
		this.channel = channel;
	}

	public String getServer() {
		return this.server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public UUID getUuid() {
		return this.uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getDisplayName() {
		return (null == pPlayer) ? playerName : pPlayer.getDisplayName();
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return this.suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public boolean isColorMessages() {
		return this.colorMessages;
	}

	public void setColorMessages(boolean colorMessages) {
		this.colorMessages = colorMessages;
	}
}
