package io.dblosevn.xserver.bukkit.chat;

import io.dblosevn.xserver.bukkit.chat.ChatSourceObject;
import io.dblosevn.xserver.bukkit.events.global.CrossServerChatEvent;
import io.dblosevn.xserver.bukkit.events.global.LocalChatWrapperEvent;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public abstract class BaseChatChannel {
	private String name;
	private String alias;
	private String permission;
	private String prefix;
	private boolean isLocal = false;

	public abstract boolean canSend(Player var1);

	public abstract ArrayList<Player> getRecipeients(CrossServerChatEvent var1);

	public BaseChatChannel(String name, String alias, String permission, String prefix, boolean isLocal) {
		this.name = name;
		this.alias = alias;
		this.permission = permission;
		this.prefix = prefix;
		this.isLocal = isLocal;
	}

	public void sendMessage(CrossServerChatEvent e) {
		String output = this.buildMessage(e);
		if (e.getSender().getPlayerName().equals("*console*") || e.getSender().getPlayer().isOnline()) {
			String message = e.getMessage();
			if (!e.getSender().getChannel().isLocal()) {
				message = String.valueOf(ChatColor.stripColor((String) ChatColor.translateAlternateColorCodes(
						(char) '&',
						(String) e.getSender().getChannel().getPrefix().replaceAll("%w", e.getSender().getServer()))))
						+ message;
			}
			if (!e.getSender().getChannel().getName().equals("broadcast")
					&& !e.getSender().getChannel().getName().equals("pm")) {
				AsyncPlayerChatEvent event = new LocalChatWrapperEvent(false, e.getSender().getPlayer().getPlayer(),
						message, this.getRecipeients(e));
				Bukkit.getServer().getPluginManager().callEvent(event);
			}
		}
		if (e.isCancelled()) {
			return;
		}
		for (Player p : this.getRecipeients(e)) {
			if (output == null)
				continue;
			p.sendMessage(output);
		}
	}

	public String getName() {
		return this.name;
	}

	public String getAlias() {
		return this.alias;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public boolean hasPermission(CommandSender p) {
		if (p.hasPermission(this.permission)) {
			return true;
		}
		return false;
	}

	public String buildMessage(CrossServerChatEvent e) {
		ChatSourceObject source = e.getSender();
		String server = source.getServer();
		server = source.getChannel().getPrefix().replaceAll("%w", server);
		server = String.valueOf(server) + source.getPrefix() + source.getDisplayName() + ChatColor.RESET + ": "
				+ source.getSuffix();
		return ChatColor.translateAlternateColorCodes((char) '&', (String) (String.valueOf(server) + e.getMessage()));
	}

	public boolean isLocal() {
		return this.isLocal;
	}
}
