package io.dblosevn.xserver.bukkit.chat.channels;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.chat.BaseChatChannel;
import io.dblosevn.xserver.bukkit.events.global.CrossServerChatEvent;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class BroadcastChannel extends BaseChatChannel {
	public BroadcastChannel() {
		super("broadcast", "bc", "crossserver.chat.global", "", false);
	}

	@Override
	public boolean canSend(Player sender) {
		return this.hasPermission(sender);
	}

	@Override
	public ArrayList<Player> getRecipeients(CrossServerChatEvent e) {
		ArrayList<Player> players = new ArrayList<Player>();
		for (Player p : Bukkit.getOnlinePlayers()) {
			players.add(p);
		}
		return players;
	}

	@Override
	public String buildMessage(CrossServerChatEvent e) {
		String message = ChatColor.translateAlternateColorCodes((char) '&', (String) e.getMessage());
		String prefix = CrossServerLib.get().getConfig().getString("chat.channels.broadcast.broadcastTag");
		prefix = ChatColor.translateAlternateColorCodes((char) '&', (String) prefix);
		Bukkit.getServer().getConsoleSender().sendMessage(String.valueOf(prefix) + message);
		return String.valueOf(prefix) + message;
	}
}
