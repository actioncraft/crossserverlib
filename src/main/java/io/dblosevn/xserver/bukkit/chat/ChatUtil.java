package io.dblosevn.xserver.bukkit.chat;

import io.dblosevn.xserver.bukkit.CrossServerLib;
import io.dblosevn.xserver.bukkit.chat.BaseChatChannel;
import io.dblosevn.xserver.bukkit.chat.channels.AdminChannel;
import io.dblosevn.xserver.bukkit.chat.channels.BroadcastChannel;
import io.dblosevn.xserver.bukkit.chat.channels.GlobalChannel;
import io.dblosevn.xserver.bukkit.chat.channels.LocalChannel;
import io.dblosevn.xserver.bukkit.chat.channels.PMChannel;
import io.dblosevn.xserver.bukkit.chat.channels.StaffChannel;
import java.util.Collection;
import java.util.HashMap;
import org.bukkit.configuration.ConfigurationSection;

public class ChatUtil {
	private static HashMap<String, BaseChatChannel> channels = new HashMap<String, BaseChatChannel>();

	static {
		ConfigurationSection config = CrossServerLib.get().getConfig().getConfigurationSection("chat.channels");
		if (config.getBoolean("local.enabled")) {
			channels.put("local", new LocalChannel());
		}
		if (config.getBoolean("staff.enabled")) {
			channels.put("staffchat", new StaffChannel());
		}
		if (config.getBoolean("admin.enabled")) {
			channels.put("adminchat", new AdminChannel());
		}
		if (config.getBoolean("global.enabled")) {
			channels.put("global", new GlobalChannel());
		}
		if (config.getBoolean("broadcast.enabled")) {
			channels.put("broadcast", new BroadcastChannel());
		}
		if (config.getBoolean("pm.enabled")) {
			channels.put("pm", new PMChannel());
		}
	}

	public static BaseChatChannel get(String name) {
		return channels.get(name);
	}

	public static Collection<BaseChatChannel> getChannels() {
		return channels.values();
	}

	public static BaseChatChannel lookup(String command) {
		for (BaseChatChannel c : channels.values()) {
			if (command.toLowerCase().startsWith("/" + c.getName().toLowerCase() + " ")) {
				return c;
			}
			if (!command.toLowerCase().startsWith("/" + c.getAlias().toLowerCase() + " "))
				continue;
			return c;
		}
		return null;
	}
}
