package io.dblosevn.xserver.bungee.listeners;


import io.dblosevn.xserver.bukkit.objects.BanInfo;
import io.dblosevn.xserver.bungee.CrossServerLib;
import io.dblosevn.xserver.bungee.objects.SQL;
import io.dblosevn.xserver.bungee.util.Util;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import static io.dblosevn.xserver.bungee.CrossServerLib.secondaryColor;

import java.util.logging.Logger;

public class PlayerListener
implements Listener {
	Logger logger;
	public PlayerListener() {
		logger = CrossServerLib.get().getLogger();
	}	
	@EventHandler
	public void shouldKick(LoginEvent e) {
		SQL.clearBans();
    	if (SQL.isBanned(e.getConnection().getUniqueId())) {
    		BanInfo ban = SQL.getBanInfo(e.getConnection().getUniqueId());
    		switch (ban.type) {
	    		case PERM:
	    			e.setCancelled(true);
	    			e.setCancelReason(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes((char) '&',
							ChatColor.RED + " You have been banned from the server by "
									+  secondaryColor + ban.staff +  ChatColor.RED + " for "
									+ secondaryColor + ban.reason)));
	    		break;
	    		case TEMP:
	    			e.setCancelled(true);
					e.setCancelReason(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes((char) '&',
							ChatColor.RED + " You have been temporarily banned from the server by "
									+  secondaryColor + ban.staff + ChatColor.RED + "\n until " + secondaryColor + Util.timeToString(ban.remaining * 1000) + ChatColor.RED + " for " + secondaryColor + ban.reason
					)));
	    			
	    		break;
	    		default: break;
    		}
    	}
	}
    @EventHandler
    public void onPlayerLogin(PostLoginEvent e) {
    	if (!SQL.isBanned(e.getPlayer().getUniqueId())) {
    		SQL.playerLogin(e.getPlayer().getUniqueId(), e.getPlayer().getName(), e.getPlayer().getAddress().getAddress().getHostAddress());
    	} else {
    		e.getPlayer().disconnect(TextComponent.fromLegacyText("Banned"));
    	}
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent e) {
        SQL.playerDisconnect(e.getPlayer().getUniqueId(), e.getPlayer().getName());
    }

    @EventHandler
    public void onPlayerSwitchServer(ServerSwitchEvent e) {
        SQL.playerServer(e.getPlayer().getUniqueId(), e.getPlayer().getServer().getInfo().getName());
    }
}