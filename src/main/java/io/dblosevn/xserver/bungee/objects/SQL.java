package io.dblosevn.xserver.bungee.objects;

import io.dblosevn.xserver.bukkit.api.DB;
import io.dblosevn.xserver.bukkit.api.DBSchema;
import io.dblosevn.xserver.bukkit.objects.BanInfo;
import io.dblosevn.xserver.bukkit.objects.PlayerInfo;
import io.dblosevn.xserver.bungee.CrossServerLib;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import net.md_5.bungee.api.plugin.Plugin;

public class SQL {
	public static void playerLogin(UUID uuid, String name, String ip) {
		String set = "player_uuid_upper=?, player_uuid_lower=?, player_uuid=?, player_name=?, player_last_login=?, player_last_ip=?";
		String sql = "Insert Into " + (DBSchema.Tables.PLAYERS) + " Set player_join_date=?, " + set
				+ " On Duplicate Key Update " + set;
		Connection connection = DB.connect();
		PreparedStatement ps = null;
		long timeNow = System.currentTimeMillis();
		try {
			try {
				ps = connection.prepareStatement(sql);
				ps.setLong(1, timeNow);
				ps.setLong(2, uuid.getMostSignificantBits());
				ps.setLong(3, uuid.getLeastSignificantBits());
				ps.setString(4, uuid.toString());
				ps.setString(5, name);
				ps.setLong(6, timeNow);
				ps.setString(7, ip);
				ps.setLong(8, uuid.getMostSignificantBits());
				ps.setLong(9, uuid.getLeastSignificantBits());
				ps.setString(10, uuid.toString());
				ps.setString(11, name);
				ps.setLong(12, timeNow);
				ps.setString(13, ip);
				ps.execute();
				SQL.addLogin(ip, name, timeNow, uuid);
			} catch (SQLException e) {
				e.printStackTrace();
				DB.close(null, ps, connection);
			}
		} finally {
			DB.close(null, ps, connection);
		}
	}

	public static boolean isBanned(UUID player) {
		Connection connection;
		ResultSet rs;
		PreparedStatement ps;
		String sql = "Select 1 From " + (DBSchema.Tables.BANS) + " Join " + (DBSchema.Tables.PLAYERS)
				+ " on player_id = ban_player_id Where player_uuid_lower=? and player_uuid_upper=? and ban_status='ACTIVE'";
		connection = DB.connect();
		ps = null;
		rs = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, player.getLeastSignificantBits());
			ps.setLong(2, player.getMostSignificantBits());
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.close(rs, ps, connection);
		}
		DB.close(rs, ps, connection);
		return false;
	}

	protected static void addLogin(final String ip, final String name, final long timeNow, final UUID uuid) {
		CrossServerLib.get().getProxy().getScheduler().runAsync((Plugin) CrossServerLib.get(), new Runnable() {

			@Override
			public void run() {
				PreparedStatement ps;
				Connection connection;
				connection = DB.connect();
				ps = null;
				try {
					try {
						PlayerInfo rs = SQL.getPlayerInfo(uuid);
						int player_id = rs.player_id;
						String sql = "Insert Into " + (DBSchema.Tables.LOGINS)
								+ " Set player_id=?, player_join_time=?, player_ip=?, player_name=?";
						ps = connection.prepareStatement(sql);
						ps.setInt(1, player_id);
						ps.setLong(2, timeNow);
						ps.setString(3, ip);
						ps.setString(4, name);
						ps.execute();
					} catch (SQLException e) {
						e.printStackTrace();
						DB.close(null, ps, connection);
					}
				} catch (Throwable throwable) {
				}
				DB.close(null, ps, connection);
			}
		});
	}

	public static void playerDisconnect(final UUID uuid, String name) {
		CrossServerLib.get().getProxy().getScheduler().runAsync((Plugin) CrossServerLib.get(), new Runnable() {

			@Override
			public void run() {
				block5: {
					Connection connection;
					PreparedStatement ps;
					String sql = "Update " + (DBSchema.Tables.PLAYERS)
							+ " Set player_last_logout=?, player_time=player_time + (? - player_last_login) Where player_uuid_upper=? And player_uuid_lower=?";
					connection = DB.connect();
					ps = null;
					try {
						try {
							long timeNow = System.currentTimeMillis();
							ps = connection.prepareStatement(sql);
							ps.setLong(1, timeNow);
							ps.setLong(2, timeNow);
							ps.setLong(3, uuid.getMostSignificantBits());
							ps.setLong(4, uuid.getLeastSignificantBits());
							ps.execute();
						} catch (SQLException e) {
							e.printStackTrace();
							DB.close(null, ps, connection);
							break block5;
						}
					} catch (Throwable throwable) {
						DB.close(null, ps, connection);
						throw throwable;
					}
					DB.close(null, ps, connection);
				}
			}
		});
	}

	public static void playerServer(final UUID uuid, final String server) {
		CrossServerLib.get().getProxy().getScheduler().runAsync((Plugin) CrossServerLib.get(), new Runnable() {

			@Override
			public void run() {
				String sql = "Update " + (DBSchema.Tables.PLAYERS)
						+ " Set player_last_server=? Where player_uuid_upper=? And player_uuid_lower=?";
				Connection connection = DB.connect();
				PreparedStatement ps = null;
				try {
					try {
						ps = connection.prepareStatement(sql);
						ps.setString(1, server);
						ps.setLong(2, uuid.getMostSignificantBits());
						ps.setLong(3, uuid.getLeastSignificantBits());
						ps.execute();
					} catch (SQLException e) {
						e.printStackTrace();
						DB.close(null, ps, connection);
					}
				} finally {
					DB.close(null, ps, connection);
				}
			}
		});
	}

	public static void playerRank(final UUID uuid, final String rank) {
		CrossServerLib.get().getProxy().getScheduler().runAsync((Plugin) CrossServerLib.get(), new Runnable() {

			@Override
			public void run() {
				String sql = "Update " + (DBSchema.Tables.PLAYERS)
						+ " Set player_rank=? Where player_uuid_upper=? And player_uuid_lower=?";
				Connection connection = DB.connect();
				PreparedStatement ps = null;
				try {
					try {
						ps = connection.prepareStatement(sql);
						ps.setString(1, rank);
						ps.setLong(2, uuid.getMostSignificantBits());
						ps.setLong(3, uuid.getLeastSignificantBits());
						ps.execute();
					} catch (SQLException e) {
						e.printStackTrace();
						DB.close(null, ps, connection);
					}
				} finally {
					DB.close(null, ps, connection);
				}
			}
		});
	}

	public static PlayerInfo getPlayerInfo(UUID uuid) {
		Connection connection;
		ResultSet rs;
		PreparedStatement ps;
		String sql = "Select * From " + (DBSchema.Tables.PLAYERS)
				+ " Where player_uuid_upper=? And player_uuid_lower=?";
		connection = DB.connect();
		ps = null;
		rs = null;
		PlayerInfo out = new PlayerInfo();
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, uuid.getMostSignificantBits());
			ps.setLong(2, uuid.getLeastSignificantBits());
			rs = ps.executeQuery();
			if (rs.next()) {
				out.player_id = rs.getInt("player_id");
				out.player_join_date = rs.getLong("player_join_date");
				out.player_last_ip = rs.getString("player_last_ip");
				out.player_last_login = rs.getLong("player_last_login");
				out.player_last_logout = rs.getLong("player_last_logout");
				out.player_last_server = rs.getString("player_last_server");
				out.player_name = rs.getString("player_name");
				out.player_rank = rs.getString("player_rank");
				out.player_uuid = rs.getString("player_uuid");
				out.player_uuid_lower = rs.getLong("player_uuid_lower");
				out.player_uuid_upper = rs.getLong("player_uuid_upper");
				out.player_time = rs.getLong("player_time");
				PlayerInfo playerInfo = out;
				DB.close(rs, ps, connection);
				return playerInfo;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.close(rs, ps, connection);
		}
		DB.close(rs, ps, connection);
		return null;
	}
	public static void clearBans() {
		String sql = "Update " + (DBSchema.Tables.BANS)
				+ " Set ban_status='EXPIRED' Where ban_type='TEMP' and ((UNIX_TIMESTAMP(NOW()) - (ban_time + ban_length)) * -1) <= 0";
		Connection connection = DB.connect();
		PreparedStatement ps = null;
		try {
			try {
				ps = connection.prepareStatement(sql);
				ps.execute();
			} catch (SQLException e) {
				e.printStackTrace();
				DB.close(null, ps, connection);
			}
		} finally {
			DB.close(null, ps, connection);
		}
		
	}
	public static BanInfo getBanInfo(UUID uuid) {
		Connection connection;
		ResultSet rs;
		PreparedStatement ps;
		String sql = "select sp.`player_name` `staff_name`, bp.`player_name`, (UNIX_TIMESTAMP(NOW()) - (ban_time + ban_length)) * -1 `remaining`, b.* From " + (DBSchema.Tables.BANS) + " b join " + (DBSchema.Tables.PLAYERS) + " bp on b.ban_player_id = bp.`player_id` join " + (DBSchema.Tables.PLAYERS) + " sp on b.ban_staff_id = sp.`player_id`"
				+ " Where bp.player_uuid_lower=? and bp.player_uuid_upper=? and b.ban_status='ACTIVE'";
		
		connection = DB.connect();
		ps = null;
		rs = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, uuid.getLeastSignificantBits());
			ps.setLong(2, uuid.getMostSignificantBits());
			rs = ps.executeQuery();
			if (rs.next()) {
				return new BanInfo(rs);
			} else {
				System.out.println("No ban record found");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.close(rs, ps, connection);
		}
		DB.close(rs, ps, connection);
		return null;
	}

	public static PlayerInfo getPlayerInfo(String playerName) {
		Connection connection;
		ResultSet rs;
		PreparedStatement ps;
		String sql = "Select * From " + (DBSchema.Tables.PLAYERS) + " Where player_name=?";
		connection = DB.connect();
		ps = null;
		rs = null;
		PlayerInfo out = new PlayerInfo();
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, playerName);
			rs = ps.executeQuery();
			if (rs.next()) {
				out.player_id = rs.getInt("player_id");
				out.player_join_date = rs.getLong("player_join_date");
				out.player_last_ip = rs.getString("player_last_ip");
				out.player_last_login = rs.getLong("player_last_login");
				out.player_last_logout = rs.getLong("player_last_logout");
				out.player_last_server = rs.getString("player_last_server");
				out.player_name = rs.getString("player_name");
				out.player_rank = rs.getString("player_rank");
				out.player_uuid = rs.getString("player_uuid");
				out.player_uuid_lower = rs.getLong("player_uuid_lower");
				out.player_uuid_upper = rs.getLong("player_uuid_upper");
				out.player_time = rs.getLong("player_time");
				PlayerInfo playerInfo = out;
				DB.close(rs, ps, connection);
				return playerInfo;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.close(rs, ps, connection);
		}
		DB.close(rs, ps, connection);
		return null;
	}

}
