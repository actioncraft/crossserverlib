package io.dblosevn.xserver.bungee.sockets;

import io.dblosevn.xserver.bungee.sockets.Packet;

public interface ProxyTcpClientEventHandler {
    public void onMessage(Packet var1);

    public void onOpen();

    public void onClose();
}

