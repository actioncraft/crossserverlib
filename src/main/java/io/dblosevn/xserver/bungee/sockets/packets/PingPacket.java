package io.dblosevn.xserver.bungee.sockets.packets;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import io.dblosevn.xserver.bungee.events.global.CrossServerEvent;
import io.dblosevn.xserver.bungee.sockets.Packet;

public class PingPacket
extends Packet {
    public PingPacket() {
    }

    public PingPacket(CrossServerEvent event) {
        super("Proxy Ping Packet", 7, event);
    }

    @Override
    protected void buildPacketHeaders(ByteArrayDataOutput out) {
    }

    @Override
    protected void buildPacketData(ByteArrayDataOutput out) {
    }

    @Override
    protected void deSerializeHeaders(ByteArrayDataInput in) {
    }

    @Override
    protected void deSerializeData(ByteArrayDataInput in) {
        this.packetId = 7;
    }
}

