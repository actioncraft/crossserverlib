package io.dblosevn.xserver.bungee.sockets;

import io.dblosevn.xserver.bukkit.util.NetworkUtil;
import io.dblosevn.xserver.bungee.CrossServerLib;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;

import javax.xml.bind.DatatypeConverter;

public class ProxyTcpClient {
    private BufferedReader bReader;
    private BufferedWriter bWriter;
    private boolean closer = false;
    private ProxyTcpClientEventHandler handler;
    private String host;
    private InputStreamReader iReader;
    private int port;
    private int readInterval = 0;
    private Socket sock;

    public int getReadInterval() {
        return this.readInterval;
    }

    public void setReadInterval(int msec) {
        this.readInterval = msec;
    }

    public ProxyTcpClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public ProxyTcpClient(Socket connected_socket) {
        this.sock = connected_socket;
    }

    public boolean run() {
        this.closer = false;
        try {
            this.bWriter = new BufferedWriter(new OutputStreamWriter(this.sock.getOutputStream()));
            this.iReader = new InputStreamReader(this.sock.getInputStream());
            this.bReader = new BufferedReader(this.iReader);
            final ProxyTcpClient that = this;
            CrossServerLib.get().getProxy().getScheduler().runAsync(CrossServerLib.get(), new Runnable() {
                public void run() {
                    while (!ProxyTcpClient.this.closer && !Thread.interrupted()) {
                        try {
                            Thread.sleep((long) ProxyTcpClient.this.readInterval);
                            String line = ProxyTcpClient.this.bReader.readLine();
                            if (line == null) {
                                that.close();
                                return;
                            } else if (line.length() > 0) {
                            	
                                Packet packet = PacketRegistry.deserialize(DatatypeConverter.parseHexBinary(line.substring(8)));
                                if (!(packet == null || ProxyTcpClient.this.handler == null)) {
                                    ProxyTcpClient.this.handler.onMessage(packet);
                                }
                            }
                        } catch (SocketException e) {
                            that.close();
                        } catch (IOException e2) {
                            that.close();
                        } catch (Exception e3) {
                            that.close();
                        }
                    }
                }
            });
            if (this.handler != null) {
                this.handler.onOpen();
            }
            return true;
        } catch (ConnectException ex) {
            ex.printStackTrace();
            if (this.handler == null) {
                return false;
            }
            this.handler.onClose();
            return false;
        } catch (Exception ex2) {
            ex2.printStackTrace();
            close();
            if (this.handler == null) {
                return false;
            }
            this.handler.onClose();
            return false;
        }
    }

    public boolean connect() {
        if (this.sock != null) {
            return false;
        }
        try {
            this.sock = new Socket(this.host, this.port);
            return run();
        } catch (ConnectException e) {
            if (this.handler == null) {
                return false;
            }
            this.handler.onClose();
            return false;
        } catch (Exception e2) {
            close();
            if (this.handler == null) {
                return false;
            }
            this.handler.onClose();
            return false;
        }
    }

    public void close() {
        try {
            this.closer = true;
            this.sock.close();
            this.sock = null;
            if (this.handler != null) {
                this.handler.onClose();
            }
        } catch (Exception e) {
        }
    }

    public boolean send(Packet line) {
        if (this.sock == null) {
            return false;
        }
        try {
            this.bWriter.write(new StringBuilder(String.valueOf(NetworkUtil.bytesToHex(line.getBytes()))).append("\r\n").toString());
            this.bWriter.flush();
            return true;
        } catch (Exception e) {
            close();
            if (this.handler == null) {
                return false;
            }
            this.handler.onClose();
            return false;
        }
    }

    public void addEventHandler(ProxyTcpClientEventHandler handler) {
        this.handler = handler;
    }
}
