package io.dblosevn.xserver.bungee.sockets;

import io.dblosevn.xserver.bungee.CrossServerLib;
import io.dblosevn.xserver.bungee.sockets.Packet;
import io.dblosevn.xserver.bungee.sockets.ProxyTcpClient;
import io.dblosevn.xserver.bungee.sockets.ProxyTcpClientEventHandler;
import net.md_5.bungee.api.plugin.Plugin;

public abstract class ProxyClient {
    boolean shutdown = false;
    final ProxyTcpClient that_sock;
    boolean reconnecting = false;
    boolean connected = false;
    long timeout = 3000L;

    public ProxyClient(String host, int port, long timeout) {
        this(host, port);
        this.timeout = timeout;
    }

    public ProxyClient(String host, int port) {
        ProxyTcpClient sock;
        this.that_sock = sock = new ProxyTcpClient(host, port);
        final ProxyTcpClientEventHandler handler = new ProxyTcpClientEventHandler(){

            @Override
            public void onMessage(Packet packet) {
                ProxyClient.this.onMessage(packet);
            }

            @Override
            public void onOpen() {
                ProxyClient.this.connected = true;
                ProxyClient.this.onOpen();
                System.out.println("* Connection Estabished.");
                ProxyClient.this.reconnecting = false;
            }

            @Override
            public void onClose() {
                ProxyClient.this.connected = false;
                System.out.println("* Connection Lost.");
                ProxyClient.this.onClose();
                if (!ProxyClient.this.shutdown) {
                    try {
                        Thread.sleep(ProxyClient.this.timeout);
                    }
                    catch (Exception exception) {
                        // empty catch block
                    }
                    System.out.println("* Reconnecting..");
                    ProxyClient.this.reconnecting = true;
                    ProxyClient.this.that_sock.connect();
                }
            }
        };
        CrossServerLib.get().getProxy().getScheduler().runAsync((Plugin)CrossServerLib.get(), new Runnable(){

            @Override
            public void run() {
                sock.addEventHandler(handler);
                sock.connect();
            }
        });
    }

    protected abstract void onMessage(Packet var1);

    protected abstract void onOpen();

    protected abstract void onClose();

    protected abstract void onSend(Packet var1);

    public void shutdown() {
        this.shutdown = true;
        this.that_sock.close();
    }

    public boolean isConnected() {
        return this.connected;
    }

    public void send(Packet packet) {
        this.onSend(packet);
        this.that_sock.send(packet);
    }

}

