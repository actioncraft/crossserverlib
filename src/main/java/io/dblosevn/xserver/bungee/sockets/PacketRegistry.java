package io.dblosevn.xserver.bungee.sockets;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import io.dblosevn.xserver.bungee.sockets.Packet;
import io.dblosevn.xserver.bungee.sockets.PacketType;

public class PacketRegistry {
    public static Packet deserialize(byte[] data) {
        ByteArrayDataInput in = ByteStreams.newDataInput(data);
        int myId = in.readInt();
        PacketType type = PacketType.getById(myId);
        if (type.equals(PacketType.UNKNOWN)) {
            return null;
        }
        Class<? extends Packet> clazz = type.getClazz();
        try {
            Packet packet = clazz.newInstance();
            packet.deserialize(in);
            return packet;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

