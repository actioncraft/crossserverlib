package io.dblosevn.xserver.bungee.sockets;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.google.gson.JsonParser;
import io.dblosevn.xserver.bungee.events.global.CrossServerEvent;
import io.dblosevn.xserver.bungee.sockets.PacketType;

public abstract class Packet {
    private ByteArrayDataOutput bb = ByteStreams.newDataOutput();
    private String packetName = "";
    protected int packetId;
    private CrossServerEvent event;

    public Packet() {
    }

    public Packet(String packetName, int packetId, CrossServerEvent event) {
        this.packetName = packetName;
        this.packetId = packetId;
        this.event = event;
    }

    public ByteArrayDataOutput getBuffer() {
        return this.bb;
    }

    public String getPacketName() {
        return this.packetName;
    }

    public int getPacketId() {
        return this.packetId;
    }

    protected abstract void buildPacketHeaders(ByteArrayDataOutput var1);

    protected abstract void buildPacketData(ByteArrayDataOutput var1);

    protected abstract void deSerializeHeaders(ByteArrayDataInput var1);

    protected abstract void deSerializeData(ByteArrayDataInput var1);

    public void deserialize(ByteArrayDataInput in) {
        String eventClass = in.readUTF();
        this.deSerializeHeaders(in);
        try {
            Class<?> clazz = Class.forName(eventClass.replace("bukkit", "bungee"));
            try {
                CrossServerEvent e = (CrossServerEvent)(clazz.newInstance());
                String data = in.readUTF();
                JsonParser p = new JsonParser();
                e.deSerialize(p.parse(data).getAsJsonObject());
                this.event = e;
            }
            catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        this.deSerializeData(in);
    }

    public byte[] getBytes() {
        this.bb = ByteStreams.newDataOutput();
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeInt(this.packetId);
        out.writeUTF(this.event.getClass().getCanonicalName());
        this.buildPacketHeaders(out);
        out.writeUTF(this.event.serialize().toString());
        this.buildPacketData(out);
        this.bb.writeInt(out.toByteArray().length);
        this.bb.write(out.toByteArray());
        return this.bb.toByteArray();
    }

    public CrossServerEvent getEvent() {
        return this.event;
    }

    public PacketType getType() {
        return PacketType.getById(this.packetId);
    }
}

