package io.dblosevn.xserver.bungee.sockets;

import io.dblosevn.xserver.bungee.sockets.Packet;
import io.dblosevn.xserver.bungee.sockets.packets.PingPacket;
import io.dblosevn.xserver.bungee.sockets.packets.ProxyStatusPacket;

public enum PacketType {
    UNKNOWN(Packet.class, 0),
    PROXY_STATUS(ProxyStatusPacket.class, 5),
    PING(PingPacket.class, 7);
    
    private final Class<? extends Packet> clazz;
    private final int id;

    private PacketType(Class<? extends Packet> clazz, int id) {
        this.clazz = clazz;
        this.id = id;
    }

    public Class<? extends Packet> getClazz() {
        return this.clazz;
    }

    public static PacketType getById(int id) {
        PacketType[] arrpacketType = PacketType.values();
        int n = arrpacketType.length;
        int n2 = 0;
        while (n2 < n) {
            PacketType t = arrpacketType[n2];
            if (t.getId() == id) {
                return t;
            }
            ++n2;
        }
        return UNKNOWN;
    }

    public int getId() {
        return this.id;
    }
}

