package io.dblosevn.xserver.bungee.events.global;

import com.google.gson.JsonObject;
import io.dblosevn.xserver.bungee.events.global.CrossServerEvent;
import io.dblosevn.xserver.bungee.sockets.packets.PingPacket;

public class CrossServerPingEvent
extends CrossServerEvent {
    private String server = "proxy";

    public CrossServerPingEvent() {
    }

    public CrossServerPingEvent(JsonObject o) {
        this.deSerialize(o);
    }

    @Override
    public JsonObject serialize() {
        JsonObject o = new JsonObject();
        o.addProperty("server", this.server);
        return o;
    }

    @Override
    public void deSerialize(JsonObject o) {
        this.server = o.get("server").getAsString();
    }

    @Override
    public PingPacket getPacket() {
        return new PingPacket(this);
    }
}

