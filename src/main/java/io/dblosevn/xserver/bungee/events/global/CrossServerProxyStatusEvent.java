package io.dblosevn.xserver.bungee.events.global;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.dblosevn.xserver.bungee.CrossServerLib;
import io.dblosevn.xserver.bungee.events.global.CrossServerEvent;
import io.dblosevn.xserver.bungee.sockets.packets.ProxyStatusPacket;
import java.util.Map.Entry;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class CrossServerProxyStatusEvent
extends CrossServerEvent {
    public final int STATUS_REQUEST = 1;
    public final int STATUS_RESPONSE = 2;
    private int status = 0;

    public CrossServerProxyStatusEvent() {
    }

    public CrossServerProxyStatusEvent(int status) {
        this.status = status;
    }

    public CrossServerProxyStatusEvent(JsonObject o) {
        this.deSerialize(o);
    }

    @Override
    public JsonObject serialize() {
        JsonObject o = new JsonObject();
        o.addProperty("status", (Number)this.status);
        switch (this.status) {
            case 1: {
                break;
            }
            case 2: {
                ProxyServer p = CrossServerLib.get().getProxy();
                JsonObject servers = new JsonObject();
                for (Entry<String, ServerInfo> e : p.getServers().entrySet()) {
                    JsonArray players = new JsonArray();
                    for (ProxiedPlayer player : ((ServerInfo)e.getValue()).getPlayers()) {
                        JsonObject temp = new JsonObject();
                        temp.addProperty("uuid", player.getUniqueId().toString());
                        temp.addProperty("name", player.getName());
                        players.add((JsonElement)temp);
                    }
                    servers.add((String)e.getKey(), (JsonElement)players);
                }
                o.add("servers", (JsonElement)servers);
            }
        }
        return o;
    }

    @Override
    public void deSerialize(JsonObject o) {
        this.status = o.get("status").getAsInt();
    }

    @Override
    public ProxyStatusPacket getPacket() {
        return new ProxyStatusPacket(this);
    }

    public boolean isRequest() {
        if (this.status == 1) {
            return true;
        }
        return false;
    }
}

