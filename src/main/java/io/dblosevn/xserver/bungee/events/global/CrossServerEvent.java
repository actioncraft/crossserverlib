package io.dblosevn.xserver.bungee.events.global;

import com.google.gson.JsonObject;
import io.dblosevn.xserver.bungee.sockets.Packet;
import net.md_5.bungee.api.plugin.Event;

public abstract class CrossServerEvent
extends Event {
    public abstract JsonObject serialize();

    public abstract void deSerialize(JsonObject var1);

    public abstract Packet getPacket();
}

