package io.dblosevn.xserver.bungee;

import io.dblosevn.xserver.bukkit.api.DB;
import io.dblosevn.xserver.bukkit.api.DBSchema;
import io.dblosevn.xserver.bungee.events.global.CrossServerPingEvent;
import io.dblosevn.xserver.bungee.events.global.CrossServerProxyStatusEvent;
import io.dblosevn.xserver.bungee.listeners.PlayerListener;
import io.dblosevn.xserver.bungee.sockets.Packet;
import io.dblosevn.xserver.bungee.sockets.PacketType;
import io.dblosevn.xserver.bungee.sockets.ProxyClient;
import io.dblosevn.xserver.bungee.sockets.packets.ProxyStatusPacket;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.concurrent.TimeUnit;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class CrossServerLib extends Plugin {
	private static CrossServerLib instance;
	private static ProxyClient client;
	public static ChatColor primaryColor;
	public static ChatColor secondaryColor;
	
	public void onEnable() {
		instance = this;
		Configuration config = this.getConfig();
		String host = config.getString("db.host", "localhost");
		int port = config.getInt("db.port", 3306);
		String database = config.getString("db.database", "game");
		String username = config.getString("db.user", "root");
		String password = config.getString("db.pass", "");
		String prefix = config.getString("db.prefix", "");
		primaryColor = ChatColor.valueOf(config.getString("textColors.primary", "DARK_PURPLE"));
		secondaryColor = ChatColor.valueOf(config.getString("textColors.secondary", "YELLOW"));
		
		try {
			DB.initialize(host, port, database, username, password, prefix);
			DBSchema.createTables();
		} catch (Exception e) {
			e.printStackTrace();
		}
		client = new ProxyClient(config.getString("xserver.host", "localhost"), config.getInt("xserver.port", 9876)) {

			@Override
			protected void onSend(Packet packet) {
			}

			@Override
			protected void onOpen() {
				System.out.println("Open: ");
				try {
					ProxyStatusPacket packet = new ProxyStatusPacket(new CrossServerProxyStatusEvent(2));
					client.send(packet);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			protected void onMessage(Packet packet) {
				if (packet.getType().equals(PacketType.PROXY_STATUS)
						&& ((CrossServerProxyStatusEvent) packet.getEvent()).isRequest()) {
					client.send(new ProxyStatusPacket(new CrossServerProxyStatusEvent(2)));
				}
			}

			@Override
			protected void onClose() {
				System.out.println("Close: ");
			}
		};
		this.getProxy().getPluginManager().registerListener((Plugin) this, (Listener) new PlayerListener());
		CrossServerLib.get().getProxy().getScheduler().schedule((Plugin) CrossServerLib.get(), new Runnable() {

			@Override
			public void run() {
				if (client.isConnected()) {
					client.send(new CrossServerPingEvent().getPacket());
				}
			}
		}, 5L, 5L, TimeUnit.SECONDS);
	}

	public void onDisable() {
		client.shutdown();
	}

	public static CrossServerLib get() {
		return instance;
	}

	public Configuration getConfig() {
		File file;
		if (!this.getDataFolder().exists()) {
			this.getDataFolder().mkdir();
		}
		if (!(file = new File(this.getDataFolder(), "config.yml")).exists()) {
			try {
				InputStream in = this.getResourceAsStream("config.yml");
				try {
					Files.copy(in, file.toPath());
				} finally {
					if (in != null) {
						in.close();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			return ConfigurationProvider.getProvider(YamlConfiguration.class)
					.load(new File(this.getDataFolder(), "config.yml"));
		} catch (IOException e) {
			return null;
		}
	}

}
