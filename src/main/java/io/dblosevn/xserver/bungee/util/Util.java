package io.dblosevn.xserver.bungee.util;

import java.util.concurrent.TimeUnit;

import io.dblosevn.xserver.bungee.CrossServerLib;

public class Util {
    private static String messagePrefix = CrossServerLib.get().getConfig().getString("chat.channels.broadcast.broadcastTag");

    public static String getTag() {
        return messagePrefix;
    }
    
    public static String timeToString(long time) {
        String res = "";
        long days = TimeUnit.MILLISECONDS.toDays(time);
        long hours = TimeUnit.MILLISECONDS.toHours(time) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(time));
        long minutes = TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time));
        long seconds = TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time));
        res = days == 0L ? (hours == 0L ? (minutes == 0L ? String.format("%02ds", seconds) : String.format("%dm %02ds", minutes, seconds)) : String.format("%dh %01dm", hours, minutes)) : String.format("%dd %dh", days, hours);
        return res;
    }

    public static long stringToDuration(String time, String delim) {
    	String parts[] = time.split(delim);
    	long out = 0;
    	for (String s: parts) {
    		String unit = s.substring(s.length() - 1, 1);
    		int amount = Integer.parseInt(s.substring(0, s.length() - 2));

    		switch (unit) {
				case "s": out += (amount); break;
				case "m": out += (amount * 60); break;
    			case "h": out += (amount * 60 * 60); break;
    			case "d": out += (amount * 60 * 60 * 24); break;
    		}
    	}
    	return out;
    }
}